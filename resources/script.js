/**
 * Register javascript actions
 *
 * @category   Ajax
 * @package    Register
 * @author     Tom Kaczocha <tom@crazydev.org>
 * @copyright  2012 Tom Kaczocha
 * @license    http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Register callbacks
 */
var RegisterCallback = {
    
    getproductprices: function(response)
    {
        var obj = eval('(' + response + ')');
        
        var table = $('products-o');
        
        var rowCount = table.rows.length - 5;
        var total = 0;
        
        // Iterate over each row in table
        for (var j = 1; j < rowCount - 1; j++) {
            
            var product_id = table.rows[j].cells[1].childNodes[0].value;
            
            // grab a reference to the correct row
            var row = table.rows[j];
            
            // find the correct record
            for (var i = 0; i < obj.length; i++){
                
                if (obj[i].product_id == product_id) {
                    var qty = parseInt(row.cells[2].childNodes[0].value);
                    var price = parseFloat(obj[i].price);
                    var rowDiscount = $('discount').value;
                    var rowTotal = parseFloat(qty * price);
                    total += parseFloat(rowTotal) - rowDiscount;
                    
                    row.cells[3].style.textAlign = 'center';
                    row.cells[3].innerHTML = '$ ' + build_price(price);
                    break;
                }
            }
        }
        
        //var orderShipping = $('shipping').value;
        //var orderDiscount = $('discount').value;
        //var orderTax = $('tax').value;
        //
        //total += (orderShipping + orderTax) - orderDiscount;
        //
        //table.rows[rowCount + 4].cells[1].innerHTML = '$ ' + build_price(total);
        
    },
    
    deleteproduct: function(response)
    {
        showResponse(response);
        if (response[0]['level'] == 'RESPONSE_NOTICE')
        {
            $('product-row-' + response[0]['data']['id']).hide();
        }
    },
    
    deletecategory: function(response)
    {
        showResponse(response);
        if (response[0]['level'] == 'RESPONSE_NOTICE')
        {
            $('category-row-' + response[0]['data']['id']).hide();
        }
    },
    
    deletecustomer: function(response)
    {
        showResponse(response);
        if (response[0]['level'] == 'RESPONSE_NOTICE')
        {
            $('customer-row-' + response[0]['data']['id']).hide();
        }
    },
    
    deletesupplier: function(response)
    {
        showResponse(response);
        if (response[0]['level'] == 'RESPONSE_NOTICE')
        {
            $('supplier-row-' + response[0]['data']['id']).hide();
        }
    },
    
    deletepurchaseorder: function(response)
    {
        showResponse(response);
        if (response[0]['level'] == 'RESPONSE_NOTICE')
        {
            $('purchase-order-row-' + response[0]['data']['id']).hide();
        }
    },
    
    deletecustomerorder: function(response)
    {
        showResponse(response);
        if (response[0]['level'] == 'RESPONSE_NOTICE')
        {
            $('customer-order-row-' + response[0]['data']['id']).hide();
        }
    },
    
    savesettings: function(response)
    {
        showResponse(response, true);
    },
    
    calculatecustomerordertotal: function(response)
    {
        var table = $('products-o');
        var rowCount = table.rows.length - 5;
        table.rows[rowCount + 4].cells[1].innerHTML = '$ ' + build_price(response);
    },
    
    completeimagecrop: function(response)
    {
        showResponse(response);
    }
}

/**
 * Builds price string
 */
function build_price(price)
{
    if (price == 0)
    {
        return '0.00';
    }
    
    var p = price.toString();
    var pos = p.lastIndexOf('.');
    
    if (pos == -1) {
        return p + '.00';
    }
    if ((p.length - pos) <= 2)
    {
        return p + '0';
    }
    
    return price;
}

/**
 * Used by other functions to set COOKIES
 */
function set_cookie(name, value, expire)
{
    var cookie_string = 'name=' + name + ';' + value + ';expires=' + expire;
    alert(cookie_string);
    
    document.cookie = name;
    document.cookie = ''
}

/**
 * Creates a cookie expiration date
 */
function cookieDate(hours, expire) {
    
    var now = new Date();
    var num = 1;
    
    if (expire == true)
    {
        num = num * -1;
    }
    
    now.setHours(now.getHours() + (hours * num));
    
    return now.toUTCString() + ';';
}

/**
 * Validates Purchase Order Form for minimum
 * input values
 */
function validate_purchase_order_form()
{
    var supplier = $('suppliers').value;
    
    if (supplier == '')
    {
        alert('please select a supplier');
        return false;
    }
    
    var table = $('products-o');
    var rowCount = table.rows.length - 5;
    
    var valid_row_count = 0;
    
    // validate product table rows
    for (var i = 1; i < rowCount; i++)
    {
        var row = table.rows[i];
        var productsCombo = row.cells[1].childNodes[0];
        var product = productsCombo.options[productsCombo.selectedIndex].text;
        
        var qty = row.cells[2].childNodes[0].value;
        var price = row.cells[3].childNodes[0].value;
        var discount = row.cells[4].childNodes[0].value;
        
        if (product != '')
        {
            valid_row_count++;
            
            if (qty == '')
            {
                alert('please enter a quantity for ' + product);
                return false;
            }
            if (price == '')
            {
                alert('please enter a item price for ' + product);
                return false;
            }
            if (discount == '')
            {
                alert('please enter a discount value for ' + product);
                return false;
            }
        }
        else if (product == '' && rowCount <= 2)
        {
            alert('please add products to your order');
            return false;
        }
    }
    
    if (valid_row_count > 0)
    {
        return true;
    }
    
    alert('please add a product to your order');
    return false;
}

/**
 * Validates Purchase Order Form for minimum
 * input values
 */
function validate_customer_order_form()
{
    var customer = $('customers').value;
    
    if (customer == 0)
    {
        alert('please select a customer');
        return false;
    }
    
    var table = $('products-o');
    var rowCount = table.rows.length - 5;
    
    var valid_row_count = 0;
    
    // validate product table rows
    for (var i = 1; i < rowCount; i++)
    {
        var row = table.rows[i];
        var productsCombo = row.cells[1].childNodes[0];
        var product = productsCombo.options[productsCombo.selectedIndex].text;
        
        var qty = row.cells[2].childNodes[0].value;
    
        if (product != '')
        {
            valid_row_count++;
            
            if (qty == '' || qty == 0)
            {
                alert('please enter a quantity for ' + product);
                return false;
            }
        }
        else if (product == '' && rowCount <= 2)
        {
            alert('please add products to your order');
            return false;
        }
    }
    
    if (valid_row_count > 0)
    {
        return true;
    }
    
    alert('please add a product to your order');
    return false;
}

/**
 * Adds a product row to the purchase order
 * form
 */
function add_purchase_order_row()
{
    var table = $('products-o');
    
    var rowCount = table.rows.length - 5;
    var row = table.insertRow(rowCount);
    var colCount = table.rows[1].cells.length;
    
    for (var i = 0; i < colCount; i++)
    {
        var newCell = row.insertCell(i);
        
        newCell.innerHTML = table.rows[1].cells[i].innerHTML;
        
        if (newCell.childNodes[0] != null) {
            // set values
            switch(newCell.childNodes[0].type)
            {
                case "text":
                    newCell.childNodes[0].value = "";
                    newCell.style.width = '50px';
                    newCell.style.textAlign = 'center';
                    break;
                case "checkbox":
                    newCell.childNodes[0].value = false;
                    newCell.style.textAlign = 'center';
                    break;
                case "select-one":
                    newCell.childNodes[0].selectedIndex = 0;
                    newCell.style.textAlign = 'center';
                    break;
            }
        }
    }
}

/**
 * Adds a product row to the customer order
 * form
 */
function add_customer_order_row()
{
    var table = $('products-o');
    
    var rowCount = table.rows.length - 5;
    var row = table.insertRow(rowCount);
    var colCount = table.rows[1].cells.length;
    
    try
    {
        for (var i = 0; i < colCount; i++)
        {
            var newCell = row.insertCell(i);
            
            newCell.innerHTML = table.rows[1].cells[i].innerHTML;
            
            if (newCell.childNodes[0] != null)
            {
                // set values
                switch(newCell.childNodes[0].type)
                {
                    case "text":
                        if (newCell.childNodes[0].name == 'qty')
                        {
                            newCell.childNodes[0].value = "1";
                        }
                        if (newCell.childNodes[0].name == 'discount')
                        {
                            newCell.childNodes[0].value = "0";
                        }
                        newCell.style.width = '50px';
                        newCell.style.textAlign = 'center';
                        break;
                    case "checkbox":
                        newCell.childNodes[0].value = false;
                        newCell.style.textAlign = 'center';
                        break;
                    case "select-one":
                        newCell.childNodes[0].selectedIndex = 0;
                        newCell.style.textAlign = 'center';
                        break;
                }
            }
        }
    }
    catch (e)
    {
        alert('Add Product Row Error:\n' + e);
    }
}

/**
 * Calculates order total
 */
function calculate_c_order_total()
{
    var table = $('products-o');
    var rowCount = table.rows.length - 5;
    
    var ids = new Array();
    
    try {
        // get all product IDs
        for (var i = 1; i < rowCount; i++)
        {
            var row = table.rows[i];
            
            if (row.cells[1].childNodes[0].selectedIndex == 0)
            {
                row.cells[3].innerHTML = '';
            }
            else{
                ids[i] = row.cells[1].childNodes[0].value;
            }
        }
    }
    catch (e)
    {
        alert('Calculate Order Total Error:\n' + e);
    }
    
    // Get Prices
    var response = registerSync.getproductprices(ids.toString());
    
    var obj = eval('(' + response + ')');
    
    // Iterate over each row in table
    for (var j = 1; j < rowCount; j++)
    {
        var product_id = table.rows[j].cells[1].childNodes[0].value;
        
        // grab a reference to the correct row
        var row = table.rows[j];
        
        // find the correct record
        for (var i = 0; i < obj.length; i++)
        {
            if (obj[i].product_id == product_id)
            {
                var price = parseFloat(obj[i].price);
                row.cells[3].style.textAlign = 'center';
                row.cells[3].innerHTML = '$ ' + build_price(price);
                break;
            }
        }
    }
    
    // Set Order Total Async
    registerAsync.calculatecustomerordertotal('edit_customer_order', 'edit_customer_order_products');
}

/**
 * Calculates purchase order total
 */
function calculate_p_order_total()
{
    var table = $('products-o');
    var rowCount = table.rows.length - 5;
    
    var total = 0;
    
    try {
        // sum products total
        for (var i = 1; i < rowCount; i++)
        {
            var row = table.rows[i];
            
            if (row.cells[1].childNodes[0].value != 0 &&
                row.cells[1].childNodes[0].value != '')
            {
                var qty = parseInt(row.cells[2].childNodes[0].value);
                var itemPrice = parseFloat(row.cells[3].childNodes[0].value);
                var discount = parseFloat(row.cells[4].childNodes[0].value);
                
                if (isNaN(qty))
                {
                    qty = 0;
                }
                if (isNaN(itemPrice))
                {
                    itemPrice = 0;
                }
                if (isNaN(discount))
                {
                    discount = 0;
                }
                
                total += ((qty * itemPrice) - discount);
            }
        }
        
        var shipping = parseFloat($('shipping').value);
        var discount = parseFloat($('discount').value);
        var tax = parseFloat($('tax').value);
        
        if (isNaN(shipping))
        {
            shipping = 0;
        }
        if (isNaN(discount))
        {
            discount = 0;
        }
        if (isNaN(tax))
        {
            tax = 0;
        }
        
        var sum = total + shipping + tax - discount;
        sum = sum.toFixed(2);
        $('total').innerHTML = '$ ' + build_price(sum);
    }
    catch (e)
    {
        alert('Calculate Order Total Error:\n' + e);
    }
}

/**
 * Deletes the selected rows in the purchase
 * order form
 */
function delete_purchase_order_row()
{
    try
    {
        var table = $('products-o');
        var rowCount = table.rows.length - 5;
        
        for (var i = 0; i < rowCount; i++)
        {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            
            if (null != chkbox  && true == chkbox.checked)
            {
                if (rowCount <= 2)
                {
                    alert('Cannot delete all the rows');
                    break;
                }
                
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        }
    }
    catch (e)
    {
        alert('Delete Product Row Error:\n' + e);
    }
}

/**
 * Deletes the selected rows in the customer
 * order form
 */
function delete_customer_order_row()
{
    try
    {
        var table = $('products-o');
        var rowCount = table.rows.length - 5;
        
        for (var i = 0; i < rowCount; i++)
        {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            
            if (null != chkbox  && true == chkbox.checked)
            {
                if (rowCount <= 2)
                {
                    alert('Cannot delete all the rows');
                    break;
                }
                
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        }
    }
    catch (e)
    {
        alert('Delete Product Row Error:\n' + e);
    }
}

/**
 * Deletes Product Async
 */
function delete_product(id)
{
    registerAsync.deleteproduct(id);
}

/**
 * Deletes Category Async
 */
function delete_category(id)
{
    registerAsync.deletecategory(id);
}

/**
 * Deletes a customer identified by ID
 */
function delete_customer(id)
{
    registerAsync.deletecustomer(id);
}

/**
 * Deletes a supplier identified by ID
 */
function delete_supplier(id)
{
    registerAsync.deletesupplier(id);
}

/**
 * Deletes a purchase order identified by ID
 */
function delete_purchase_order(id)
{
    registerAsync.deletepurchaseorder(id);
}

/**
 * Deletes a customer order identified by ID
 */
function delete_customer_order(id)
{
    registerAsync.deletecustomerorder(id);
}

/**
 * Validate Test Run
 */
function validate_tests_selected()
{
    var table = $('tests');
    var rowCount = table.rows.length;
    var selectedTests = 0;
    var cookie = 'tests';
    var value = '';
    
    // validate we have tests to run
    for (var i = 1; i < rowCount; i++)
    {
        var row = table.rows[i];
        if (row.cells[0].childNodes[0].checked)
        {
            selectedTests++;
            value += row.cells[1].innerHTML + '$';
        }
    }
    
    if (selectedTests > 0)
    {
        registerSync.addteststodictionary(value);
        return true;
    }
    
    alert('Please select tests you want to run');
    
    return false;
}

/**
 * Toggles all Tests Selection
 */
function toggle_select_all_tests()
{
    var table = $('tests');
    var rowCount = table.rows.length;
    
    var selectAll = $('all_tests_all');
    
    for (var i = 1; i < rowCount; i++)
    {
        var row = table.rows[i];
        row.cells[0].childNodes[0].checked = selectAll.checked;
    }
    
    //selected.checked = false;
    
}

/**
 * Runs selected tests
 */
function run_tests()
{
    var table = $('test-results');
    var rowCount = table.rows.length;
    var summaryRow = table.rows[rowCount - 1];
    var lastRow = 1;
    var totalTime = 0;
    var success = 0;
    var failed = 0;
    
    summaryRow.hide();
    
    var testString = registerSync.gettestsfromdictionary();
    
    var tests = testString.split('$');
    
    for (var i = 0; i < tests.length - 1; i++)
    {
        var result = registerSync.runtest(tests[i]);
        
        for (var k = 0; k < result.length; k++)
        {
            var testName = result[k].test_name;
            var testResult = result[k].status == 1 ? 'PASS' : 'FAIL';
            var background = testResult == 'PASS' ? 'green' : 'red';
            var time = result[k].time;
            
            totalTime += time;
            
            if (testResult == 'PASS')
            {
                success++;
            }
            else
            {
                failed++;
            }
            
            for (var j = lastRow; j < rowCount; j++)
            {
                var row = table.rows[j];
                var rowID = row.id;
                
                if (rowID == 'test-name-' + testName)
                {
                    row.setStyle('background-color:' + background + ';color:#ffffff;');
                    
                    row.insertCell(1);
                    row.cells[1].setStyle('text-align:center');
                    row.cells[1].insert(time + ' sec');
                    row.insertCell(2);
                    row.cells[2].setStyle('text-align:center');
                    row.cells[2].insert(testResult);
                    
                    if (testResult == 'FAIL')
                    {
                        table.insertRow(lastRow + 1);
                        table.rows[lastRow + 1].insertCell(0);
                        table.rows[lastRow + 1].insertCell(1);
                        table.rows[lastRow + 1].insertCell(2);
                        table.rows[lastRow + 1].setStyle('background-color:red;color:#ffffff;');
                        table.rows[lastRow + 1].cells[0].setStyle('padding-left:20px;');
                        table.rows[lastRow + 1].cells[0].update(result[k].message);
                    }
                    break;
                }
                
                lastRow++;
            }
        }
        
    }
    
    var summary = '';
    summary = 'Total Execution Time: ' + totalTime.toFixed(4) + ' sec';
    summary += ' | Successful Tests: ' + success;
    summary += ' |  Failed Tests: ' + failed;
    
    summaryRow.cells[0].update(summary);
    summaryRow.show();
}

/**
 * Save User Preferences
 */
function save_settings()
{
    var params = '';
    
    params += 'environment=' + $('environment').value;
    params += '&company=' + $('company').value;
    params += '&abn=' + $('abn').value;
    params += '&email=' + $('email').value;
    params += '&website=' + $('website').value;
    params += '&phone=' + $('phone').value;
    params += '&fax=' + $('fax').value;
    params += '&street1=' + $('street1').value;
    params += '&street2=' + $('street2').value;
    params += '&city=' + $('city').value;
    params += '&state=' + $('state').value;
    params += '&pcode=' + $('pcode').value;
    params += '&country=' + $('country').value;
    
    registerAsync.savesettings(params);
}

/**
 * Saves Product being edited to Dictionary
 */
function save_product_to_dictionary()
{
    var key = 'edit_product';
    var params = '';
    
    params += 'name=' + $('name').value;
    params += '&category=' + $('catid').value;
    params += '&supplier=' + $('suppid').value;
    params += '&url=' + $('url').value;
    params += '&visible=' + $('visibility').checked;
    params += '&wPrice=' + $('whole_price').value;
    params += '&rPrice=' + $('retail_price').value;
    params += '&type=' + $('product_type').value;
    params += '&sDescription=' + $('short_desc_block').value;
    params += '&lDescription=' + $('long_desc_block').value;
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(params));
}

/**
 * Saves Product being edited to Dictionary
 */
function save_category_to_dictionary()
{
    var key = 'edit_category';
    var params = '';
    
    params += 'name=' + $('name').value;
    params += '&visible=' + $('visibility').checked;
    params += '&description=' + $('short_desc_block').value;
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(params));
}

/**
 * Saves Supplier being edited to Dictionary
 */
function save_supplier_to_dictionary()
{
    var key = 'edit_supplier';
    var params = '';
    
    params += 'name=' + $('txt_company').value;
    params += '&abn=' + $('txt_abn').value;
    params += '&website=' + $('txt_website').value;
    params += '&visible=' + $('visibility').checked;
    params += '&phone=' + $('txt_phone').value;
    params += '&fax=' + $('txt_fax').value;
    params += '&f_name=' + $('txt_rep_first_name').value;
    params += '&l_name=' + $('txt_rep_last_name').value;
    params += '&email=' + $('txt_email').value;
    params += '&mobile=' + $('txt_mobile').value;
    params += '&street1=' + $('txt_street1').value;
    params += '&street2=' + $('txt_street2').value;
    params += '&city=' + $('txt_city').value;
    params += '&state=' + $('txt_state').value;
    params += '&pcode=' + $('txt_pcode').value;
    params += '&country=' + $('txt_country').value;
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(params));
}

/**
 * Saves Supplier being edited to Dictionary
 */
function save_customer_to_dictionary()
{
    var key = 'edit_customer';
    var params = '';
    
    params += 'f_name=' + $('txt_first_name').value;
    params += '&l_name=' + $('txt_last_name').value;
    params += '&company=' + $('txt_company').value;
    params += '&abn=' + $('txt_abn').value;
    params += '&website=' + $('txt_website').value;
    params += '&visible=' + $('visibility').checked;
    params += '&phone=' + $('txt_phone').value;
    params += '&fax=' + $('txt_fax').value;
    params += '&email=' + $('txt_email').value;
    params += '&mobile=' + $('txt_mobile').value;
    params += '&street1=' + $('txt_street1').value;
    params += '&street2=' + $('txt_street2').value;
    params += '&city=' + $('txt_city').value;
    params += '&state=' + $('txt_state').value;
    params += '&pcode=' + $('txt_pcode').value;
    params += '&country=' + $('txt_country').value;
    
    params += '&ship_same=' + $('same_shipping').checked;
    
    params += '&ship_street1=' + $('txt_ship_street1').value;
    params += '&ship_street2=' + $('txt_ship_street2').value;
    params += '&ship_city=' + $('txt_ship_city').value;
    params += '&ship_state=' + $('txt_ship_state').value;
    params += '&ship_pcode=' + $('txt_ship_pcode').value;
    params += '&ship_country=' + $('txt_ship_country').value;
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(params));
}

/**
 * Saves Purchase Order being edited to Dictionary
 */
function save_purchase_order_to_dictionary()
{
    var key = 'edit_purchase_order';
    
    var supplier_id = $('suppliers').value;
    var date = $('order_date').value;
    var ref = $('ref_no').value;
    var status = $('status').value;
    var comment = $('comments').value;
    var shipping = $('shipping').value;
    var discount = $('discount').value;
    var added = $('added').value;
    var tax = $('tax').value;
    
    if (shipping == '')
    {
        shipping = 0;
    }
    if (discount == '')
    {
        discount = 0;
    }
    if (tax == '')
    {
        tax = 0;
    }
    
    var value =
        'supplier_id=' + supplier_id +
        '&date=' + date +
        '&ref=' + ref +
        '&status=' + status +
        '&added=' + added +
        '&comments=' + encodeURI(comment) +
        '&shipping=' + shipping +
        '&discount=' + discount +
        '&tax=' + tax
    ;
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(value));
}

/**
 * Saves Supplier being edited to Dictionary
 */
function save_purchase_order_products_to_dictionary()
{
    var key = 'edit_purchase_order_products';
    
    var product_string = '';
    
    try {
        var table = $('products-o');
        var rowCount = table.rows.length - 5;
        
        for (var i = 1; i < rowCount; i++)
        {
            var row = table.rows[i];
            var product_index = row.cells[1].childNodes[0].value;
            var qty = row.cells[2].childNodes[0].value;
            var price = row.cells[3].childNodes[0].value;
            var discount = row.cells[4].childNodes[0].value;
            
            if (product_index == '')
            {
                product_index = 0;
            }
            if (qty == '')
            {
                qty = 0;
            }
            if (price == '')
            {
                price = 0;
            }
            if (discount == '')
            {
                discount = 0;
            }
            
            var value = 
                'product=' + product_index +
                '&qty=' + qty +
                '&price=' + price +
                '&discount=' + discount +
                '$';
            
            product_string += value;
        }
    }
    catch (e)
    {
        alert(e);
    }
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(product_string + ';'));
}

/**
 * Saves Customer Order Details to Dictionary
 */
function save_customer_order_to_dictionary()
{
    var key = 'edit_customer_order';
    
    var customer_id     = $('customers').value;
    var date            = $('order_date').value;
    var status          = $('status').value;
    var comments        = $('comments').value;
    var shipping        = $('shipping').value;
    var discount        = $('discount').value;
    var removed         = $('removed').value;
    var tax             = $('tax').value;
    
    if (shipping == '')
    {
        shipping = 0;
    }
    if (discount == '')
    {
        discount = 0;
    }
    if (tax == '')
    {
        tax = 0;
    }
    
    var value =
        'customer_id=' + customer_id +
        '&date=' + date +
        '&status=' + status +
        '&comments=' + comments +
        '&shipping=' + shipping +
        '&discount=' + discount +
        '&removed=' + removed +
        '&tax=' + tax
    ;
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(value));
}

/**
 * Saves Customer Order Products to Dictionary
 */
function save_customer_order_products_to_dictionary()
{
    var key = 'edit_customer_order_products';
    var product_string = '';
    
    try {
        var table = $('products-o');
        var rowCount = table.rows.length - 5;
        
        for (var i = 1; i < rowCount; i++)
        {
            var row = table.rows[i];
            var product_id = row.cells[1].childNodes[0].value;
            var qty = row.cells[2].childNodes[0].value;
            var discount = row.cells[4].childNodes[0].value;
            
            if (product_id == 0)
            {
                continue;
            }
            
            if (qty == '')
            {
                qty = 0;
            }
            
            var value = 
                'product=' + product_id +
                '&qty=' + qty +
                '&discount=' + discount +
                '$';
            
            product_string += value;
        }
    }
    catch (e)
    {
        alert('Set Products Cookie Error:\n' + e);
    }
    
    registerSync.savekeyvaluetodictionary(key, encodeURI(product_string + ';'));
}

function select_image_crop(coords, dimensions)
{
    var image = $('product-image');
    var source = image.src;
    
    registerSync.savecroptodictionary(source, coords.x1, coords.y1, dimensions.width, dimensions.height);
}

function complete_image_crop()
{
    var image = $('product-image');
    var source = image.src;
    
    registerSync.completeimagecrop(source);
}

//Combo colors
var evenColor = '#fff';
var oddColor  = '#edf3fe';

//Which action are we runing?
var currentAction = null;

//Which row selected in DataGrid
var selectedRow = null;
var selectedRowColor = null;

var registerAsync = new registeradminajax(RegisterCallback);
registerAsync.serverErrorFunc = Jaws_Ajax_ServerError;
registerAsync.onInit = showWorkingNotification;
registerAsync.onComplete = hideWorkingNotification;

var registerSync = new registeradminajax();
registerSync.serverErrorFunc = Jaws_Ajax_ServerError;
registerSync.onInit = showWorkingNotification;
registerSync.onComplete = hideWorkingNotification;

