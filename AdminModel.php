<?php
/**
 * Register Gadget
 *
 * @category    GadgetModel
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */

 require_once JAWS_PATH . 'gadgets/Register/Model.php';
 
 class RegisterAdminModel extends RegisterModel
 {
    private $_dictionary;
    private $_environment = 2;
    
    public function __construct()
    {
        $this->_dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
    }
    
    /**
     * Install the gadget
     *
     * @access public
     * @return bool\Jaws_Error     TRUE on success and Jaws_Error on failure
     */
    public function InstallGadget()
    {
        $this->createDatabase();
        
        return TRUE;
    }
    
    /**
     * Creates complete database
     *
     * @access public
     * @return NULL\Jaws_Error
     */
    public function createDatabase()
    {
        $result = $this->installSchema('schema.xml');
        if (Jaws_Error::IsError($result))
        {
            return $result;
        }
        
        $now = time();
        $data = array(
            'date_created'          => $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s'),
            'date_updated'          => $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s'),
            'description_general'   => 'General solutions',
            'description_mobile'    => 'Mobile solutions',
            'description1'          => 'mTraveller for WP7',
            'description2'          => 'mTraveller for Android',
        );
        
        $result = $this->installSchema('insert.xml', $data, 'schema.xml', true);
        if (Jaws_Error::IsError($result))
        {
            return $result;
        }
        
        $this->_dictionary->createKey('settings/environment', $this->_environment);
        $this->_dictionary->createKey('settings/company', 'Jaws Project');
        $this->_dictionary->createKey('settings/abn', '123-456-789');
        $this->_dictionary->createKey('settings/email', 'me@jaws-project.com');
        $this->_dictionary->createKey('settings/website', 'http://jaws-project.com');
        $this->_dictionary->createKey('settings/phone', '613 9309 3455');
        $this->_dictionary->createKey('settings/fax', '613 9309 5695');
        $this->_dictionary->createKey('settings/street1', '13 Smiley St');
        $this->_dictionary->createKey('settings/street2', ' ');
        $this->_dictionary->createKey('settings/city', 'Melbourne');
        $this->_dictionary->createKey('settings/state', 'VIC');
        $this->_dictionary->createKey('settings/pcode', '3659');
        $this->_dictionary->createKey('settings/country', 'Australia');
        
    }
    
    /**
     * Uninstalls the gadget
     *
     * @access public
     * @return bool     TRUE
     */
    public function UninstallGadget()
    {
        $this->removeDatabase();
        
        return TRUE;
    }
    
    /**
     * Drops all database tables
     *
     * @access public
     * @return NULL\Jaws_Error      Nothing or Jaws_Error on error
     */
    public function removeDatabase()
    {
        $result = $this->_dictionary->get('settings/environment');
        $this->_environment = $result['value'];
        
        $tables = array(
            'register_products',
            'register_categories',
            'register_suppliers',
            'register_customers',
            'register_customer_orders',
            'register_customer_order_items',
            'register_purchase_orders',
            'register_purchase_order_items',
            'register_order_status',
            'register_dictionary',
            'register_product_types',
            'register_product_images',
        );
        
        foreach ($tables as $table)
        {
            $result = $GLOBALS['db']->dropTable($table);
            if (Jaws_Error::IsError($result))
            {
                $gName = _t('REGISTER_NAME');
                $errMsg = _t('GLOBAL_ERROR_GADGET_NOT_UNINSTALLED', $gName);
                $GLOBALS['app']->Session->PushLastResponse($errMsg, RESPONSE_ERROR);
                return new Jaws_Error($errMsg, $gName);
            }
        }
    }
    
    /**
     * Creates a Test Database
     *
     * @access public
     */
    public function createTestDatabase()
    {
        $GLOBALS['db'] = Jaws_DB::getInstance(array('prefix' => 'test_'), 'test');
        $this->createDatabase();
    }
    
    /**
     * Removes Test Database
     *
     * @access public
     */
    public function removeTestDatabase()
    {
        $GLOBALS['db'] = Jaws_DB::getInstance(array('prefix' => 'test_'), 'test');
        $this->removeDatabase();
        $GLOBALS['db'] = Jaws_DB::getInstance();
    }
    
    /**
     * Update the gadget
     *
     * @access public
     * @param string    $old    Current version (in registry)
     * @param string    $new    New version (in the $gadgetInfo file)
     * @return bool\Jaws_Error  Success/Failure (Jaws_Error)
     */
    public function UpdateGadet($old, $new)
    {
        //if (version_compare($old, '0.8.0', '<'))
        //{
        //    $result = $this->installSchema('0.8.0.xml', '', "$old.xml");
        //    if (Jaws_Error::IsError($result))
        //    {
        //        return $result;
        //    }
        //    
        //    $result = $this->installSchema('insert.xml', '', '0.8.0.xml', true);
        //    if (Jaws_Error::IsError($result))
        //    {
        //        return $result;
        //    }
        //    
        //    // ACL keys
        //    $GLOBALS['app']->ACL->NewKey('/ACL/gadgets/Register/ManageGroups', 'true');
        //    
        //    // Registry keys
        //    $GLOBALS['app']->Registry->NewKey('/gadgets/Register/cookie_period', '150');
        //}
        
        //$result = $this->installSchema('schema.xml', '', "0.8.0.xml");
        //if (Jaws_Error::IsError($result)) {
        //    return $result;
        //}
        
        return TRUE;
    }
    
 }