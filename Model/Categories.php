<?php
/**
 * Register Gadget Categories Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Categories extends Jaws_Model
{
    /**
     * Retrieve product categories
     *
     * @access public
     * @return array\Jaws_Error An array of available categories or Jaws_Error
     */
    public function GetAllCategories()
    {
        $sql = '
            SELECT *
            FROM [[register_categories]]
            ORDER BY
                [category_name] ASC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves category array
     *
     * @access public
     * @param int       $id     category ID
     * @return array\Jaws_Error category array or Jaws_Error
     */
    public function GetCategoryById($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CATEGORY_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_CATEGORY_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_categories]]
            WHERE [category_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    
}