<?php

require_once JAWS_PATH . 'gadgets/Register/Model.php';
/**
 * Register Gadget Admin Orders Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Admin_Orders extends RegisterModel
{
    /**
     * Saves purchase order to database
     *
     * @access public
     * @param int       $supplier   supplier ID
     * @param string    $date       order date
     * @param string    $ref        order reference
     * @param int       $status     order status ID
     * @param array     $products   array of product IDs
     * @param array     $qty        array of product quantities
     * @param array     $prices     array of product prices
     * @param array     $discounts  array of discounts
     * @param string    $comments   order comments
     * @param double    $discount   order discount
     * @param double    $tax        order tax
     * @param double    $shipping   order shipping
     * @return int\Jaws_Error  inserted ID or Jaws_Error
     */
    public function SaveNewPurchaseOrder(
        $supplier, $date, $ref, $status, $products = array(), $qty = array(),
        $prices = array(), $discounts = array(), $comments, $discount, $tax, $shipping)
    {
        $params = array();
        $params['supplier_id']          = $supplier;
        $params['date']                 = $date;
        $params['ref']                  = $ref;
        $params['status']               = $status;
        $params['comments']             = $comments;
        $params['discount']             = $discount;
        $params['tax']                  = $tax;
        $params['shipping']             = $shipping;
        $params['total']                = 0;
        $params['date_created']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $product_count = count($products);
        
        // Get Order Total
        for ($i = 0; $i < $product_count; $i++)
        {
            $params['total'] += ($qty[$i] * $prices[$i]) - $discounts[$i];
        }
        
        $params['total'] += ($shipping + $tax) - $discount;
        
        $sql = '
            INSERT INTO [[register_purchase_orders]]
                ([supplier_id], [reference_no], [purchase_order_date], [purchase_order_total], [purchase_order_status_id],
                [purchase_order_comments], [date_created], [date_updated])
            VALUES
                ({supplier_id}, {ref}, {date}, {total}, {status}, {comments}, {date_created}, {date_updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_NOT_ADDED'));
        }
        
        $insert_id = $GLOBALS['db']->lastInsertId('register_purchase_orders');
        
        // Add Purchase Order Items
        for ($i = 0; $i < $product_count; $i++)
        {
            $res = $this->SavePurchaseOrderItem($insert_id, $products[$i], $qty[$i], $prices[$i], $discounts[$i], FALSE, $status);
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ADDED'), RESPONSE_NOTICE);
        
        return $insert_id;
    }
    
    /**
     * Adds a Purchase Order Item to database
     *
     * @access public
     * @param int       $purchase_id    purchase order ID
     * @param int       $product_id     product ID
     * @param int       $qty            quantity for product
     * @param double    $price          item price
     * @param double    $discount       discount
     * $param bool      $added          indicates if products qty has been added to products
     * @param int       $status         purchase order status ID
     * @return mixed    insert ID or Jaws_Error
     */
    public function SavePurchaseOrderItem($purchase_id, $product_id, $qty, $price, $discount, $added, $status)
    {
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
        
        // If order is not Closed & Products have been added
        if ($status != 5 && $added)
        {
            $p_model->UpdateProductQty($product_id, $qty, FALSE);
        }
        else if ($status == 5 && !$added)
        {
            $p_model->UpdateProductQty($product_id, $qty, TRUE);
        }
        
        $params = array();
        $params['purchase_id']          = $purchase_id;
        $params['product_id']           = $product_id;
        $params['qty']                  = $qty;
        $params['price']                = $price;
        $params['discount']             = $discount;
        
        $sql = '
            INSERT INTO [[register_purchase_order_items]]
                ([purchase_order_id], [product_id], [quantity], [item_price], [discount])
            VALUES
                ({purchase_id}, {product_id}, {qty}, {price}, {discount})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_ITEM_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_ITEM_NOT_ADDED'));
        }
        
        return $GLOBALS['db']->lastInsertId('register_purchase_order_items');
    }
    
    /**
     * Updates purchase order
     *
     * @access public
     * @param int       $id         purchase order ID
     * @param int       $supplier   supplier ID
     * @param string    $date       order date
     * @param string    $ref        order reference
     * @param int       $status     order status ID
     * @param array     $products   array of product IDs
     * @param array     $qty        array of product quantities
     * @param array     $prices     array of product prices
     * @param array     $discounts  array of discounts
     * @param bool      $added      have products been added to stock
     * @param string    $comments   order comments
     * @param double    $discount   order discount
     * @param double    $tax        order tax
     * @param double    $shipping   order shipping
     * @return mixed    TRUE or Jaws_Error
     */
    public function UpdatePurchaseOrder(
        $id, $supplier, $date, $ref, $status, $products = array(), $qty = array(),
        $prices = array(), $discounts = array(), $added = 0, $comments, $discount, $tax, $shipping)
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['supplier_id']          = $supplier;
        $params['date']                 = $date;
        $params['ref']                  = $ref;
        $params['status']               = $status;
        $params['added']                = $added;
        $params['comments']             = $comments;
        $params['discount']             = $discount;
        $params['tax']                  = $tax;
        $params['shipping']             = $shipping;
        $params['total']                = 0;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        // update qty_added
        if ($status != 5 && $added)
        {
            $params['added'] = FALSE;
        }
        else if ($status == 5 && !$added)
        {
            $params['added'] = TRUE;
        }
        
        $product_count = count($products);
        
        // Get Order Total
        for ($i = 0; $i < $product_count; $i++)
        {
            $params['total'] += ($qty[$i] * $prices[$i]) - $discounts[$i];
        }
        
        $params['total'] += ($shipping + $tax) - $discount;
        
        $sql = '
            UPDATE [[register_purchase_orders]]
            SET
                [supplier_id]               = {supplier_id},
                [reference_no]              = {ref},
                [purchase_order_date]       = {date},
                [purchase_order_status_id]  = {status},
                [purchase_order_comments]   = {comments},
                [purchase_order_discount]   = {discount},
                [purchase_order_shipping]   = {shipping},
                [purchase_order_total]      = {total},
                [purchase_order_tax]        = {tax},
                [qty_added]                 = {added},
                [date_updated]              = {date_updated}
            WHERE [purchase_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_NOT_UPDATED'));
        }
        
        // get existing purchase order products
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $res = $model->GetPurchaseOrderProductsByPurchaseOrderId($params['id']);
        
        // delete existing
        foreach ($res as $existing)
        {
            $this->DeletePurchaseOrderItem($existing['purchase_order_items_id']);
        }
        
        // add new
        // Add Purchase Order Items
        for ($i = 0; $i < $product_count; $i++)
        {
            $res = $this->SavePurchaseOrderItem($id, $products[$i], $qty[$i], $prices[$i], $discounts[$i], $added, $status);
        }
        
        return TRUE;
    }
    
    /**
     * Deletes Purchase Order Item
     *
     * @access public
     * @param int       $id         purchase order ID
     * @return bool\Jaws_Error      TRUE if successful, else Jaws_Error
     */
    public function DeletePurchaseOrderItem($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            DELETE FROM [[register_purchase_order_items]]
            WHERE [purchase_order_items_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'));
        }
        
        //$GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ITEM_DELETED'), RESPONSE_NOTICE);
        
        return TRUE;
    }
    
    /**
     * Deletes Purchase Order and ALL order products
     *
     * @todo Method Does not adjust product quantities
     * 
     * @access public
     * @param int       $id     purchase order ID
     */
    public function DeletePurchaseOrder($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_DELETED'), RESPONSE_ERROR);
            return;
        }
        
        $params = array();
        $params['id']       = $id;
        
        // DELETE PURCHASE ORDER
        $sql = '
            DELETE FROM [[register_purchase_orders]]
            WHERE [purchase_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_DELETED'), RESPONSE_ERROR, $params);
            return;
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_DELETED'), RESPONSE_NOTICE, $params);
        
        // DELETE PURCHASE ORDER ITEMS
        $sql = '
            DELETE FROM [[register_purchase_order_items]]
            WHERE [purchase_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_DELETED'), RESPONSE_ERROR, $params);
            return;
        }
    }
    
    
    
    
    
    
    
    
    /**
     * Saves customer order to database
     *
     * @access public
     * @param int       $customer_id    customer ID
     * @param string    $date           order date
     * @param int       $status         order status ID
     * @param array     $products       array of product IDs
     * @param array     $qty            array of product quantities
     * @param array     $prices         array of product prices
     * @param array     $discounts      array of discounts
     * @param string    $comments       order comments
     * @param double    $discount       order discount
     * @param double    $tax            order tax
     * @param double    $shipping       order shipping
     * @return int\Jaws_Error           order ID or Jaws_Error
     */
    public function SaveNewCustomerOrder(
        $custmer_id, $date, $status, $products = array(), $qty = array(),
        $prices = array(), $discounts = array(), $comments, $discount, $tax, $shipping)
    {
        $params = array();
        $params['customer_id']          = $custmer_id;
        $params['date']                 = $date;
        $params['status']               = $status;
        $params['comments']             = $comments;
        $params['discount']             = $discount;
        $params['tax']                  = $tax;
        $params['shipping']             = $shipping;
        $params['total']                = 0;
        $params['date_created']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $product_count = count($products);
        
        // Get Order Total
        for ($i = 0; $i < $product_count; $i++)
        {
            $params['total'] += ($qty[$i] * $prices[$i]) - $discounts[$i];
        }
        
        $params['total'] += ($shipping + $tax) - $discount;
        
        $sql = '
            INSERT INTO [[register_customer_orders]]
                ([customer_id], [customer_order_date], [customer_order_total], [customer_order_status_id],
                 [customer_order_comments], [customer_order_tax], [customer_order_discount], [customer_order_shipping],
                 [date_created], [date_updated])
            VALUES
                ({customer_id}, {date}, {total}, {status}, {comments}, {tax}, {discount}, {shipping},
                {date_created}, {date_updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_NOT_ADDED'));
        }
        
        $order_id = $GLOBALS['db']->lastInsertId('register_customer_orders');
        
        // Add Customer Order Items
        for ($i = 0; $i < $product_count; $i++)
        {
            $res = $this->SaveCustomerOrderItem($order_id, $products[$i], $qty[$i], $prices[$i], $discounts[$i], FALSE, $status);
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ADDED'), RESPONSE_NOTICE);
        
        return $order_id;
    }
    
    /**
     * Adds a Customer Order Item to database
     *
     * @access public
     * @param int       $order_id       customer order ID
     * @param int       $product_id     product ID
     * @param int       $qty            product quantity
     * @param double    $price          item price
     * @param double    $discount       item discount
     * @param int       $status         order status ID
     * @param bool      $removed        have products been removed from stock
     * @return int\Jaws_Error   item ID or Jaws_Error
     */
    public function SaveCustomerOrderItem($order_id, $product_id, $qty, $price, $discount, $removed, $status)
    {
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
        
        // If order is not Filled, In-Transit OR Closed & Products have been Removed
        if (($status != 3 || $status != 4 || $status != 5) && $removed)
        {
            $p_model->UpdateProductQty($product_id, $qty, TRUE);
        }
        else if (($status == 3 || $status == 4 || $status == 5) && !$removed)
        {
            $p_model->UpdateProductQty($product_id, $qty, FALSE);
        }
        
        $params = array();
        $params['order_id']             = $order_id;
        $params['product_id']           = $product_id;
        $params['qty']                  = $qty;
        $params['price']                = $price;
        $params['discount']             = $discount;
        
        $sql = '
            INSERT INTO [[register_customer_order_items]]
                ([customer_order_id], [product_id], [quantity], [item_price], [discount])
            VALUES
                ({order_id}, {product_id}, {qty}, {price}, {discount})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_ITEM_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_ITEM_NOT_ADDED'));
        }
        
        return $GLOBALS['db']->lastInsertId('register_purchase_order_items');
    }
    
    /**
     * Updates Customer Order
     *
     * @access public
     * @param int       $id             order ID
     * @param int       $customer_id    customer ID
     * @param string    $date           order date
     * @param int       $status         order status ID
     * @param array     $products       array of product IDs
     * @param array     $qty            array of product quantities
     * @param array     $prices         array of product prices
     * @param array     $discounts      array of discounts
     * $param bool      $removed        have products been removed from stock
     * @param string    $comments       order comments
     * @param double    $discount       order discount
     * @param double    $tax            order tax
     * @param double    $shipping       order shipping
     * @return mixed    TRUE or Jaws_Error
     */
    public function UpdateCustomerOrder(
        $id, $custmer_id, $date, $status, $products = array(), $qty = array(),
        $prices = array(), $discounts = array(), $removed = 0, $comments, $discount, $tax, $shipping)
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['customer_id']          = $custmer_id;
        $params['date']                 = $date;
        $params['status']               = $status;
        $params['comments']             = $comments;
        $params['discount']             = $discount;
        $params['tax']                  = $tax;
        $params['shipping']             = $shipping;
        $params['removed']              = $removed;
        $params['total']                = 0;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        // update qty_added
        if (($status != 3 || $status != 4 || $status != 5) && $removed)
        {
            $params['removed'] = FALSE;
        }
        else if (($status == 3 || $status == 4 || $status == 5) && !$removed)
        {
            $params['removed'] = TRUE;
        }
        
        $product_count = count($products);
        
        // Get Order Total
        for ($i = 0; $i < $product_count; $i++)
        {
            $params['total'] += ($qty[$i] * $prices[$i]) - $discounts[$i];
        }
        
        $params['total'] += ($shipping + $tax) - $discount;
        
        $sql = '
            UPDATE [[register_customer_orders]]
            SET
                [customer_id]               = {customer_id},
                [customer_order_date]       = {date},
                [customer_order_status_id]  = {status},
                [customer_order_comments]   = {comments},
                [customer_order_discount]   = {discount},
                [customer_order_shipping]   = {shipping},
                [customer_order_total]      = {total},
                [customer_order_tax]        = {tax},
                [qty_removed]               = {removed},
                [date_updated]              = {date_updated}
            WHERE [customer_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_NOT_UPDATED'));
        }
        
        // get existing purchase order products
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $res = $model->GetCustomerOrderProductsByCustomerOrderId($params['id']);
        
        // delete existing
        foreach ($res as $existing)
        {
            $this->DeleteCustomerOrderItem($existing['customer_order_items_id']);
        }
        
        // add new
        // Add Purchase Order Items
        for ($i = 0; $i < $product_count; $i++)
        {
            $res = $this->SaveCustomerOrderItem($id, $products[$i], $qty[$i], $prices[$i], $discounts[$i], $removed, $status);
        }
        
        return TRUE;
    }
    
    /**
     * Deletes Customer Order Item
     *
     * @access public
     * @param int       $id     customer order item ID
     * @return mixed    TRUE or Jaws_Error
     */
    public function DeleteCustomerOrderItem($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            DELETE FROM [[register_customer_order_items]]
            WHERE [customer_order_items_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_ORDER_ITEM_NOT_DELETED'));
        }
        
        return TRUE;
    }
    
    /**
     * Deletes Customers Order and ALL order products
     *
     * @todo Method Does not adjust product quantities
     * 
     * @access public
     * @param int       $id     customer order ID
     */
    public function DeleteCustomerOrder($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_DELETED'), RESPONSE_ERROR);
            return;
        }
        
        $params = array();
        $params['id']       = $id;
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        
        $order = $model->GetCustomerOrderById($id);
        
        if ($order[0]['customer_order_status_id'] == 3 || $order[0]['customer_order_status_id'] == 4 ||
            $order[0]['customer_order_status_id'] == 5)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_IN_TRANSIT'), RESPONSE_ERROR, $params);
            return;
        }
        
        // DELETE CUSTOMER ORDER
        $sql = '
            DELETE FROM [[register_customer_orders]]
            WHERE [customer_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_DELETED'), RESPONSE_ERROR, $params);
            return;
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_DELETED'), RESPONSE_NOTICE, $params);
        
        // DELETE CUSTOMER ORDER ITEMS
        $sql = '
            DELETE FROM [[register_customer_order_items]]
            WHERE [customer_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_ORDER_NOT_DELETED'), RESPONSE_ERROR, $params);
        }
    }
}