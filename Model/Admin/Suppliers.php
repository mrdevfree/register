<?php

require_once JAWS_PATH . 'gadgets/Register/Model.php';
/**
 * Register Gadget Admin Suppliers Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Admin_Suppliers extends RegisterModel
{
    /**
     * Saves New Supplier Action
     *
     * @access public
     * @param string    $name           company names
     * @param string    $abn            ABN (Australian Business Number)
     * @param string    $first_name     company representative first name
     * @param string    $last_name      company representative last name
     * @param string    $phone          company phone number
     * @param string    $fax            company fax number
     * @param string    $mobile         mobile number
     * @param string    $email          email address
     * @param string    $web            website address
     * @param string    $street1        street address line 1
     * @param string    $street2        street address line 2
     * @param string    $city           company city
     * @param string    $state          company state
     * @param string    $pcode          company post code ( zip )
     * @param string    $country        company country
     * @param bool      $visible        is company visible
     * @return int\Jaws_Error   supplier ID or Jaws_Error
     */
    public function SaveNewSupplier(
        $name, $abn, $first_name, $last_name, $phone, $fax, $mobile, $email, $web,
        $street1, $street2, $city, $state, $pcode, $country, $visible)
    {
        $params = array();
        $params['company_name']         = $name;
        $params['visible']              = $visible;
        $params['abn']                  = $abn;
        $params['first_name']           = $first_name;
        $params['last_name']            = $last_name;
        $params['phone']                = $phone;
        $params['fax']                  = $fax;
        $params['mobile']               = $mobile;
        $params['email']                = $email;
        $params['website']              = $web;
        $params['street1']              = $street1;
        $params['street2']              = $street2;
        $params['city']                 = $city;
        $params['state']                = $state;
        $params['pcode']                = $pcode;
        $params['country']              = $country;
        $params['date_created']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            INSERT INTO [[register_suppliers]]
                ([company_name], [supplier_visible], [abn], [rep_first_name], [rep_last_name], [phone], [fax], [mobile], [email], [website],
                    [street1], [street2], [city], [state], [pcode], [country], [date_created], [date_updated])
            VALUES
                ({company_name}, {visible}, {abn}, {first_name}, {last_name}, {phone}, {fax}, {mobile}, {email}, {website},
                    {street1}, {street2}, {city}, {state}, {pcode}, {country}, {date_created}, {date_updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_SUPPLIER_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_SUPPLIER_NOT_ADDED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_SUPPLIER_ADDED'), RESPONSE_NOTICE);
        
        return $GLOBALS['db']->lastInsertId('register_suppliers');
    }
    
    /**
     * Update Supplier Action
     *
     * @access public
     * @param int       $id             supplier ID
     * @param string    $name           company names
     * @param string    $abn            ABN (Australian Business Number)
     * @param string    $first_name     company representative first name
     * @param string    $last_name      company representative last name
     * @param string    $phone          company phone number
     * @param string    $fax            company fax number
     * @param string    $mobile         mobile number
     * @param string    $email          email address
     * @param string    $web            website address
     * @param string    $street1        street address line 1
     * @param string    $street2        street address line 2
     * @param string    $city           company city
     * @param string    $state          company state
     * @param string    $pcode          company post code ( zip )
     * @param string    $country        company country
     * @param bool      $visible        is company visible
     * @return bool\Jaws_Error   TRUE or Jaws_Error
     */
    public function UpdateSupplier(
        $id, $name, $abn, $first_name, $last_name, $phone, $fax, $mobile, $email, $web,
        $street1, $street2, $city, $state, $pcode, $country, $visible)
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_SUPPLIER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_SUPPLIER_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['company_name']         = $name;
        $params['visible']              = $visible;
        $params['abn']                  = $abn;
        $params['first_name']           = $first_name;
        $params['last_name']            = $last_name;
        $params['phone']                = $phone;
        $params['fax']                  = $fax;
        $params['mobile']               = $mobile;
        $params['email']                = $email;
        $params['website']              = $web;
        $params['street1']              = $street1;
        $params['street2']              = $street2;
        $params['city']                 = $city;
        $params['state']                = $state;
        $params['pcode']                = $pcode;
        $params['country']              = $country;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            UPDATE [[register_suppliers]]
            SET
                [company_name]      = {company_name},
                [supplier_visible]  = {visible},
                [abn]               = {abn},
                [rep_first_name]    = {first_name},
                [rep_last_name]     = {last_name},
                [phone]             = {phone},
                [fax]               = {fax},
                [mobile]            = {mobile},
                [email]             = {email},
                [website]           = {website},
                [street1]           = {street1},
                [street2]           = {street2},
                [city]              = {city},
                [state]             = {state},
                [pcode]             = {pcode},
                [country]           = {country},
                [date_updated]      = {date_updated}
            WHERE [supplier_id]     = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_SUPPLIER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_SUPPLIER_NOT_UPDATED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_SUPPLIER_UPDATED'), RESPONSE_NOTICE);
        
        return TRUE;
    }
    
    /**
     * Delete Supplier Action
     *
     * @access public
     * @param int       $id     supplier ID
     * @return bool\Jaws_Error  TRUE or Jaws_Error
     */
    function DeleteSupplier($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_SUPPLIER_NOT_DELETED'), RESPONSE_ERROR);
            return;
        }
        
        $params = array();
        $params['id']       = $id;
        
        // DO NOT ALLOW DELETE SUPPLIER IF SUPPLIER HAS PRODUCTS
        // comment-out this block if you want to allow the deletion
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        if ($model->GetProductCountBySupplier($id) > 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_SUPPLIER_HAS_PRODUCTS'), RESPONSE_ERROR, $params);
            return;
        }
        
        $sql = '
            DELETE FROM [[register_suppliers]]
            WHERE [supplier_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_SUPPLIER_NOT_DELETED'), RESPONSE_ERROR, $params);
            return;
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_SUPPLIER_DELETED'), RESPONSE_NOTICE, $params);
    }
}