<?php

require_once JAWS_PATH . 'gadgets/Register/Model.php';
/**
 * Register Gadget Admin Products Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Admin_Products extends RegisterModel
{
    /**
     * Save new Product
     *
     * @access public
     * @param string    $name       product name
     * @param int       $catID      category ID
     * @param int       $suppID     supplier ID
     * @param bool      $visible    visibility
     * @param string    $short      short description
     * @param string    $url        product url
     * @param double    $wPrice     wholesale price
     * @param double    $rPrice     retail price
     * @param string    $long       long description
     * @return mixed    product ID or Jaws_Error
     */
    public function SaveNewProduct(
        $name, $catID, $suppID, $visible, $short, $url = '', $wPrice = '', $rPrice = '', $type = 1, $long = '')
    {
        $params = array();
        $params['product_name']         = $name;
        $params['category_id']          = $catID;
        $params['supplier_id']          = $suppID;
        $params['visible']              = $visible;
        $params['url']                  = $url;
        $params['short_description']    = $short;
        $params['long_description']     = $long;
        $params['price_wholesale']      = $wPrice;
        $params['price_retail']         = $rPrice;
        $params['type']                 = $type;
        $params['date_created']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            INSERT INTO [[register_products]]
                ([product_name], [category_id], [supplier_id], [product_visible], [url], [short_description], [long_description],
                [price_wholesale], [price_retail], [product_type], [date_created], [date_updated])
            VALUES
            ({product_name}, {category_id}, {supplier_id}, {visible}, {url}, {short_description}, {long_description},
                {price_wholesale}, {price_retail}, {type}, {date_created}, {date_updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_ADDED'), RESPONSE_ERROR, $params);
            return new Jaws_Error(_t('REGISTER_ERROR_PRODUCT_NOT_ADDED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ADDED'), RESPONSE_NOTICE, $params);
        
        return $GLOBALS['db']->lastInsertId('register_products');
    }
    
    /**
     * Update Product
     *
     * @access public
     * @param int       $id         product ID
     * @param string    $name       product name
     * @param int       $catID      category ID
     * @param int       $suppID     supplier ID
     * @param bool      $visible    visibility
     * @param string    $short      short description
     * @param string    $url        product url
     * @param double    $wPrice     wholesale price
     * @param double    $rPrice     retail price
     * @param string    $long       long description
     * @return mixed    TRUE or Jaws_Error
     */
    public function UpdateProduct(
        $id, $name, $catID, $suppID, $visible, $short, $url = '', $wPrice = '', $rPrice = '', $type = 1, $long = '')
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_PRODUCT_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['product_name']         = $name;
        $params['category_id']          = $catID;
        $params['supplier_id']          = $suppID;
        $params['visible']              = $visible;
        $params['url']                  = $url;
        $params['short_description']    = $short;
        $params['long_description']     = $long;
        $params['price_wholesale']      = $wPrice;
        $params['price_retail']         = $rPrice;
        $params['type']                 = $type;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            UPDATE [[register_products]]
            SET
                [product_name]      = {product_name},
                [category_id]       = {category_id},
                [supplier_id]       = {supplier_id},
                [product_visible]   = {visible},
                [url]               = {url},
                [short_description] = {short_description},
                [long_description]  = {long_description},
                [price_wholesale]   = {price_wholesale},
                [price_retail]      = {price_retail},
                [product_type]      = {type},
                [date_updated]      = {date_updated}
            WHERE [product_id]      = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_PRODUCT_NOT_UPDATED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_UPDATED'), RESPONSE_NOTICE);
        
        return TRUE;
    }
    
    /**
     * Delete Product
     *
     * @access public
     * @param int       $id     product ID
     */
    public function DeleteProduct($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_DELETED'), RESPONSE_ERROR);
            return;
        }
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        
        $params = array();
        $params['id']       = $id;
        
        if ($model->GetPurchaseOrderCountForProduct($id) != 0 || $model->GetCustomerOrderCountForProduct($id) != 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_HAS_ORDERS'), RESPONSE_ERROR, $params);
            return;
        }
        
        $sql = '
            DELETE FROM [[register_products]]
            WHERE [product_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_DELETED'), RESPONSE_ERROR, $params);
        }
        else if ($result == 1)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_DELETED'), RESPONSE_NOTICE, $params);
        }
        else
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_EXIST'), RESPONSE_ERROR, $params);
        }
    }
    
    /**
     * Updates product stock levels
     *
     * Purchase orders don't need to supply the third optional
     * value, which would be TRUE. Customer orders do have to
     * supply the FALSE value
     *
     * @access public
     * @param int       $id     product ID
     * @param int       $qty    quantity to adjust by
     * @param bool      $add    true = Add, false = Subtract
     */
    public function UpdateProductQty($id, $qty, $add = TRUE)
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_UPDATED'), RESPONSE_ERROR);
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['qty']                  = $qty;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '';
        
        if ($add)
        {
            $sql = '
                UPDATE [[register_products]]
                SET
                    [qty]               = qty + {qty},
                    [date_updated]      = {date_updated}
                WHERE [product_id]      = {id}
            ';
        }
        else
        {
            $sql = '
                UPDATE [[register_products]]
                SET
                    [qty]               = qty - {qty},
                    [date_updated]      = {date_updated}
                WHERE [product_id]      = {id}
            ';
        }
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_UPDATED'), RESPONSE_ERROR, $params);
        }
        else if ($result == 1)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_UPDATED'), RESPONSE_NOTICE, $params);
        }
        else{
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_EXIST'), RESPONSE_ERROR, $params);
        }
    }
    
    /**
     * Inserts new product image
     *
     * @access public
     * @param   int     $id     product ID
     * @param   string  $name   filename
     * @return  mixed   insert ID or Jaws_Error
     */
    public function AddNewProductImage($id = 0, $name = NULL)
    {
        if ($id == 0 || $name == NULL)
        {
            return new Jaws_Error("Missing Parameters", JAWS_ERROR_FATAL);
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['name']                 = $name;
        
        $sql = '
            INSERT INTO [[register_product_images]]
                ([product_id], [image_url])
            VALUES
            ({id}, {name})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_PRODUCT_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_PRODUCT_NOT_ADDED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ADDED'), RESPONSE_NOTICE);
        
        return $GLOBALS['db']->lastInsertId('register_product_images');
    }
    
}