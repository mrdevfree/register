<?php

require_once JAWS_PATH . 'gadgets/Register/Model.php';
/**
 * Register Gadget Admin Categories Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Admin_Categories extends RegisterModel
{
    /**
     * Save new Category
     *
     * @access public
     * @param string    $name           category name
     * @param bool      $visible        visibility of the category
     * @param string    $description    short category description
     * @return int\Jaws_Error   last insert ID or Jaws_Error
     */
    public function SaveNewCategory($name, $visible, $description)
    {
        $params = array();
        $params['category_name']        = $name;
        $params['visible']              = $visible;
        $params['description']          = $description;
        $params['date_created']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            INSERT INTO [[register_categories]]
                ([category_name], [category_visible], [description], [date_created], [date_updated])
            VALUES
            ({category_name}, {visible}, {description}, {date_created}, {date_updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CATEGORY_NOT_ADDED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CATEGORY_ADDED'), RESPONSE_NOTICE);
        
        return $GLOBALS['db']->lastInsertId('register_categories');
    }

    /**
     * Update Category
     *
     * @access public
     * @param int       $id             category ID
     * @param string    $name           category name
     * @param bool      $visible        visibility of the category
     * @param string    $description    short category description
     * @return bool     TRUE if successful, else Jaws_Error 
     */
    public function UpdateCategory($id, $name, $visible, $description)
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CATEGORY_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['category_name']        = $name;
        $params['visible']              = $visible;
        $params['description']          = $description;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            UPDATE [[register_categories]]
            SET
                [category_name]     = {category_name},
                [category_visible]  = {visible},
                [description]       = {description},
                [date_updated]      = {date_updated}
            WHERE [category_id]      = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CATEGORY_NOT_UPDATED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CATEGORY_UPDATED'), RESPONSE_NOTICE);
        
        return TRUE;
    }
    
    /**
     * Delete Category
     *
     * @access public
     * @param int   $id category ID
     */
    public function DeleteCategory($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_NOT_DELETED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CATEGORY_NOT_DELETED'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        // DO NOT ALLOW DELETE CATEGORY IF CATEGORY HAS PRODUCTS
        // comment-out this block if you want to allow the deletion
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        if ($model->GetProductCountByCategory($id) > 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_HAS_PRODUCTS'), RESPONSE_ERROR, $params);
            return new Jaws_Error(_t('REGISTER_ERROR_CATEGORY_HAS_PRODUCTS'));
        }
        
        $sql = '
            DELETE FROM [[register_categories]]
            WHERE [category_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_NOT_DELETED'), RESPONSE_ERROR, $params);
            return new Jaws_Error(_t('REGISTER_ERROR_CATEGORY_NOT_DELETED'));
        }
        
        if ($result == 1)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CATEGORY_DELETED'), RESPONSE_NOTICE, $params);
        }
        else{
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CATEGORY_NOT_EXIST'), RESPONSE_ERROR, $params);
        }
    }
    
}