<?php

require_once JAWS_PATH . 'gadgets/Register/Model.php';
/**
 * Register Gadget Admin Customers Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Admin_Customers extends RegisterModel
{
    /**
     * Save New Customer
     *
     * @access public
     * @param string    $first_name     customers first name
     * @param string    $last_name      customers last name
     * @param string    $company        company name
     * @param string    $abn            ABN - Australian Business Number
     * @param string    $phone          customers phone
     * @param string    $fax            customers fax
     * @param string    $mobile         customers mobile number
     * @param string    $email          customers email address
     * @param string    $web            customers website address
     * @param string    $street1        customers street address line 1
     * @param string    $street2        customers street address line 2
     * @param string    $city           customers city
     * @param string    $state          customers state
     * @param string    $pcode          customers post code / zip
     * @param string    $country        customers country
     * @param bool      $visible        visibility
     * @return int\Jaws_Error           last insert ID or Jaws_Error
     */
    public function SaveNewCustomer(
        $first_name, $last_name, $company, $abn, $phone, $fax, $mobile, $email, $web,
        $street1, $street2, $city, $state, $pcode, $country, $visible)
    {
        $params = array();
        $params['company_name']         = $company;
        $params['visible']              = $visible;
        $params['abn']                  = $abn;
        $params['first_name']           = $first_name;
        $params['last_name']            = $last_name;
        $params['phone']                = $phone;
        $params['fax']                  = $fax;
        $params['mobile']               = $mobile;
        $params['email']                = $email;
        $params['website']              = $web;
        $params['street1']              = $street1;
        $params['street2']              = $street2;
        $params['city']                 = $city;
        $params['state']                = $state;
        $params['pcode']                = $pcode;
        $params['country']              = $country;
        $params['date_created']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            INSERT INTO [[register_customers]]
                ([company_name], [customer_visible], [abn], [name_first], [name_last], [phone], [fax], [mobile], [email], [website],
                    [street1], [street2], [city], [state], [pcode], [country], [date_created], [date_updated])
            VALUES
                ({company_name}, {visible}, {abn}, {first_name}, {last_name}, {phone}, {fax}, {mobile}, {email}, {website},
                    {street1}, {street2}, {city}, {state}, {pcode}, {country}, {date_created}, {date_updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_ADDED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_ADDED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_ADDED'), RESPONSE_NOTICE);
        
        return $GLOBALS['db']->lastInsertId('register_customers');
    }
    
    /**
     * Update Customer Action
     *
     * @access public
     * @param int       $id             customer ID
     * @param string    $first_name     customers first name
     * @param string    $last_name      customers last name
     * @param string    $company        company name
     * @param string    $abn            ABN - Australian Business Number
     * @param string    $phone          customers phone
     * @param string    $fax            customers fax
     * @param string    $mobile         customers mobile number
     * @param string    $email          customers email address
     * @param string    $web            customers website address
     * @param string    $street1        customers street address line 1
     * @param string    $street2        customers street address line 2
     * @param string    $city           customers city
     * @param string    $state          customers state
     * @param string    $pcode          customers post code / zip
     * @param string    $country        customers country
     * @param bool      $visible        visibility
     * @return bool\Jaws_Error          TRUE if successful, else Jaws_Error
     */
    public function UpdateCustomer($id, $first_name, $last_name, $company, $abn, $phone, $fax, $mobile, $email, $web,
                             $street1, $street2, $city, $state, $pcode, $country, $visible)
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['company_name']         = $company;
        $params['visible']              = $visible;
        $params['abn']                  = $abn;
        $params['first_name']           = $first_name;
        $params['last_name']            = $last_name;
        $params['phone']                = $phone;
        $params['fax']                  = $fax;
        $params['mobile']               = $mobile;
        $params['email']                = $email;
        $params['website']              = $web;
        $params['street1']              = $street1;
        $params['street2']              = $street2;
        $params['city']                 = $city;
        $params['state']                = $state;
        $params['pcode']                = $pcode;
        $params['country']              = $country;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            UPDATE [[register_customers]]
            SET
                [name_first]        = {first_name},
                [name_last]         = {last_name},
                [customer_visible]  = {visible},
                [company_name]      = {company_name},
                [abn]               = {abn},
                [phone]             = {phone},
                [fax]               = {fax},
                [mobile]            = {mobile},
                [email]             = {email},
                [website]           = {website},
                [street1]           = {street1},
                [street2]           = {street2},
                [city]              = {city},
                [state]             = {state},
                [pcode]             = {pcode},
                [country]           = {country},
                [date_updated]      = {date_updated}
            WHERE [customer_id]     = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_UPDATED'), RESPONSE_NOTICE);
        
        return TRUE;
    }
    
    /**
     * Updates Customer Shipping Address
     *
     * @access public
     * @param   int     $id         customer ID
     * @param   string  $street1    street address line 1
     * @param   string  $street2    street address line 2
     * @param   string  $city       delivery city
     * @param   string  $state      deliery state
     * @param   string  $pcode      deliery post code or zip
     * @param   string  $country    delivery country
     * @return mixed    TRUE or Jaws_Error
     */
    public function UpdateShippingAddress($id = '', $same = TRUE, $street1 = '', $street2 = '', $city = '', $state = '', $pcode = '', $country = '')
    {
        if ($id == null || $id == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'));
        }
        
        $params = array();
        $params['id']                   = $id;
        $params['same']                 = $same;
        $params['street1']              = $street1;
        $params['street2']              = $street2;
        $params['city']                 = $city;
        $params['state']                = $state;
        $params['pcode']                = $pcode;
        $params['country']              = $country;
        $params['date_updated']         = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            UPDATE [[register_customers]]
            SET
                [ship_same]         = {same},
                [ship_street1]      = {street1},
                [ship_street2]      = {street2},
                [ship_city]         = {city},
                [ship_state]        = {state},
                [ship_pcode]        = {pcode},
                [ship_country]      = {country},
                [date_updated]      = {date_updated}
            WHERE [customer_id]     = {id}
        ';
        
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_CUSTOMER_NOT_UPDATED'));
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_UPDATED'), RESPONSE_NOTICE);
        
        return TRUE;
    }
    
    /**
     * Delete Customer Action
     *
     * @access public
     * @param   int       $id         customer ID
     */
    public function DeleteCustomer($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_DELETED'), RESPONSE_ERROR);
            return;
        }
        
        $params = array();
        $params['id']       = $id;
        
        // DO NOT ALLOW DELETE CUSTOMER IF CUSTOMER HAS ORDERS
        // comment-out this block if you want to allow the deletion
        $orders_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        if ($orders_model->GetOrdersCountByCustomerID($id) > 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_HAS_ORDERS'), RESPONSE_ERROR, $params);
            return;
        }
        
        $sql = '
            DELETE FROM [[register_customers]]
            WHERE [customer_id] = {id}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_CUSTOMER_NOT_DELETED'), RESPONSE_ERROR, $params);
            return;
        }
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_DELETED'), RESPONSE_NOTICE, $params);
    }
    
}