<?php
/**
 * Register Gadget Suppliers Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Suppliers extends Jaws_Model
{
    /**
     * Retrieves all suppliers
     *
     * @access public
     * @return array\Jaws_Error     array of suppliers or Jaws_Error
     */
    public function GetAllSuppliers()
    {
        $sql = '
            SELECT *
            FROM [[register_suppliers]]
            ORDER BY
                [supplier_id] ASC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves category array
     *
     * @access public
     * @param int       $id     supplier ID
     * @return array\Jaws_Error supplier array or Jaws_Error
     */
    public function GetSupplierById($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_SUPPLIER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_SUPPLIER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_suppliers]]
            WHERE [supplier_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
}