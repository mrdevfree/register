<?php
/**
 * Register Gadget Publisher
 *
 * @category    Model
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Publisher
{
    /**
     * default save location
     */
    private $_dir = 'gadgets/Register/assets/';
    
    /**
     * Constructor
     *
     * @access public
     * @param array     $options    document options
     */
    public function __construct()
    {
        
    }
    
    public function createInvoice($name = 'Invoice-0.pdf', $order = array(), $customer = array())
    {
        require_once 'Invoice.php';
        
        $invoice = new RegisterInvoice($order, $customer);
        $invoice->AliasNbPages();
        $invoice->lMargin = 15;
        $invoice->AddPage();
        $invoice->SetFont('Times', '', 12);
        $invoice->Body($customer);
        $invoice->Output($this->_dir . $name);
    }
    
}