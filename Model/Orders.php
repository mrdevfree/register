<?php
/**
 * Register Gadget Orders Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Orders extends Jaws_Model
{
    /**
     * Retrieves all orders
     * 
     * @access public
     * @return array\Jaws_Error array of customer orders or Jaws_Error
     */
    public function GetAllCustomerOrders()
    {
        $sql = '
            SELECT *
            FROM [[register_customer_orders]]
            LEFT JOIN [[register_customers]] ON [[register_customer_orders]].[customer_id] = [[register_customers]].[customer_id]
            LEFT JOIN [[register_order_status]] ON [[register_customer_orders]].[customer_order_status_id] = [[register_order_status]].[order_status_id]
            ORDER BY
                [customer_order_date] DESC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves all orders
     * 
     * @access public
     * @return array\Jaws_Error     array of purchase orders or Jaws_Error
     */
    public function GetAllPurchaseOrders()
    {
        $sql = '
            SELECT *
            FROM [[register_purchase_orders]]
            LEFT JOIN [[register_suppliers]] ON [[register_purchase_orders]].[supplier_id] = [[register_suppliers]].[supplier_id]
            LEFT JOIN [[register_order_status]] ON [[register_purchase_orders]].[purchase_order_status_id] = [[register_order_status]].[order_status_id]
            ORDER BY
                [purchase_order_date] DESC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves Customer Order Total
     *
     * @access public
     * @param int       $id     customer ID
     * @return int\string\Jaws_Error   orders total for a customer or Jaws_Error
     */
    public function GetCustomerOrdersTotal($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_CUSTOMER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT SUM([customer_order_total]) AS \'total\'
            FROM [[register_customer_orders]]
            WHERE [customer_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result['total'] > 0 ? $result['total'] : '0.00';
    }
    
    /**
     * Gets Customer Orders
     *
     * @access public
     * @param int       $id     customer ID
     * @return int\Jaws_Error   customers orders count or Jaws_Error
     */
    public function GetOrdersCountByCustomerID($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_CUSTOMER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT COUNT(*) AS \'count\'
            FROM [[register_customer_orders]]
            WHERE [customer_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result['count'];
    }
    
    /**
     * Retrieves entire list of Order Status types
     *
     * @access public
     * @return array\Jaws_Error     order status list or Jaws_Error
     */
    public function GetOrderStatusList()
    {
        $sql = '
            SELECT *
            FROM [[register_order_status]]
            ORDER BY
                [order_status_id] ASC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves Order for Order ID
     *
     * @access public
     * @param int       $id     customer order ID
     * @return array\Jaws_Error customer order or Jaws_Error
     */
    public function GetCustomerOrderById($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ORDER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_customer_orders]]
            LEFT JOIN [[register_customer_order_items]] ON [[register_customer_orders]].[customer_order_id] = [[register_customer_order_items]].[customer_order_id]
            LEFT JOIN [[register_products]] ON [[register_customer_order_items]].[product_id] = [[register_products]].[product_id]
            WHERE [[register_customer_orders]].[customer_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryAll($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves Order for Order ID
     *
     * @access public
     * @param int       $id     purchase order ID
     * @return array\Jaws_Error purchase order or Jaws_Error
     */
    public function GetPurchaseOrderById($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ORDER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_purchase_orders]]
            LEFT JOIN [[register_purchase_order_items]]
                ON [[register_purchase_orders]].[purchase_order_id] = [[register_purchase_order_items]].[purchase_order_id]
            LEFT JOIN [[register_order_status]]
                ON [[register_purchase_orders]].[purchase_order_status_id] = [[register_order_status]].[order_status_id]
            WHERE [[register_purchase_orders]].[purchase_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryAll($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves Purchase Order Products by Purchase Order ID
     *
     * @access public
     * @param string    $id     purchase order ID
     * @return array\Jaws_Error array of purchase orders
     */
    public function GetPurchaseOrderProductsByPurchaseOrderId($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ORDER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_purchase_order_items]]
            WHERE [purchase_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryAll($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves number of purchase orders for
     * product ID
     *
     * @access public
     * @param int       $product_id     product ID
     * @return int\Jaws_Error  number of purchase orders for product or Jaws_Error
     */
    public function GetPurchaseOrderCountForProduct($product_id = 0)
    {
        $count = -1;
        
        if ($product_id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_PRODUCT_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $product_id;
        
        $sql = '
            SELECT COUNT(*) AS \'count\'
            FROM [[register_purchase_order_items]]
            WHERE [product_id] = {id}
            GROUP BY [purchase_order_id]
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        $count = @$result['count'];
        
        return $count;
    }
    
    /**
     * Retrieves Customer Order Products by Customer Order ID
     *
     * @access public
     * @param int       $id         purchase order ID
     * @return array\Jaws_Error    array of purchase orders or Jaws_Error
     */
    public function GetCustomerOrderProductsByCustomerOrderId($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ORDER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ORDER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_customer_order_items]]
            WHERE [customer_order_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryAll($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves number of customer orders for
     * product ID
     *
     * @access public
     * @param int       $product_id     product ID
     * @return int\Jaws_Error   number of purchase orders for product or Jaws_Error
     */
    public function GetCustomerOrderCountForProduct($product_id = 0)
    {
        $count = -1;
        
        if ($product_id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_PRODUCT_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $product_id;
        
        $sql = '
            SELECT COUNT(*) AS \'count\'
            FROM [[register_customer_order_items]]
            WHERE [product_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        $count = @$result['count'];
        
        return $count;
    }
    
}