<?php
/**
 * Register Gadget Products Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Products extends Jaws_Model
{
    /**
     * Retrieves all products with
     * their categories
     *
     * @access public
     * @return mixed array of products or Jaws_Error
     */
    public function GetAllProducts()
    {
        $sql = '
            SELECT *
            FROM [[register_products]]
            LEFT JOIN [[register_categories]] ON [[register_products]].[category_id] = [[register_categories]].[category_id]
            LEFT JOIN [[register_product_types]] ON [[register_products]].[product_type] = [[register_product_types]].[product_type_id]
            ORDER BY
                [[register_products]].[product_id] ASC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Gets the number of products for
     * supplier
     *
     * @access public
     * @param   int     $id     supplier ID
     * @return  mixed   number of products from supplier or Jaws_Error
     */
    public function GetProductCountBySupplier($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_SUPPLIER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_SUPPLIER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT COUNT(*) AS \'count\'
            FROM [[register_products]]
            WHERE [supplier_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result['count'];
    }
    
    /**
     * Gets the number of products in a category
     *
     * @access public
     * @param int       $id     category ID
     * @return mixed    number of products in category or Jaws_Error
     */
    public function GetProductCountByCategory($id = 0)
    {
        if ($id == 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CATEGORY_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_CATEGORY_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT COUNT(*) AS \'count\'
            FROM [[register_products]]
            WHERE [category_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result['count'];
    }
    
    /**
     * Retrieves product details from database
     *
     * @access public
     * @param int       $id     product ID
     * @return mixed    product array or Jaws_Error
     */
    public function GetProductById($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_PRODUCT_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_products]]
            LEFT JOIN [[register_categories]] ON [[register_products]].[category_id] = [[register_categories]].[category_id]
            LEFT JOIN [[register_product_types]] ON [[register_products]].[product_type] = [[register_product_types]].[product_type_id]
            WHERE [product_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieve a list of all Product Types
     *
     * @access public
     * @return mixed    types array or Jaws_Error
     */
    public function GetAllProductTypes()
    {
        $sql = '
            SELECT *
            FROM [[register_product_types]]
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves a list of all Product Images
     *
     * @access public
     * @param int   $id     product ID
     * @return mixed        array of product images or Jaws_Error
     */
    public function GetAllProductImagesByID($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_PRODUCT_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_product_images]]
            WHERE [product_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryAll($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves the number of products an image has
     *
     * @access public
     * @param int   $id     product ID
     * @return mixed        product count or Jaws_Error
     */
    public function GetProductImageCount($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_PRODUCT_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT COUNT(*) AS \'count\'
            FROM [[register_product_images]]
            WHERE [product_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        $count = $result['count'];
        
        return $count;
    }
    
    /**
     * Retrieves product image by it's ID
     *
     * @access public
     * @param   int     $id     image ID
     * @return  mixed   image array or Jaws_Error
     */
    public function GetProductImageByID($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_PRODUCT_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_PRODUCT_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_product_images]]
            WHERE [product_image_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
}