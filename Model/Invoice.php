<?php

require_once('gadgets/Register/libs/FPDF/fpdf.php');

/**
 * Register Gadget Invoice
 *
 * @category    Model
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterInvoice extends FPDF
{
    private $_order;
    private $_customer;
    private $_logo = 'gadgets/Register/images/jaws.png';
    private $_dictionary;
    
    private $_invoiceTotal = 0;
    private $_discountTotal = 0.00;
    
    /**
     * Constructor
     *
     * @access public
     * @param array     $order      the order to generate the
     * invoice for
     */
    public function __construct($order = array(), $customer = array())
    {
        parent::FPDF();
        $this->_order = $order;
        $this->_customer = $customer;
        $this->_dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
    }
    
    /**
     * Prints the Invoice header
     *
     * @access public
     */
    public function Header()
    {
        $abn = $this->_dictionary->get('settings/abn');
        
        if ($this->_order == null)
        {
            throw new Exception('Supplied Order is NULL or Invalid');
        }
        
        // Logo
        $this->Image($this->_logo, 10, 6, 35);
        $this->SetFont('Arial', 'B', 8);
        $this->SetFillColor(122, 150, 150);
        $this->SetTextColor(255, 255, 255);
        $this->Ln(10);
        $this->Cell(120);
        $this->Cell(60, 10, 'Tax Invoice', 1, 2, 'C', TRUE);
        $this->Ln(0);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(120);
        $this->Cell(20, 10, 'Order No:', 1, 0);
        $this->Cell(40, 10, $this->_order[0]['customer_order_id'], 1, 2, 'C');
        $this->Ln(0);
        $this->SetFont('Arial', '', 8);
        $this->Cell(-5);
        $this->Cell(8, 20, 'ABN: ' . $abn['value']);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(117);
        $this->Cell(20, 10, 'Date:', 1, 0);
        $this->Cell(40, 10, $this->_order[0]['customer_order_date'], 1, 1, 'C');
        $this->SetTextColor(0, 0, 0);
        
        $this->Ln(10);
    }
    
    /**
     * Prints the body of the Invoice
     *
     * @access public
     */
    public function Body()
    {
        $this->AddressLabels();
        $this->ProductsTable();
        $this->Summary();
    }
    
    /**
     * Prints the Address labels
     *
     * @access public
     */
    public function AddressLabels()
    {
        $width = 88;
        $height = 5;
        $gap = 4;
        
        if (empty($this->_customer))
        {
            throw new Exception('Require a Valid Customer');
        }
        
        $this->SetFont('Arial', 'B', 8);
        $this->SetFillColor(122, 150, 150);
        $this->SetTextColor(255, 255, 255);
        $this->Ln(0);
        $this->Cell($width, $height, 'Ship to:', 1, 0, 'C', TRUE);
        $this->Cell($gap);
        $this->Cell($width, $height, 'Bill to:', 1, 1, 'C', TRUE);
        
        $this->SetFont('Arial', '', 8);
        $this->SetTextColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        
        // Customer
        if (isset($this->_customer['company_name']))
        {
            $this->Cell($width, $height, $this->_customer['company_name'], 'LR', 0, 'L');
            $this->Cell($gap);
            $this->Cell($width, $height, $this->_customer['company_name'], 'LR', 1, 'L');
        }
        else
        {
            // First & Last Name
            $this->Cell($width, $height, $this->_customer['name_first'] . ' ' . $this->_customer['name_last'], 'LR', 0, 'L');
            $this->Cell($gap);
            $this->Cell($width, $height, $this->_customer['name_first'] . ' ' . $this->_customer['name_last'], 'LR', 1, 'L');
        }
        
        // Street 1
        $this->Cell($width, $height, $this->_customer['street1'], 'LR', 0, 'L');
        $this->Cell($gap);
        $this->Cell($width, $height, $this->_customer['street1'], 'LR', 1, 'L');
        
        // Street 2
        $this->Cell($width, $height, $this->_customer['street2'], 'LR', 0, 'L');
        $this->Cell($gap);
        $this->Cell($width, $height, $this->_customer['street2'], 'LR', 1, 'L');
        
        // City
        $this->Cell($width, $height, $this->_customer['city'], 'LR', 0, 'L');
        $this->Cell($gap);
        $this->Cell($width, $height, $this->_customer['city'], 'LR', 1, 'L');
        
        // State
        $this->Cell($width, $height, $this->_customer['state'] . ' ' . $this->_customer['pcode'], 'LR', 0, 'L');
        $this->Cell($gap);
        $this->Cell($width, $height, $this->_customer['state'] . ' ' . $this->_customer['pcode'], 'LR', 1, 'L');
        
        // Country
        $this->Cell($width, $height, $this->_customer['country'], 'LRB', 0, 'L');
        $this->Cell($gap);
        $this->Cell($width, $height, $this->_customer['country'], 'LRB', 1, 'L');
    }
    
    /**
     * Prints the Product Table
     *
     * @access public
     */
    public function ProductsTable()
    {
        $height = 5;
        $product_width = 90;
        $qty_width = 25;
        $price_width = 32;
        $total_width = 33;
        
        if (empty($this->_order))
        {
            throw new Exception('Require a Valid Order');
        }
        
        $this->SetFont('Arial', 'B', 8);
        $this->SetFillColor(122, 150, 150);
        $this->SetTextColor(255, 255, 255);
        $this->Ln(5);
        $this->Cell($product_width, $height, 'Product', 1, 0, 'C', TRUE);
        $this->Cell($qty_width, $height, 'Qty', 1, 0, 'C', TRUE);
        $this->Cell($price_width, $height, 'Unit Price', 1, 0, 'C', TRUE);
        $this->Cell($total_width, $height, 'Total', 1, 1, 'C', TRUE);
        
        $this->SetFont('Arial', '', 8);
        $this->SetTextColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);
        
        for ($i = 0; $i < count($this->_order); $i++)
        {
            $price = $this->_order[$i]['price_retail'];
            $qty = $this->_order[$i]['quantity'];
            $total = $price * $qty;
            
            $this->_discountTotal += $this->_order[0]['discount'];
            $this->_invoiceTotal += $total;
            
            // Product
            if ($i == count($this->_order) - 1)
            {
                $this->Cell($product_width, $height, $this->_order[$i]['product_name'], 'LRB', 0);
            }
            else
            {
                $this->Cell($product_width, $height, $this->_order[$i]['product_name'], 'LR', 0);
            }
            
            // Qty
            if ($i == count($this->_order) - 1)
            {
                $this->Cell($qty_width, $height, $qty, 'LRB', 0, 'C');
            }
            else
            {
                $this->Cell($qty_width, $height, $qty, 'LR', 0, 'C');
            }
            
            // Unit Price
            if ($i == count($this->_order) - 1)
            {
                $this->Cell($price_width, $height, '$ ' . $price, 'LRB', 0, 'C');
            }
            else
            {
                $this->Cell($price_width, $height, '$ ' . $price, 'LR', 0, 'C');
            }
            
            // Total
            if ($i == count($this->_order) - 1)
            {
                $this->Cell($total_width, $height, '$ ' . $total, 'LRB', 1, 'C');
            }
            else
            {
                $this->Cell($total_width, $height, '$ ' . $total, 'LR', 1, 'C');
            }
        }
        
        $this->Ln(1);
    }
    
    /**
     * Prints the invoice Summary
     *
     * @access public
     */
    public function Summary()
    {
        $this->AcceptPageBreak();
        
        $this->Ln(1);
        
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(115);
        
        // Shipping
        $this->Cell(32, 5, 'Shipping:', 'LTR', 0, 'L');
        $this->Cell(33, 5, '$ ' . $this->_order[0]['customer_order_shipping'], 'LTR', 1, 'C');
        
        $this->SetFont('Arial', 'B', 8);
        
        // Discount
        $this->Cell(115);
        $this->Cell(32, 5, 'Discount:', 'LR', 0, 'L');
        $this->Cell(33, 5, '$ ' . $this->_discountTotal, 'LR', 1, 'C');
        
        // Tax
        $this->Cell(115);
        $this->Cell(32, 5, 'Tax:', 'LRB', 0, 'L');
        $this->Cell(33, 5, '$ ' . $this->_order[0]['customer_order_tax'], 'LRB', 1, 'C');
        
        $this->Ln(2);
        
        // Order Total
        $this->Cell(115);
        $this->Cell(32, 5, 'Total:', 1, 0, 'L');
        $this->Cell(33, 5, '$ ' . $this->_invoiceTotal, 1, 1, 'C');
        
        // Comments Title
        $this->SetFont('Arial', 'BI', 8);
        $this->Cell(20, 5, 'Comments:', 0, 0);
        
        // Comments
        $this->SetFont('Arial', '', 8);
        $this->Write(5, $this->_order[0]['customer_order_comments']);
    }
    
    /**
     * Prints the Invoice footer
     *
     * @access public
     */
    public function Footer()
    {
        $email = $this->_dictionary->get('settings/email');
        $website = $this->_dictionary->get('settings/website');
        
        // Position at 1.5 cm from bottom
        $this->SetY(-20);
        
        $this->SetFont('Arial', '', 8);
        
        $this->Cell(NULL, 5, $email['value'], 0, 1, 'C');
        $this->Cell(NULL, 5, $website['value'], 0, 1, 'C');
        
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        
        $this->Cell(NULL, 10, 'Page ' . $this->PageNo() . ' of ' . $this->AliasNbPages, 0, 0, 'C');
    }
    
}