<?php
/**
 * Register Gadget Customers Model
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Customers extends Jaws_Model
{
    /**
     * Retrieves all customers
     *
     * @access public
     * @return array\Jaws_Error array of customers or Jaws_Error
     */
    public function GetAllCustomers()
    {
        $sql = '
            SELECT *
            FROM [[register_customers]]
            ORDER BY
                [customer_id] ASC
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves Customer by ID
     *
     * @access public
     * @param int       $id         customer ID
     * @return array\Jaws_Error     customer array or Jaws_Error
     */
    public function GetCustomerById($id = 0)
    {
        if ($id === 0)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_CUSTOMER_ID_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_CUSTOMER_ID_ERROR'));
        }
        
        $params = array();
        $params['id']       = $id;
        
        $sql = '
            SELECT *
            FROM [[register_customers]]
            WHERE [customer_id] = {id}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
}