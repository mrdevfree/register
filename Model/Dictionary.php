<?php
/**
 * Register Gadget Dictionary Model
 *
 * Duplicate keys are not allowed - if a key already exists
 * a Jaws_Error is returned
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Model_Dictionary extends Jaws_Model
{
    /**
     * Checks whether a key exists
     *
     * @access public
     * @param string    $key    key to check for
     * @return bool\Jaws_Error  TRUE/FALSE or Jaws_Error
     */
    public function exists($key = '')
    {
        if ($key == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_INVALID_KEY_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_INVALID_KEY_ERROR'));
        }
        
        $params = array();
        $params['key']      = $key;
        
        $sql = '
            SELECT *
            FROM [[register_dictionary]]
            WHERE [key] = {key}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return count($result) > 0 ? TRUE : FALSE;
    }
    
    /**
     * Retrieves all key-value pairs from
     * the dictionary
     *
     * @access public
     * @return array\Jaws_Error key-value pair array or Jaws_Error
     */
    public function getAll()
    {
        $sql = '
            SELECT *
            FROM [[register_dictionary]]
        ';
        
        $result = $GLOBALS['db']->queryAll($sql);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $result;
    }
    
    /**
     * Retrieves a single key-value pair from
     * the dictionary
     *
     * @access public
     * @param string    $key    key to search for
     * @return array\Jaws_Error key-value pair array or Jaws_Error
     */
    public function get($key = '')
    {
        if ($key == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_INVALID_KEY_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_INVALID_KEY_ERROR'));
        }
        
        $params = array();
        $params['key']      = $key;
        
        $sql = '
            SELECT *
            FROM [[register_dictionary]]
            WHERE [key] = {key}
        ';
        
        $result = $GLOBALS['db']->queryRow($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return count($result) > 0 ? $result : NULL;
    }
    
    /**
     * Updates value of Key
     *
     * @access public
     * @param string    $key    existing key
     * @param string    $value  new value
     * @return bool\Jaws_Error  TRUE or Jaws_Error
     */
    public function set($key = '', $value = '')
    {
        if ($key == '' || $value == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_INVALID_KEY_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_INVALID_KEY_ERROR'));
        }
        
        if (is_null($this->get($key)))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_KEY_NOT_EXISTS_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_KEY_NOT_EXISTS_ERROR'));
        }
        
        $params = array();
        $params['key']      = $key;
        $params['value']    = $value;
        $params['updated']  = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            UPDATE [[register_dictionary]]
            SET
                [value]             = {value},
                [date_updated]      = {updated}
            WHERE [key] = {key}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return TRUE;
    }
    
    /**
     * Creates a New Key-Value pair
     *
     * @access public
     * @param string    $key    existing key
     * @param string    $value  new value
     * @return bool\Jaws_Error  TRUE or Jaws_Error
     */
    public function createKey($key = '', $value = '')
    {
        if ($key == '' || $value == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_INVALID_KEY_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_INVALID_KEY_ERROR'));
        }
        
        if (!is_null($this->get($key)))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_KEY_NOT_EXISTS_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_KEY_NOT_EXISTS_ERROR'));
        }
        
        $params = array();
        $params['key']      = $key;
        $params['value']    = $value;
        $params['created']  = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $params['updated']  = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        
        $sql = '
            INSERT INTO [[register_dictionary]]
                ([key], [value], [date_created], [date_updated])
            VALUES
                ({key}, {value}, {created}, {updated})
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse($result->getMessage(), RESPONSE_ERROR);
            return new Jaws_Error($result->getMessage(), 'SQL');
        }
        
        return $GLOBALS['db']->lastInsertId('register_dictionary');
        
    }
    
    /**
     * Deletes a Key-Value pair
     *
     * @access public
     * @param string    $key    key to delete
     * @return string\Jaws_Error   deleted key or Jaws_Error
     */
    public function deleteKey($key = '')
    {
        if ($key == '')
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_INVALID_KEY_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_INVALID_KEY_ERROR'));
        }
        
        if (is_null($this->get($key)))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_KEY_NOT_EXISTS_ERROR'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_KEY_NOT_EXISTS_ERROR'));
        }
        
        $params = array();
        $params['key']       = $key;
        
        $sql = '
            DELETE FROM [[register_dictionary]]
            WHERE [key] = {key}
        ';
        
        $result = $GLOBALS['db']->query($sql, $params);
        if (Jaws_Error::IsError($result))
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_KEY_VAL_NOT_DELETED'), RESPONSE_ERROR);
            return new Jaws_Error(_t('REGISTER_ERROR_KEY_VAL_NOT_DELETED'));
        }
        
        return $key;
        
    }
    
}