<?php
/**
 * Register Gadget Admin HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterAdminHTML extends Jaws_GadgetHTML
{
    /**
     * Constructor
     *
     * @access public
     */
    public function RegisterAdminHTML()
    {
        $this->Init('Register');
    }

    /**
     * Calls default admin action
     *
     * @access public
     * @return string   Default Template content
     */
    public function Admin()
    {
        return $this->Summary();
    }
    
    /**
     * Constructs a menu bar for ControlPanel
     *
     * @access public
     * @param string    $action_selected the selection action string
     * @return string   MenuBar Template content
     */
    public function MenuBar($action_selected)
    {
        $actions = array(
            'Summary', 'Products', 'EditProduct', 'EditProductImage', 'UploadProductImage', 'Categories', 'EditCategory', 'Suppliers',
            'EditSupplier', 'Customers', 'EditCustomer', 'Orders', 'EditPurchaseOrder',
            'EditCustomerOrder', 'Reports', 'SelectTests', 'RunTests', 'Settings', 'UpdateSettings');
        
        if (!in_array($action_selected, $actions))
        {
            $action_selected = 'Summary';
        }
        
        require_once JAWS_PATH . 'include/Jaws/Widgets/Menubar.php';
        
        $menubar = new Jaws_Widgets_Menubar();
        
        $menubar->AddOption(
            'Summary',
            _t('REGISTER_SUMMARY'),
            BASE_SCRIPT . '?gadget=Register&amp;action=Summary',
            'images/stock/bottom.png'
        );
        
        if ($this->GetPermission('Products'))
        {
            $menubar->AddOption(
                'Products',
                _t('REGISTER_LBL_PRODUCTS'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Products',
                'images/stock/folder.png'
            );
        }
        
        if ($this->GetPermission('Categories'))
        {
            $menubar->AddOption(
                'Categories',
                _t('REGISTER_LBL_CATEGORIES'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Categories',
                'images/stock/folder.png'
            );
        }
        
        if ($this->GetPermission('Suppliers'))
        {
            $menubar->AddOption(
                'Suppliers',
                _t('REGISTER_SUPPLIERS'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Suppliers',
                'images/stock/folder.png'
            );
        }
        
        if ($this->GetPermission('Customers'))
        {
            $menubar->AddOption(
                'Customers',
                _t('REGISTER_CUSTOMERS'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Customers',
                'images/stock/folder.png'
            );
        }
        
        if ($this->GetPermission('Orders'))
        {
            $menubar->AddOption(
                'Orders',
                _t('REGISTER_ORDERS'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Orders',
                'images/stock/folder.png'
            );
        }
        
        if ($this->GetPermission('Reports'))
        {
            $menubar->AddOption(
                'Reports',
                _t('REGISTER_REPORTS'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Reports',
                'images/stock/stock-terminal.png'
            );
        }
        
        if ($this->GetPermission('Settings'))
        {
            $menubar->AddOption(
                'Settings',
                _t('REGISTER_SETTINGS'),
                BASE_SCRIPT . '?gadget=Register&amp;action=Settings',
                'images/stock/properties.png'
            );
        }
        
        $menubar->Activate($action_selected);
        
        return $menubar->Get();
    }
    
    /**
     * Summary Action
     *
     * @access public
     * @return string   Summary Template content
     */
    public function Summary()
    {
        $summaryHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Summary');
        return $summaryHTML->Summary();
    }
    
    /**
     * Products Action
     *
     * @access public
     * @return string   Products Template content
     */
    public function Products()
    {
        $productsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        return $productsHTML->Products();
    }
    
    /**
     * EditProduct Action
     *
     * @access public
     * @return string   Edit Product Template content
     */
    public function EditProduct()
    {
        $editProductsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        return $editProductsHTML->EditProduct();
    }
    
    /**
     * DeleteProduct Action
     *
     * @access public
     * @return string   Delete Product Template content
     */
    public function DeleteProduct()
    {
        $deleteProductHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        return $deleteProductHTML->DeleteProduct();
    }
    /**
     * CompleteEditProduct Action
     *
     * @access public
     */
    public function CompleteEditProduct()
    {
        $editProductsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        $editProductsHTML->CompleteEditProduct();
    }
    
    /**
     * EditProductImage Action
     *
     * @access public
     * @return string   EditImage Template content
     */
    public function EditProductImage()
    {
        $imageProductsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        return $imageProductsHTML->EditProductImage();
    }
    
    /**
     * EditProductImage Action - Stage 2 Upload
     *
     * @access public
     */
    public function UploadProductImage()
    {
        $imageProductsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        $imageProductsHTML->UploadProductImage();
    }
    
    public function TrimProductImage()
    {
        $imageProductsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Products');
        return $imageProductsHTML->TrimProductImage();
    }
    
    /**
     * Categories Action
     *
     * @access public
     * @return string   Categories Template content
     */
    public function Categories()
    {
        $categoriesHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Categories');
        return $categoriesHTML->Categories();
    }
    
    /**
     * EditCategory Action
     *
     * @access public
     * @return string   Edit Category Template content
     */
    public function EditCategory()
    {
        $editCategoriesHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Categories');
        return $editCategoriesHTML->EditCategory();
    }
    
    /**
     * DeleteCategory Action
     *
     * @access public
     * @return string   Delete Category Template content
     */
    public function DeleteCategory()
    {
        $deleteCategoryHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Categories');
        return $deleteCategoryHTML->DeleteCategory();
    }
    
    /**
     * CompleteEditCategory Action
     *
     * @access public
     */
    public function CompleteEditCategory()
    {
        $editCategoriesHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Categories');
        $editCategoriesHTML->CompleteEditCategory();
    }
    
    /**
     * Suppliers Action
     *
     * @access public
     * @return string   Suppliers Template content
     */
    public function Suppliers()
    {
        $suppliersHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Suppliers');
        return $suppliersHTML->Suppliers();
    }
    
    /**
     * EditSupplier Action
     *
     * @access public
     * @return string   Edit Suppliers Template content
     */
    public function EditSupplier()
    {
        $editSupplierHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Suppliers');
        return $editSupplierHTML->EditSupplier();
    }
    
    /**
     * CompleteEditSupplier Action
     *
     * @access public
     */
    public function CompleteEditSupplier()
    {
        $editSupplierHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Suppliers');
        $editSupplierHTML->CompleteEditSupplier();
    }
    
    /**
     * DeleteSupplier Action
     *
     * @access public
     * @return string   Delete Suppliers Template content
     */
    public function DeleteSupplier()
    {
        $deleteSupplierHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Suppliers');
        return $deleteSupplierHTML->DeleteSupplier();
    }
    
    /**
     * Customers Action
     *
     * @access public
     * @return string   Customers Template content
     */
    public function Customers()
    {
        $customersHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Customers');
        return $customersHTML->Customers();
    }
    
    /**
     * EditCustomer Action
     *
     * @access public
     * @return string   Edit Customer Template content
     */
    public function EditCustomer()
    {
        $editCustomersHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Customers');
        return $editCustomersHTML->EditCustomer();
    }
    
    /**
     * CompleteEditCustomer Action
     *
     * @access public
     */
    public function CompleteEditCustomer()
    {
        $editCustomersHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Customers');
        $editCustomersHTML->CompleteEditCustomer();
    }
    
    /**
     * DeleteCustomer Action
     *
     * @access public
     * @return string   Delete Customer Template content
     */
    public function DeleteCustomer()
    {
        $deleteCustomerHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Customers');
        return $deleteCustomerHTML->DeleteCustomer();
    }
    
    /**
     * Orders Action
     *
     * @access public
     * @return string   Orders Template content
     */
    public function Orders()
    {
        $ordersHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Orders');
        return $ordersHTML->Orders();
    }
    
    /**
     * EditPurchaseOrder Action
     *
     * @access public
     * @return string   Edit Purchase Order Template content
     */
    public function EditPurchaseOrder()
    {
        $editPurchaseOrderHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'PurchaseOrders');
        return $editPurchaseOrderHTML->EditPurchaseOrder();
    }
    
    /**
     * EditCustomerOrder Action
     *
     * @access public
     * @return string   Edit Customer Order Template content
     */
    public function EditCustomerOrder()
    {
        $editCustomerOrderHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'CustomerOrders');
        return $editCustomerOrderHTML->EditCustomerOrder();
    }
    
    /**
     * CompleteEditPurchaseOrder Action
     *
     * @access public
     */
    public function CompleteEditPurchaseOrder()
    {
        $editPurchaseOrderHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'PurchaseOrders');
        $editPurchaseOrderHTML->CompleteEditPurchaseOrder();
    }
    
    /**
     * CompleteEditCustomerOrder Action
     *
     * @access public
     */
    public function CompleteEditCustomerOrder()
    {
        $editCustomerOrderHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'CustomerOrders');
        $editCustomerOrderHTML->CompleteEditCustomerOrder();
    }
    
    /**
     * Reports Action
     *
     * @access public
     * @return string   Reports Template content
     */
    public function Reports()
    {
        $reportsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Reports');
        return $reportsHTML->Reports();
    }
    
    
    public function SelectTests()
    {
        $selectTestHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Reports');
        return $selectTestHTML->SelectTests();
    }
    /**
     * RunTests Action
     *
     * @access public
     * @return string   Run Tests Template content
     */
    public function RunTests()
    {
        $testsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Reports');
        return $testsHTML->RunTests();
    }
    
    /**
     * PSettings Action
     *
     * @access public
     * @return string   Settings Template content
     */
    public function Settings()
    {
        $settingsHTML = $GLOBALS['app']->LoadGadget('Register', 'AdminHTML', 'Settings');
        return $settingsHTML->Settings();
    }
    
}