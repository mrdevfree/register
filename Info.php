<?php
/**
 * Register Gadget
 *
 * @category    GadgetInfo
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterInfo extends Jaws_GadgetInfo
{
    /**
     * Gadget Info
     *
     * @access public
     */
    public function RegisterInfo()
    {
        parent::Init('Register');
        $this->GadgetName(_t('REGISTER_NAME'));
        $this->GadgetDescription(_t('REGISTER_DESCRIPTION'));
        $this->GadgetVersion('0.3.0');
        $this->Doc('Register');
        $this->ListURL(true);
        
        $acls = array(
            'ManageProducts',
            'ManageCategories',
            'ManageCustomers',
            'ManageSuppliers',
            'ManageSettings',
            'ManageOrders',
        );
        
        $this->PopulateACLs($acls);
        $this->Requires('ControlPanel');
        
    }
}