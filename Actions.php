<?php
/**
 * Register Actions file
 *
 * @category    GadgetActions
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */

$actions = array();

/* Admin Actions */
$actions['Summary']                     = array('AdminAction');

$actions['Products']                    = array('AdminAction');
$actions['EditProduct']                 = array('AdminAction');
$actions['CompleteEditProduct']         = array('AdminAction');
$actions['DeleteProduct']               = array('AdminAction');

$actions['EditProductImage']            = array('AdminAction');
$actions['UploadProductImage']          = array('AdminAction');
$actions['TrimProductImage']            = array('AdminAction');

$actions['Categories']                  = array('AdminAction');
$actions['EditCategory']                = array('AdminAction');
$actions['CompleteEditCategory']        = array('AdminAction');
$actions['DeleteCategory']              = array('AdminAction');

$actions['Suppliers']                   = array('AdminAction');
$actions['EditSupplier']                = array('AdminAction');
$actions['CompleteEditSupplier']        = array('AdminAction');
$actions['DeleteSupplier']              = array('AdminAction');

$actions['Customers']                   = array('AdminAction');
$actions['EditCustomer']                = array('AdminAction');
$actions['CompleteEditCustomer']        = array('AdminAction');
$actions['DeleteCustomer']              = array('AdminAction');

$actions['Orders']                      = array('AdminAction');
$actions['EditPurchaseOrder']           = array('AdminAction');
$actions['CompleteEditPurchaseOrder']   = array('AdminAction');
$actions['EditCustomerOrder']           = array('AdminAction');
$actions['CompleteEditCustomerOrder']   = array('AdminAction');

$actions['Reports']                     = array('AdminAction');
$actions['SelectTests']                 = array('AdminAction');
$actions['RunTests']                    = array('AdminAction');

$actions['Settings']                    = array('AdminAction');
$actions['UpdateSettings']              = array('AdminAction');
$actions['CompleteEditSettings']        = array('AdminAction');