<?php

require_once JAWS_PATH . 'include/Jaws/Ajax.php';

/**
 * Register AJAX API
 *
 * @category   Ajax
 * @package    Register
 * @author     Tom Kaczocha <tom@crazydev.org>
 * @copyright  2012 Tom Kaczocha
 * @license    http://www.gnu.org/copyleft/gpl.html
 */
class RegisterAdminAjax extends Jaws_Ajax
{
    /**
     * Returns a key value array with product id and retail price
     *
     * @access public
     * @param string    $product_ids    string of product IDs
     * @return string   JSON encoded product string
     */
    public function GetProductPrices($product_ids)
    {
        $result = array();
        $done = array();
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $ids = explode(',', $product_ids);
        
        foreach ($ids as $id)
        {
            if ($id != '' && is_numeric($id))
            {
                if (!in_array($id, $done)){
                    $product = $model->GetProductByID($id);
                    $result[] = array(
                        'product_id' => $id,
                        'price' => $product['price_retail'],
                    );
                    
                    $done[] = $id;
                }
                
            }
        }
        
        return json_encode($result);
    }
    
    /**
     * Deletes product by ID
     *
     * @access public
     * @param   int         $id   product ID
     * @return  mixed       product ID or error message
     */
    public function DeleteProduct($id = 0)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
        $model->DeleteProduct($id);
        
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Deletes category by ID
     *
     * @access public
     * @param   int         $id   product ID
     * @return  array       response array
     */
    public function DeleteCategory($id = 0)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Categories');
        
        $model->DeleteCategory($id);
        
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Deletes supplier by ID
     *
     * @access public
     * @param   int         $id   product ID
     * @return  array       response array
     */
    public function DeleteSupplier($id = 0)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Suppliers');
        
        $model->DeleteSupplier($id);
        
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Deletes customer by ID
     *
     * @access public
     * @param   int         $id   product ID
     * @return  array       response array
     */
    public function DeleteCustomer($id = 0)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Customers');
        
        $model->DeleteCustomer($id);
        
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Delete purchase order by ID
     *
     * @access public
     * @param   int         $id     purchase order ID
     * @return  mixed       deleted purchase order ID or error message
     */
    public function DeletePurchaseOrder($id = 0)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        
        $model->DeletePurchaseOrder($id);
        
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Delete customer order by ID
     *
     * @access public
     * @param   int     $id     customer order ID
     * @return  mixed   deleted customer order ID or error message
     */
    public function DeleteCustomerOrder($id = 0)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        
        $model->DeleteCustomerOrder($id);
        
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Saves application settings to database
     *
     * @access public
     * @param string    $string     key-value pairs
     * @return array    response array
     */
    public function SaveSettings($string)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $environment = $model->getUrlValue($string, 'environment');
        $company = $model->getUrlValue($string, 'company');
        $abn = $model->getUrlValue($string, 'abn');
        $email = $model->getUrlValue($string, 'email');
        $website = $model->getUrlValue($string, 'website');
        $phone = $model->getUrlValue($string, 'phone');
        $fax = $model->getUrlValue($string, 'fax');
        $street1 = $model->getUrlValue($string, 'street1');
        $street2 = $model->getUrlValue($string, 'street2');
        $city = $model->getUrlValue($string, 'city');
        $state = $model->getUrlValue($string, 'state');
        $pcode = $model->getUrlValue($string, 'pcode');
        $country = $model->getUrlValue($string, 'country');
        
        $dictionary->set('settings/environment', $environment);
        $dictionary->set('settings/company', $company != '' ? $company : ' ');
        $dictionary->set('settings/abn', $abn != '' ? $abn : ' ');
        $dictionary->set('settings/email', $email != '' ? $email : ' ');
        $dictionary->set('settings/website', $website != '' ? $website : ' ');
        $dictionary->set('settings/phone', $phone != '' ? $phone : ' ');
        $dictionary->set('settings/fax', $fax != '' ? $fax : ' ');
        $dictionary->set('settings/street1', $street1 != '' ? $street1 : ' ');
        $dictionary->set('settings/street2', $street2 != '' ? $street2 : ' ');
        $dictionary->set('settings/city', $city != '' ? $city : ' ');
        $dictionary->set('settings/state', $state != '' ? $state : ' ');
        $dictionary->set('settings/pcode', $pcode != '' ? $pcode : ' ');
        $dictionary->set('settings/country', $country != '' ? $country : ' ');
        
        $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_SETTINGS_UPDATED'), RESPONSE_NOTICE);
        return $GLOBALS['app']->Session->PopLastResponse();
    }
    
    /**
     * Saves Product String to Dictionary
     *
     * @access public
     * @param string    $key    key
     * @param string    $value  value
     */
    public function SaveKeyValueToDictionary($key, $value)
    {
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $result = $dictionary->createKey($key, $value);
        
        if ($result instanceof Jaws_Error)
        {
            $dictionary->set($key, $value);
        }
    }
    
    /**
     * Gets Order Price Total
     *
     * @access public
     * @param string    $orderKey       dictionary key to order
     * @param string    $productsKey    dictionary key to products
     * @return mixed    order total or error message
     */
    public function CalculateCustomerOrderTotal($orderKey, $productsKey)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $order = $dictionary->get($orderKey);
        $products = $dictionary->get($productsKey);
        
        if ($order != NULL && $products != NULL)
        {
            return $model->calculateCustomerOrderTotal($order['value'], $products['value']);
        }
        
        return _t('REGISTER_ERROR_CALCULATING_PRICE');
    }
    
    /**
     * Adds Tests to run To Dictionary
     *
     * @access public
     * @param   string      $tests      '$' deliminated string
     */
    public function addTestsToDictionary($tests)
    {
        $key = 'tests';
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        if ($dictionary->exists($key))
        {
            return $dictionary->set($key, $tests);
        }
        else
        {
            return $dictionary->createKey($key, $tests);
        }
        
    }
    
    /**
     * Retrieves tests from dictionary
     *
     * @access public
     * @return string       '$' deliminated string of tests
     */
    public function getTestsFromDictionary()
    {
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $testResult = $dictionary->get('tests');
        
        return $testResult['value'];
    }
    
    /**
     * Runs the specified test case
     *
     * @access public
     * @param string    $testCase   test class name
     * @return mixed    test result array or false on failure
     */
    public function runTest($testCase)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $admin = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        
        $suite = $model->initTestSuite();
        
        $testCases = $suite->getTestCases();
        
        $results = array();
        
        foreach($testCases as $case)
        {
            if (get_class($case) == $testCase)
            {
                $admin->createTestDatabase();
                $case->setMulti();
                $case->run();
                $res = $case->getResults();
                $admin->removeTestDatabase();
                
                foreach ($res as $result)
                {
                    $results[] = array(
                        'class_name' => $result->GetClass(),
                        'test_name' => $result->getName(),
                        'status' => $result->GetStatus(),
                        'time' => $result->GetTime(),
                        'message' => $result->GetMessage()
                    );
                }
                
                return $results;
            }
        }
        
        return false;
    }
    
    /**
     * Saves Image Edit Figures to Dictionary
     * 
     * @param   int         $x1         first x point
     * @param   int         $y1         first y point
     * @param   int         $width      image width
     * @param   int         $height     image height
     */
    public function SaveCropToDictionary($image, $x, $y, $width, $height)
    {
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $key = $image;
        $value = '';
        
        $value .= 'x=' . $x;
        $value .= '&y=' . $y;
        $value .= '&width=' . $width;
        $value .= '&height=' . $height;
        
        if ($dictionary->exists($key))
        {
            $dictionary->set($key, $value);
            return;
        }
        
        $dictionary->createKey($key, $value);
    }
    
    /**
     * Creates a New Image
     *
     * @access public
     * @param   string      $image      image name
     * @return  bool        TRUE or FALSE if fails
     */
    public function CompleteImageCrop($image = '', $filename = NULL)
    {
        include_once JAWS_PATH . 'include/Jaws/Image.php';
        
        try
        {
            $objImage = Jaws_Image::factory();
            if (Jaws_Error::IsError($objImage))
            {
                return $objImage->GetMessage();
            }
            
            $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
            $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
            
            $imageData = $dictionary->get($image);
            
            if ($imageData == null)
            {
                return false;
            }
            
            $width = $model->getUrlValue($imageData['value'], 'width');
            $height = $model->getUrlValue($imageData['value'], 'height');
            $x = $model->getUrlValue($imageData['value'], 'x');
            $y = $model->getUrlValue($imageData['value'], 'y');
            
            $elements = explode('/', $image);
            
            // load image
            $result = $objImage->load('gadgets/Register/images/products/temp/' . end($elements));
            if (!$result)
            {
                return $result;
            }
            
            // crop image
            $result = $objImage->crop($width, $height, $x, $y);
            if (Jaws_Error::IsError($result))
            {
                return $result->GetMessage();
            }
            
            // save image
            if ($filename == NULL)
            {
                $result = $objImage->save('gadgets/Register/images/products/origin/' . end($elements));
            }
            else
            {
                //return 'gadgets/Register/images/products/origin/' . $filename;
                $result = $objImage->save('gadgets/Register/images/products/origin/' . $filename);
            }
            
            if (Jaws_Error::IsError($result))
            {
                return $result->GetMessage();
            }
            
            // unload image
            $objImage->free();
            
            $dictionary->deleteKey($image);
            
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
        
    }
    
}