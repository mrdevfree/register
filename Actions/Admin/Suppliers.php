<?php
/**
 * Register Gadget Admin Suppliers HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Suppliers extends RegisterAdminHTML
{
    /**
     * Suppliers Action
     *
     * @access public
     * @return string   parsed template
     */
    public function Suppliers()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Suppliers');
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $this->CheckPermission('Suppliers');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Suppliers.html');
        $tpl->SetBlock('suppliers');
        $tpl->SetVariable('menubar', $this->MenuBar('Suppliers'));
        
        $btnAddSupplier =& Piwi::createWidget('Button', 'btn_add', _t('REGISTER_SUPPLIER'), STOCK_ADD);
        $btnAddSupplier->SetStyle('float:right;');
        $btnAddSupplier->AddEvent(
            ON_CLICK,
            "javascript:
                window.location = 'admin.php?gadget=Register&action=EditSupplier';
            "
        );
        $tpl->SetVariable('add_supplier', $btnAddSupplier->Get());
        
        // Process Suppliers
        $tpl->SetBlock('suppliers');
        $tpl->SetVariable('name', _t('REGISTER_LBL_NAME'));
        $tpl->SetVariable('count', _t('REGISTER_LBL_PRODUCTS'));
        $tpl->SetVariable('visible', _t('REGISTER_LBL_VISIBLE'));
        $tpl->SetVariable('actions', _t('REGISTER_LBL_ACTIONS'));
        
        $suppliers = $model->GetAllSuppliers();
        
        if (($count = count($suppliers, COUNT_NORMAL)) > 0)
        {
            $bg = '';
            $tpl->SetVariable('no-supplier', 'none');
            foreach ($suppliers as $supplier)
            {
                $tpl->SetBlock('suppliers/supplier-row');
                $bg = Jaws_Utils::RowColor($bg);
                $id = $supplier['supplier_id'];
                $name = $supplier['company_name'];
                $count = $p_model->GetProductCountBySupplier($id);
                $vis = $supplier['supplier_visible'] == 0 ? 'No' : 'Yes';
                
                $editButton =& Piwi::createWidget('Button', 'edit', '', STOCK_EDIT);
                $editButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditSupplier&id=" . $id . "';
                    "
                );
                
                $delButton =& Piwi::createWidget('Button', 'delete', '', STOCK_DELETE);
                $delButton->AddEvent(
                    ON_CLICK,
                    "if (confirm('Are you sure you want to delete this supplier?'))
                    {
                        delete_supplier(" . $id . ");
                    }"
                );
                
                $tpl->SetVariable('id', $id);
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('name', $name);
                $tpl->SetVariable('count', $count);
                $tpl->SetVariable('visible', $vis);
                $tpl->SetVariable('edit_button', $editButton->Get());
                $tpl->SetVariable('delete_button', $delButton->Get());
                $tpl->ParseBlock('suppliers/supplier-row');
            }
        }
        else
        {
            $tpl->SetVariable('no-supplier', 'block');
        }
        
        $tpl->ParseBlock('suppliers');
        
        return $tpl->Get();
    }
    
    /**
     * Edit Supplier Action
     *
     * @access public
     * @return string   parsed template
     */
    public function EditSupplier()
    {
        $supplier = null;
        
        $base_model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Suppliers');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $this->CheckPermission('EditSupplier');
        $this->AjaxMe('script.js');
        
        if (isset($_GET['id'])) {
            $supplier = $model->GetSupplierById($_GET['id']);
        }
        
        // get supplier if exists in dictionary
        if ($dictionary->exists('edit_supplier'))
        {
            $supplier = array();
            $str = $dictionary->get('edit_supplier');
            $str = urldecode($str['value']);
            $supplier['supplier_id'] = 0;
            $supplier['company_name'] = $base_model->getUrlValue($str, 'name');
            $supplier['abn'] = $base_model->getUrlValue($str, 'abn');
            $supplier['supplier_visible'] = $base_model->getUrlValue($str, 'visible') == 'true' ? TRUE : FALSE;
            $supplier['email'] = $base_model->getUrlValue($str, 'email');
            $supplier['website'] = $base_model->getUrlValue($str, 'website');
            $supplier['rep_first_name'] = $base_model->getUrlValue($str, 'f_name');
            $supplier['rep_last_name'] = $base_model->getUrlValue($str, 'l_name');
            $supplier['phone'] = $base_model->getUrlValue($str, 'phone');
            $supplier['fax'] = $base_model->getUrlValue($str, 'fax');
            $supplier['mobile'] = $base_model->getUrlValue($str, 'mobile');
            $supplier['street1'] = $base_model->getUrlValue($str, 'street1');
            $supplier['street2'] = $base_model->getUrlValue($str, 'street2');
            $supplier['city'] = $base_model->getUrlValue($str, 'city');
            $supplier['state'] = $base_model->getUrlValue($str, 'state');
            $supplier['pcode'] = $base_model->getUrlValue($str, 'pcode');
            $supplier['country'] = $base_model->getUrlValue($str, 'country');
        }
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditSupplier.html');
        $tpl->SetBlock('edit_supplier');
        $tpl->SetVariable('menubar', $this->MenuBar('Suppliers'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'EditSupplier');
        $id = $supplier == null ? 0 : $supplier['supplier_id'];
        $tpl->SetVariable('id', $id);
        
        //==================================================================================
        // Supplier Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_company', _t('REGISTER_LBL_COMPANY'));
        
        $tpl->SetVariable('lbl_comp_name', _t('REGISTER_LBL_NAME'));
        $company_name =& Piwi::createWidget('Entry', 'txt_company', $supplier['company_name']);
        $company_name->SetStyle('width:300px;');
        $company_name->SetTitle(_t('REGISTER_LBL_NAME'));
        $company_name->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_company', $company_name->Get());
        
        //==================================================================================
        // Supplier ABN
        //==================================================================================
        
        $tpl->SetVariable('lbl_abn', _t('REGISTER_LBL_ABN'));
        $abn =& Piwi::createWidget('Entry', 'txt_abn', $supplier['abn']);
        $abn->SetStyle('width:150px;');
        $abn->SetTitle(_t('REGISTER_LBL_ABN'));
        $abn->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_abn', $abn->Get());
        
        //==================================================================================
        // Supplier Visibility
        //==================================================================================
        
        $vis = $supplier != null ? $supplier['supplier_visible'] : true;
        $chk_visible =& Piwi::createWidget('CheckButtons', 'visibility');
        $chk_visible->AddOption(_t('REGISTER_LBL_VISIBLE'), 'chk_visible', 'visibility', $vis);
        $chk_visible->AddEvent(
            ON_CLICK,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        $tpl->SetVariable('visible', $chk_visible->Get());
        
        //==================================================================================
        // Supplier Email
        //==================================================================================
        
        $tpl->SetVariable('lbl_email', _t('REGISTER_LBL_EMAIL'));
        $email =& Piwi::createWidget('Entry', 'txt_email', $supplier['email']);
        $email->SetStyle('width:300px');
        $email->SetTitle(_t('REGISTER_LBL_EMAIL'));
        $email->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_email', $email->Get());
        
        //==================================================================================
        // Supplier Website
        //==================================================================================
        
        $tpl->SetVariable('lbl_website', _t('REGISTER_LBL_WEBSITE'));
        $website =& Piwi::createWidget('Entry', 'txt_website', $supplier['website']);
        $website->SetStyle('width:300px');
        $website->SetTitle(_t('REGISTER_LBL_WEBSITE'));
        $website->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_website', $website->Get());
        
        //==================================================================================
        // Supplier Rep Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_rep', _t('REGISTER_LBL_COMPANY_REP'));
        
        //==================================================================================
        // Supplier Rep First Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_rep_first_name', _t('REGISTER_LBL_FIRST_NAME'));
        $rep_first =& Piwi::createWidget('Entry', 'txt_rep_first_name', $supplier['rep_first_name']);
        $rep_first->SetStyle('width:150px');
        $rep_first->SetTitle(_t('REGISTER_LBL_FIRST_NAME'));
        $rep_first->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_rep_first_name', $rep_first->Get());
        
        //==================================================================================
        // Supplier Rep Last Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_rep_last_name', _t('REGISTER_LBL_LAST_NAME'));
        $rep_last =& Piwi::createWidget('Entry', 'txt_rep_last_name', $supplier['rep_last_name']);
        $rep_last->SetStyle('width:150px');
        $rep_last->SetTitle(_t('REGISTER_LBL_LAST_NAME'));
        $rep_last->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_rep_last_name', $rep_last->Get());
        
        //==================================================================================
        // Supplier Phone
        //==================================================================================
        
        $tpl->SetVariable('lbl_phone', _t('REGISTER_LBL_PHONE'));
        $phone =& Piwi::createWidget('Entry', 'txt_phone', $supplier['phone']);
        $phone->SetStyle('width:150px');
        $phone->SetTitle(_t('REGISTER_LBL_PHONE'));
        $phone->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_phone', $phone->Get());
        
        //==================================================================================
        // Supplier Fax
        //==================================================================================
        
        $tpl->SetVariable('lbl_fax', _t('REGISTER_LBL_FAX'));
        $fax =& Piwi::createWidget('Entry', 'txt_fax', $supplier['fax']);
        $fax->SetStyle('width:150px');
        $fax->SetTitle(_t('REGISTER_LBL_FAX'));
        $fax->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_fax', $fax->Get());
        
        //==================================================================================
        // Supplier Mobile
        //==================================================================================
        
        $tpl->SetVariable('lbl_mobile', _t('REGISTER_LBL_MOBILE'));
        $mobile =& Piwi::createWidget('Entry', 'txt_mobile', $supplier['mobile']);
        $mobile->SetStyle('width:150px');
        $mobile->SetTitle(_t('REGISTER_LBL_MOBILE'));
        $mobile->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_mobile', $mobile->Get());
        
        //==================================================================================
        // Supplier Address
        //==================================================================================
        
        $tpl->SetVariable('lbl_address', _t('REGISTER_LBL_ADDRESS'));
        
        //==================================================================================
        // Supplier Street1
        //==================================================================================
        
        $tpl->SetVariable('lbl_street1', _t('REGISTER_LBL_STREET'));
        $street1 =& Piwi::createWidget('Entry', 'txt_street1', $supplier['street1']);
        $street1->SetStyle('width:300px');
        $street1->SetTitle(_t('REGISTER_LBL_STREET'));
        $street1->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_street1', $street1->Get());
        
        //==================================================================================
        // Supplier Street2
        //==================================================================================
        
        $street2 =& Piwi::createWidget('Entry', 'txt_street2', $supplier['street2']);
        $street2->SetStyle('width:300px');
        $street2->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        $tpl->SetVariable('txt_street2', $street2->Get());
        
        //==================================================================================
        // Supplier City
        //==================================================================================
        
        $tpl->SetVariable('lbl_city', _t('REGISTER_LBL_CITY'));
        $city =& Piwi::createWidget('Entry', 'txt_city', $supplier['city']);
        $city->SetStyle('width:200px');
        $city->SetTitle(_t('REGISTER_LBL_CITY'));
        $city->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_city', $city->Get());
        
        //==================================================================================
        // Supplier State
        //==================================================================================
        
        $tpl->SetVariable('lbl_state', _t('REGISTER_LBL_STATE'));
        $state =& Piwi::createWidget('Entry', 'txt_state', $supplier['state']);
        $state->SetStyle('width:150px');
        $state->SetTitle(_t('REGISTER_LBL_STATE'));
        $state->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_state', $state->Get());
        
        //==================================================================================
        // Supplier Post Code
        //==================================================================================
        
        $tpl->SetVariable('lbl_pcode', _t('REGISTER_LBL_PCODE'));
        $pcode =& Piwi::createWidget('Entry', 'txt_pcode', $supplier['pcode']);
        $pcode->SetStyle('width:150px');
        $pcode->SetTitle(_t('REGISTER_LBL_PCODE'));
        $pcode->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_pcode', $pcode->Get());
        
        //==================================================================================
        // Supplier Country
        //==================================================================================
        
        $tpl->SetVariable('lbl_country', _t('REGISTER_LBL_COUNTRY'));
        $country =& Piwi::createWidget('Entry', 'txt_country', $supplier['country']);
        $country->SetStyle('width:300px');
        $country->SetTitle(_t('REGISTER_LBL_COUNTRY'));
        $country->AddEvent(
            ON_CHANGE,
            "javascript:
                save_supplier_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_country', $country->Get());
        
        //==================================================================================
        // Save Button
        //==================================================================================
        
        $btnSave =& Piwi::createWidget('Button', 'save', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $btnSave->AddEvent(
            ON_CLICK,
            "javascript:
            if (this.form.elements['txt_company'].value == '') {
                alert('" . _t('REGISTER_SUPPLIER_MISSING_NAME') . "');
                this.form.elements['name'].focus();
            } else {
                this.form.submit();
            }"
        );
        
        $tpl->SetVariable('save_button', $btnSave->Get());
        
        $tpl->ParseBlock('edit_supplier');
        
        return $tpl->Get();
    }
    
    /**
     * Processes EditSupplier form
     *
     * @access public
     */
    public function CompleteEditSupplier()
    {
        $this->CheckPermission('EditSupplier');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Suppliers');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $request =& Jaws_Request::getInstance();
        $names = array('id', 'txt_company', 'txt_abn', 'visibility', 'txt_email',
                       'txt_website', 'txt_rep_first_name', 'txt_rep_last_name',
                       'txt_phone', 'txt_fax', 'txt_mobile', 'txt_street1', 'txt_street2',
                       'txt_city', 'txt_state', 'txt_pcode', 'txt_country');
        
        $post = $request->get($names, 'post');
        
        if ($post['id'] > 0)
        {
            $model->UpdateSupplier($post['id'], $post['txt_company'], $post['txt_abn'], $post['txt_rep_first_name'], $post['txt_rep_last_name'],
                                   $post['txt_phone'], $post['txt_fax'], $post['txt_mobile'], $post['txt_email'], $post['txt_website'],
                                   $post['txt_street1'], $post['txt_street2'], $post['txt_city'], $post['txt_state'], $post['txt_pcode'],
                                   $post['txt_country'], isset($post['visibility']));
        }
        else{
            $model->SaveNewSupplier($post['txt_company'], $post['txt_abn'], $post['txt_rep_first_name'], $post['txt_rep_last_name'],
                                    $post['txt_phone'], $post['txt_fax'], $post['txt_mobile'], $post['txt_email'], $post['txt_website'],
                                    $post['txt_street1'], $post['txt_street2'], $post['txt_city'], $post['txt_state'], $post['txt_pcode'],
                                    $post['txt_country'], isset($post['visibility']));
        }
        
        $dictionary->deleteKey('edit_supplier');
        
        if ($dictionary->exists('edit_product'))
        {
            $id = $GLOBALS['db']->lastInsertId('register_suppliers');
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditProduct&supplier=' . $id);
        }
        
        if ($dictionary->exists('edit_purchase_order'))
        {
            $id = $GLOBALS['db']->lastInsertId('register_suppliers');
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditPurchaseOrder&supplier=' . $id);
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Suppliers');
    }
    
    /**
     * Delete Supplier Action
     *
     * @access public
     * @return string   parsed template
     */
    //public function DeleteSupplier()
    //{
    //    $this->CheckPermission('DeleteSupplier');
    //    $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Suppliers');
    //    $admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Suppliers');
    //    
    //    $request =& Jaws_Request::getInstance();
    //    $post = $request->get(array('id', 'step'), 'post');
    //    
    //    if (!is_null($post['id']) && $post['step'] == 'delete')
    //    {
    //        $admin_model->DeleteSupplier($post['id']);
    //        
    //        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Suppliers');
    //    }
    //    
    //    $get = $request->get(array('id', 'action'), 'get');
    //    
    //    // Ask for delete confirmation
    //    $tpl = new Jaws_Template('gadgets/Register/templates/');
    //    $tpl->Load('DeleteSupplier.html');
    //    $tpl->SetBlock('delete_supplier');
    //    
    //    $tpl->SetVariable('base_script', BASE_SCRIPT);
    //    $tpl->SetVariable('menubar', $this->MenuBar('EditSupplier'));
    //    $tpl->SetVariable('delete_message', _t('REGISTER_SUPPLIER_DELETE_MESSAGE'));
    //    
    //    // Delete Button
    //    $deleteButton =& Piwi::createWidget('Button', 'delete', _t('GLOBAL_DELETE'), STOCK_DELETE);
    //    $deleteButton->SetSubmit();
    //    $tpl->SetVariable('delete_button', $deleteButton->Get());
    //    
    //    // Cancel Button
    //    $cancelButton =& Piwi::createWidget('Button', 'cancel', _t('GLOBAL_CANCEL'), STOCK_CANCEL);
    //    $cancelButton->AddEvent(ON_CLICK,
    //        "javascript:
    //            this.form.action.value = 'Suppliers';
    //            this.form.submit();
    //        "
    //    );
    //    
    //    $tpl->SetVariable('cancel_button', $cancelButton->Get());
    //    $tpl->SetVariable('id', $get['id']);
    //    
    //    $supplier = $model->GetSupplierById($get['id']);
    //    
    //    if ($supplier == null)
    //    {
    //        return new Jaws_Error(_t('REGISTER_FAILED_GETTING_SUPPLIER'));
    //    }
    //    
    //    $tpl->SetVariable('name', $supplier['company_name']);
    //    $tpl->SetVariable('lbl_created', _t('REGISTER_LBL_CREATED'));
    //    
    //    $date = $GLOBALS['app']->loadDate();
    //    $tpl->SetVariable('created_date', $date->Format($supplier['date_created']));
    //    
    //    $tpl->ParseBlock('delete_supplier');
    //    
    //    return $tpl->Get();
    //}
}