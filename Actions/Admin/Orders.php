<?php
/**
 * Register Gadget Admin Orders HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Orders extends RegisterAdminHTML
{
    /**
     * Edit Orders Action
     *
     * @access public
     * @return string   parsed template
     */
    public function Orders()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        
        $this->CheckPermission('Orders');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Orders.html');
        $tpl->SetBlock('orders');
        $tpl->SetVariable('menubar', $this->MenuBar('Orders'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'Orders');
        
        $btnAddPurchaseOrder =& Piwi::createWidget('Button', 'btn_add_p_order', _t('REGISTER_LBL_PURCHASE_ORDER'), STOCK_ADD);
        $btnAddPurchaseOrder->SetStyle('float:right;');
        $btnAddPurchaseOrder->AddEvent(
            ON_CLICK,
            "javascript:
                window.location = 'admin.php?gadget=Register&action=EditPurchaseOrder';
            "
        );
        
        $tpl->SetVariable('add_purchase_order', $btnAddPurchaseOrder->Get());
        
        $btnAddCustomerOrder =& Piwi::createWidget('Button', 'btn_add_c_order', _t('REGISTER_LBL_CUSTOMER_ORDER'), STOCK_ADD);
        $btnAddCustomerOrder->SetStyle('float:right;');
        $btnAddCustomerOrder->AddEvent(
            ON_CLICK,
            "javascript:
                window.location = 'admin.php?gadget=Register&action=EditCustomerOrder';
            "
        );
        
        $tpl->SetVariable('add_customer_order', $btnAddCustomerOrder->Get());
        
        // Process Customer Orders
        $tpl->SetBlock('orders/customer_orders');
        $tpl->SetVariable('customer_orders', _t('REGISTER_LBL_CUSTOMER_ORDERS'));
        $tpl->SetVariable('customer', _t('REGISTER_CUSTOMER'));
        $tpl->SetVariable('date', _t('REGISTER_LBL_DATE'));
        $tpl->SetVariable('lbl_status', _t('REGISTER_LBL_STATUS'));
        $tpl->SetVariable('total', _t('REGISTER_LBL_ORDER_TOTAL'));
        $tpl->SetVariable('actions', _t('REGISTER_LBL_ACTIONS'));
        
        $orders = $model->GetAllCustomerOrders();
        
        if (($count = count($orders, COUNT_NORMAL)) > 0)
        {
            $bg = '';
            $tpl->SetVariable('no-order', 'none');
            foreach ($orders as $order)
            {
                $tpl->SetBlock('orders/customer_orders/order-row');
                $bg = Jaws_Utils::RowColor($bg);
                $id = $order['customer_order_id'];
                $customer = $order['name_first'] . ' ' . $order['name_last'];
                $total = $order['customer_order_total'];
                $date = $GLOBALS['app']->LoadDate();
                $date = $date->Format($order['customer_order_date']);
                $status = $order['status_name'];
                
                $editButton =& Piwi::createWidget('Button', 'edit', '', STOCK_EDIT);
                $editButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditCustomerOrder&id=" . $id . "';
                    "
                );
                
                $delButton =& Piwi::createWidget('Button', 'delete', '', STOCK_DELETE);
                $delButton->AddEvent(
                    ON_CLICK,
                    "if (confirm('Are you sure you want to delete this customer order?'))
                    {
                        delete_customer_order(" . $id . ");
                    }"
                );
                
                $tpl->SetVariable('id', $id);
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('customer', $customer);
                $tpl->SetVariable('date', $date);
                $tpl->SetVariable('status', $status);
                $tpl->SetVariable('total', $total);
                $tpl->SetVariable('edit_button', $editButton->Get());
                $tpl->SetVariable('delete_button', $delButton->Get());               
                $tpl->ParseBlock('orders/customer_orders/order-row');
            }
        }
        else
        {
            $tpl->SetVariable('no-order', 'block');
        }
        
        $tpl->ParseBlock('orders/customer_orders');
        
        // Process Purchase Orders
        $tpl->SetBlock('orders/purchase_orders');
        $tpl->SetVariable('purchase_orders', _t('REGISTER_LBL_PURCHASE_ORDERS'));
        $tpl->SetVariable('supplier', _t('REGISTER_SUPPLIER'));
        $tpl->SetVariable('date', _t('REGISTER_LBL_DATE'));
        $tpl->SetVariable('total', _t('REGISTER_LBL_ORDER_TOTAL'));
        $tpl->SetVariable('actions', _t('REGISTER_LBL_ACTIONS'));
        $tpl->SetVariable('lbl_status', _t('REGISTER_LBL_STATUS'));
        
        $orders = $model->GetAllPurchaseOrders();
        
        if (($count = count($orders, COUNT_NORMAL)) > 0)
        {
            $bg = '';
            $tpl->SetVariable('no-order', 'none');
            foreach ($orders as $order)
            {
                $tpl->SetBlock('orders/purchase_orders/order-row');
                $bg = Jaws_Utils::RowColor($bg);
                $id = $order['purchase_order_id'];
                $supplier = $order['company_name'];
                $total = $order['purchase_order_total'];
                $date = $GLOBALS['app']->LoadDate();
                $date = $date->Format($order['purchase_order_date']);
                $status = $order['status_name'];
                
                $editButton =& Piwi::createWidget('Button', 'edit', '', STOCK_EDIT);
                $editButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditPurchaseOrder&id=" . $id . "';
                    "
                );
                
                $delButton =& Piwi::createWidget('Button', 'delete', '', STOCK_DELETE);
                $delButton->AddEvent(
                    ON_CLICK,
                    "if (confirm('Are you sure you want to delete this purchase order?'))
                    {
                        delete_purchase_order(" . $id . ");
                    }"
                );
                
                $tpl->SetVariable('id', $id);
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('supplier', $supplier);
                $tpl->SetVariable('date', $date);
                $tpl->SetVariable('status', $status);
                $tpl->SetVariable('total', $total);
                $tpl->SetVariable('edit_button', $editButton->Get());
                $tpl->SetVariable('delete_button', $delButton->Get());
                $tpl->ParseBlock('orders/purchase_orders/order-row');
            }
        }
        else
        {
            $tpl->SetVariable('no-order', 'block');
        }
        
        $tpl->ParseBlock('orders/purchase_orders');
        
        $tpl->ParseBlock('orders');
        
        return $tpl->Get();
    }
}