<?php
/**
 * Register Gadget Admin Categories HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Categories extends RegisterAdminHTML
{
    /**
     * Categories Action
     *
     * @access public
     * @return string   parsed template
     */
    public function Categories()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Categories');
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $this->CheckPermission('Products');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Categories.html');
        $tpl->SetBlock('categories');
        $tpl->SetVariable('menubar', $this->MenuBar('Categories'));
        
        $btnAddCategory =& Piwi::createWidget('Button', 'btn_add', _t('REGISTER_CATEGORY'), STOCK_ADD);
        $btnAddCategory->SetStyle('float:right;');
        $btnAddCategory->AddEvent(
            ON_CLICK,
            "javascript:
                window.location = 'admin.php?gadget=Register&action=EditCategory';
            "
        );
        $tpl->SetVariable('add_category', $btnAddCategory->Get());
        
        // Process Categories
        $tpl->SetBlock('categories');
        $tpl->SetVariable('name', _t('REGISTER_LBL_NAME'));
        $tpl->SetVariable('desc', _t('REGISTER_LBL_DESCRIPTION'));
        $tpl->SetVariable('count', _t('REGISTER_LBL_COUNT'));
        $tpl->SetVariable('visible', _t('REGISTER_LBL_VISIBLE'));
        $tpl->SetVariable('actions', _t('REGISTER_LBL_ACTIONS'));
        
        $categories = $model->GetAllCategories();
        
        if (($count = count($categories, COUNT_NORMAL)) > 0)
        {
            $bg = '';
            $tpl->SetVariable('no-category', 'none');
            foreach ($categories as $category)
            {
                $tpl->SetBlock('categories/category-row');
                $bg = Jaws_Utils::RowColor($bg);
                $id = $category['category_id'];
                $name = $category['category_name'];
                $description = $category['description'];
                $count = $p_model->GetProductCountByCategory($id);
                $vis = $category['category_visible'] == 0 ? 'No' : 'Yes';
                
                $imageButton =& Piwi::createWidget('Button', 'image', '', STOCK_IMAGE);
                $imageButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditCategoryImage&id=" . $id . "';
                    "
                );
                
                $editButton =& Piwi::createWidget('Button', 'edit', '', STOCK_EDIT);
                $editButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditCategory&id=" . $id . "';
                    "
                );
                
                $delButton =& Piwi::createWidget('Button', 'delete', '', STOCK_DELETE);
                $delButton->AddEvent(
                    ON_CLICK,
                    "if (confirm('Are you sure you want to delete this category?'))
                    {
                        delete_category(" . $id . ");
                    }"
                );
                
                $tpl->SetVariable('id', $id);
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('name', $name);
                $tpl->SetVariable('description', $description);
                $tpl->SetVariable('count', $count);
                $tpl->SetVariable('visible', $vis);
                $tpl->SetVariable('image_button', $imageButton->Get());
                $tpl->SetVariable('edit_button', $editButton->Get());
                $tpl->SetVariable('delete_button', $delButton->Get());
                $tpl->ParseBlock('categories/category-row');
            }
        }
        else
        {
            $tpl->SetVariable('no-category', 'block');
        }
        
        $tpl->ParseBlock('categories');
        
        return $tpl->Get();
    }
    
    /**
     * Prepares the add category management view
     *
     * @access public
     * @return string   parsed template
     */
    public function EditCategory()
    {
        $category = null;
        $key = 'edit_category';
        
        $base_model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Categories');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $this->CheckPermission('EditCategory');
        $this->AjaxMe('script.js');
        
        if (isset($_GET['id'])) {
            $category = $model->GetCategoryById($_GET['id']);
        }
        
        if ($dictionary->exists($key))
        {
            $product = array();
            $str = $dictionary->get($key);
            $str = urldecode($str['value']);
            $category['category_id'] = 0;
            $category['category_name'] = $base_model->getUrlValue($str, 'name');
            $category['category_visible'] = $base_model->getUrlValue($str, 'visible') == 'true' ? TRUE : FALSE;
            $category['description'] = $base_model->getUrlValue($str, 'description');
        }
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditCategory.html');
        $tpl->SetBlock('add_category');
        $tpl->SetVariable('menubar', $this->MenuBar('Categories'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'EditCategory');
        $id = $category == null ? 0 : $category['category_id'];
        $tpl->SetVariable('id', $id);
        
        //==================================================================================
        // Category Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_name', _t('REGISTER_LBL_NAME'));
        $c_name =& Piwi::CreateWidget('Entry', 'name', $category['category_name']);
        $c_name->setStyle('width: 350px;');
        $c_name->AddEvent(
            ON_CLICK,
            "javascript:
                save_category_to_dictionary();
            "
        );
        
        $tpl->SetVariable('name', $c_name->Get());
        
        //==================================================================================
        // Category Visibility
        //==================================================================================
        
        $v = $category != null ? $category['category_visible'] : true;
        $chk_visible =& Piwi::createWidget('CheckButtons', 'visibility');
        $chk_visible->AddEvent(
            ON_CLICK,
            "javascript:
                save_category_to_dictionary();
            "
        );
        $chk_visible->AddOption(_t('REGISTER_LBL_VISIBLE'), 'chk_visible', 'visibility', $v);
        $tpl->SetVariable('visibility', $chk_visible->Get());
        
        //==================================================================================
        // Category Description
        //==================================================================================
        
        $tpl->SetVariable('lbl_short_desc', _t('REGISTER_LBL_SHORT_DESC'));
        $short_desc =& $GLOBALS['app']->LoadEditor('Register', 'short_desc_block', $category['description'], false);
        $short_desc->setId('short_desc_block');
        $short_desc->TextArea->SetRows(4);
        $short_desc->TextArea->SetStyle('width: 100%;');
        $short_desc->SetWidth('96%');
        $short_desc->TextArea->AddEvent(
            ON_CHANGE,
            "javascript:
                save_category_to_dictionary();
            "
        );
        $tpl->SetVariable('short_desc_block', $short_desc->Get());
        
        //==================================================================================
        // Save Button
        //==================================================================================
        
        $btnSave =& Piwi::createWidget('Button', 'save', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $btnSave->AddEvent(
            ON_CLICK,
            "javascript:
            if (this.form.elements['name'].value == '') {
                alert('" . _t('REGISTER_CATEGORY_MISSING_NAME') . "');
                this.form.elements['name'].focus();
            } else if (this.form.elements['short_desc_block'].value == '') {
                alert('" . _t('REGISTER_PRODUCT_MISSING_SHORT_DESC') . "');
                this.form.elements['short_desc_block'].focus();
            } else {
                this.form.submit();
            }"
        );
        
        $tpl->SetVariable('save_button', $btnSave->Get());
        
        $tpl->ParseBlock('add_category');
        
        return $tpl->Get();
    }
    
    /**
     * Retrieves all form data and passes
     * it to AdminModel for processing
     *
     * @access public
     */
    public function CompleteEditCategory()
    {
        $this->CheckPermission('AddCategory');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Categories');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $request =& Jaws_Request::getInstance();
        $names = array('id', 'name', 'visibility', 'short_desc_block');
        
        $post = $request->get($names, 'post');
        
        if ($post['id'] > 0)
        {
            $model->UpdateCategory($post['id'], $post['name'], isset($post['visibility']), $post['short_desc_block']);
        }
        else{
            $model->SaveNewCategory($post['name'], isset($post['visibility']), $post['short_desc_block']);
        }
        
        $dictionary->deleteKey('edit_category');
        
        if ($dictionary->exists('edit_product'))
        {
            $id = $GLOBALS['db']->lastInsertId('register_categories');
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditProduct&category=' . $id);
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Categories');
    }
}