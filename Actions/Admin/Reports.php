<?php
/**
 * Register Gadget Admin Reports HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Reports extends RegisterAdminHTML
{
    /**
     * Reports Action
     *
     * @access public
     * @return string   parsed template
     */
    public function Reports()
    {
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        
        $this->CheckPermission('Reports');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Reports.html');
        $tpl->SetBlock('reports');
        $tpl->SetVariable('menubar', $this->MenuBar('Reports'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'Reports');
        
        $tpl->ParseBlock('reports');
        
        return $tpl->Get();
    }
    
    public function SelectTests()
    {
        $this->CheckPermission('RunTests');
        $this->AjaxMe('script.js');
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('TestSelection.html');
        $tpl->SetBlock('tests');
        $tpl->SetVariable('menubar', $this->MenuBar('Reports'));
        
        $tpl->SetVariable('select', _t('REGISTER_SELECT'));
        $tpl->SetVariable('name', _t('REGISTER_TEST_CASE'));
        
        $selectAll =& Piwi::createWidget('CheckButtons', 'all_tests', '');
        $selectAll->SetId('all_tests');
        $selectAll->AddOption('', 'all');
        $selectAll->SetTitle('Select All');
        $selectAll->AddEvent(
            ON_CLICK,
            "javascript:
                toggle_select_all_tests();
            "
        );
        
        $tpl->SetVariable('select_all', $selectAll->Get());
        
        $suite = $model->initTestSuite();
        $testCases = $suite->getTestCases();
        
        if (count($testCases) > 0)
        {
            $bg = '';
            foreach ($testCases as $case)
            {
                $tpl->SetBlock('tests/test-row');
                $bg = Jaws_Utils::RowColor($bg);
                
                //===============================================================================
                // ADD Select Checkboxes
                //===============================================================================
                
                $sel_chk =& Piwi::createWidget('CheckButtons', 'select_buttons[]', '');
                $sel_chk->SetId('select_button');
                $sel_chk->AddOption('', '1', 'selected');
                
                $tpl->SetVariable('chk', $sel_chk->Get());
                
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('name', get_class($case));
                
                $tpl->ParseBlock('tests/test-row');
            }
        }
        
        $runBtn =& Piwi::createWidget('Button', 'run_button', _t('REGISTER_RUN_TESTS'), STOCK_SAVE);
        $runBtn->SetStyle('margin-top:20px;float:right;');
        $runBtn->AddEvent(
            ON_CLICK,
            "javascript:
                if (validate_tests_selected())
                {
                    window.location = 'admin.php?gadget=Register&action=RunTests';
                }
            "
        );
        
        $tpl->SetVariable('run_button', $runBtn->Get());
        
        $tpl->ParseBlock('tests');
        
        return $tpl->Get();
    }
    
    /**
     * Run Tests Action
     *
     * @access public
     * @return string   parsed template
     */
    public function RunTests()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $admin = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $this->CheckPermission('RunTests');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('TestResults.html');
        $tpl->SetBlock('tests');
        $tpl->SetVariable('menubar', $this->MenuBar('Reports'));
        
        $suite = $model->initTestSuite();
        $cases = $suite->getTestCases();
        
        $testsResult = $dictionary->get('tests');
        $testsToRun = explode('$', $testsResult['value']);
        
        $tpl->SetVariable('name', _t('REGISTER_TEST_NAME'));
        $tpl->SetVariable('result', _t('REGISTER_RESULT'));
        $tpl->SetVariable('lbl_time', _t('REGISTER_TIME'));
        
        $total_time = 0;
        
        foreach ($cases as $case)
        {
            if (!in_array(get_class($case), $testsToRun))
            {
                continue;
            }
            
            //$results = $case->getResults();
            $tests = $case->getTests();
            
            if (count($tests) > 0)
            {
                $tpl->SetBlock('tests/case_row');
                $tpl->SetVariable('class_display', 'block');
                $tpl->SetVariable('class_name', get_class($case));
            }
            
            //var_dump($tests); die();
            
            foreach ($tests as $test)
            {
                $tpl->SetBlock('tests/case_row/test-row');
                //$time = $result->GetTime();
                //$total_time += $time;
                //
                //$status = $result->GetStatus();
                $color = '#ffffff';
                
                //if ($status == 0)
                //{
                //    $status = 'FAIL';
                //    $color = 'red';
                //}
                //else if ($status == 1)
                //{
                //    $status = 'Pass';
                //    $color = 'green';
                //}
                //else if ($status == -1)
                //{
                //    $status = '';
                //    $color = 'orange';
                //}
                
                $tpl->SetVariable('bgcolor', $color);
                $tpl->SetVariable('name', $test);
                $tpl->SetVariable('time_ext', 'none');
                //$tpl->SetVariable('result', $status);
                $tpl->SetVariable('display-error', 'none');
                //$tpl->SetVariable('message', $result->GetMessage());
                $tpl->ParseBlock('tests/case_row/test-row');
            }
            
            if (count($tests) > 0)
            {
                $tpl->ParseBlock('tests/case_row');
            }
        }
        
        //$tpl->SetVariable('lbl_total_time', _t('REGISTER_TOTAL_EXECUTION_TIME'));
        //$tpl->SetVariable('total_time', $total_time . ' sec');
        
        $tpl->ParseBlock('tests');
        
        return $tpl->Get();
        
    }
    
}