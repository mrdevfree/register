<?php
/**
 * Register Gadget Admin Summary HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Summary extends RegisterAdminHTML
{
    /**
     * Prepares the summary management view
     *
     * @access public
     * @return string   parsed template
     */
    public function Summary()
    {
        $show_products = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_products');
        $show_categories = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_categories');
        $show_suppliers = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_suppliers');
        $show_customers = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_customers');
        $show_orders = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_orders');
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Summary.html');
        $tpl->SetBlock('summary');
        $tpl->SetVariable('menubar', $this->MenuBar('Summary'));
        
        if ($show_products === 'true')
        {
            
        }
        
        if ($show_categories === 'true')
        {
            
        }
        
        if ($show_suppliers === 'true')
        {
                    
        }
        
        if ($show_customers === 'true')
        {
            
        }
        
        if ($show_orders === 'true')
        {
            
        }
        
        $tpl->ParseBlock('summary');
        
        return $tpl->Get();
    }
}