<?php
/**
 * Register Gadget Admin Purchase Orders HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_PurchaseOrders extends RegisterAdminHTML
{
    private $_orderKey = 'edit_purchase_order';
    private $_productsKey = 'edit_purchase_order_products';
    
    /**
     * Edit Purchase Order Action
     *
     * @access public
     * @return string   parsed template
     */
    public function EditPurchaseOrder()
    {
        $p_order = array();
        $order = null;
        
        $base_model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $s_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Suppliers');
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $date = $GLOBALS['app']->LoadDate();
        
        $this->CheckPermission('EditPurchaseOrder');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditPurchaseOrder.html');
        $tpl->SetBlock('purchase_order');
        $tpl->SetVariable('menubar', $this->MenuBar('Orders'));
        
        $tpl->SetVariable('script', BASE_SCRIPT . '?gadget=Register&action=CompleteEditPurchaseOrder');
        $tpl->SetVariable('action', '');
        
        if (isset($_GET['id'])) {
            $order = $model->GetPurchaseOrderById($_GET['id']);
            $p_order['reference_no'] = $order[0]['reference_no'];
            $p_order['date'] = $order[0]['purchase_order_date'];
            $p_order['supplier_id'] = $order[0]['supplier_id'];
            $p_order['status'] = $order[0]['purchase_order_status_id'];
            $p_order['purchase_order_comments'] = $order[0]['purchase_order_comments'];
            $p_order['total'] = $order[0]['purchase_order_total'];
            $p_order['shipping'] = $order[0]['purchase_order_shipping'];
            $p_order['discount'] = $order[0]['purchase_order_discount'];
            $p_order['tax'] = $order[0]['purchase_order_tax'];
            $p_order['added'] = $order[0]['qty_added'];
            $tpl->SetVariable('id', $order[0]['purchase_order_id']);
        }
        
        if ($dictionary->exists($this->_orderKey))
        {
            $p_order = array();
            $str = $dictionary->get($this->_orderKey);
            $str = urldecode($str['value']);
            $p_order['purchase_order_id'] = 0;
            $p_order['supplier_id'] = $base_model->getUrlValue($str, 'supplier_id');
            $p_order['reference_no'] = $base_model->getUrlValue($str, 'ref');
            $p_order['date'] = $base_model->getUrlValue($str, 'date');
            $p_order['status'] = $base_model->getUrlValue($str, 'status');
            $p_order['purchase_order_comments'] = $base_model->getUrlValue($str, 'comments');
            $p_order['shipping'] = $base_model->getUrlValue($str, 'shipping');
            $p_order['discount'] = $base_model->getUrlValue($str, 'discount');
            $p_order['added'] = $base_model->getUrlValue($str, 'added');
            $p_order['tax'] = $base_model->getUrlValue($str, 'tax');
        }
        
        if (isset($_GET['supplier']))
        {
            $p_order['supplier_id'] = $_GET['supplier'];
        }
        
        if (!isset($p_order['added']))
        {
            $p_order['added'] = 0;
        }
        
        $tpl->SetVariable('added', $p_order['added']);
        
        $tpl->SetVariable('title', _t('REGISTER_LBL_PURCHASE_ORDER'));
        
        //==================================================================================
        // Supplier
        //==================================================================================
        
        $tpl->SetVariable('lbl_supplier', _t('REGISTER_SUPPLIER'));
        
        $suppliers = $s_model->GetAllSuppliers();
        
        $suppCombo =& Piwi::createWidget('Combo', 'suppliers');
        $suppCombo->setId('suppliers');
        $suppCombo->SetStyle('width:200px;');
        $suppCombo->AddOption('', '');
        $suppCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
            "
        );
        
        if (($count = count($suppliers)) > 0)
        {
            foreach ($suppliers as $supplier)
            {
                $suppCombo->AddOption($supplier['company_name'], $supplier['supplier_id']);
            }
        }
        
        if (!isset($p_order['supplier_id']))
        {
            $p_order['supplier_id'] = '';
        }
        
        $suppCombo->setDefault($p_order['supplier_id']);
        
        $tpl->SetVariable('suppliers', $suppCombo->Get());
        
        //==================================================================================
        // Add Supplier Button
        //==================================================================================
        
        $btnAddSupplier =& Piwi::createWidget('Button', 'btnAddSupplier', _t('REGISTER_SUPPLIER'), STOCK_ADD);
        $btnAddSupplier->AddEvent(
            ON_CLICK,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                window.location = 'admin.php?gadget=Register&action=EditSupplier';
            "
        );
        $tpl->SetVariable('add_supplier', $btnAddSupplier->Get());
        
        //==================================================================================
        // Date - DatePicker
        //==================================================================================
        
        $tpl->SetVariable('lbl_calander', _t('REGISTER_LBL_DATE'));
        
        if (!isset($p_order['date']))
        {
            $p_order['date'] = $date->Format(time(), 'Y-m-d');
        }
        
        $dateCal =& Piwi::createWidget('DatePicker', 'order_date', $p_order['date']);
        $dateCal->SetId('order_date');
        $dateCal->showTimePicker(true);
        $dateCal->setDateFormat('%Y-%m-%d');
        $dateCal->setLanguageCode($GLOBALS['app']->Registry->Get('/config/calendar_language'));
        $dateCal->setCalType($GLOBALS['app']->Registry->Get('/config/calendar_type'));
        $dateCal->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
            "
        );
        
        $tpl->SetVariable('calender', $dateCal->Get());
        
        //==================================================================================
        // Reference No
        //==================================================================================
        
        $tpl->SetVariable('lbl_reference_no', _t('REGISTER_LBL_REFERENCE_NO'));
        
        if (!isset($p_order['reference_no']))
        {
            $p_order['reference_no'] = '';
        }
        
        $ref_no =& Piwi::createWidget('Entry', 'ref_no', $p_order['reference_no']);
        $ref_no->SetId('ref_no');
        $ref_no->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
            "
        );
        
        $tpl->SetVariable('reference_no', $ref_no->Get());
        
        //==================================================================================
        // Order Status
        //==================================================================================
        
        $tpl->SetVariable('lbl_status', _t('REGISTER_LBL_STATUS'));
        $statusCombo =& Piwi::createWidget('Combo', 'status');
        $statusCombo->SetID('status');
        $statusCombo->SetStyle('width:175px;');
        $statusCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
            "
        );
        
        if (!isset($p_order['status']))
        {
            $p_order['status'] = 1;
        }
        
        $statusCodes = $model->GetOrderStatusList();
        
        foreach ($statusCodes as $code)
        {
            $statusCombo->AddOption($code['status_name'], $code['order_status_id']);
        }
        
        $statusCombo->SetDefault($p_order['status']);
        
        $tpl->SetVariable('status', $statusCombo->Get());
        
        //==================================================================================
        // Add Row Button
        //==================================================================================
        
        $addRow =& Piwi::createWidget('Button', 'addRow', _t('REGISTER_ADD_ROW'), STOCK_ADD);
        $addRow->AddEvent(
            ON_CLICK,
            "javascript:
                add_purchase_order_row();
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                calculate_p_order_total();
            "
        );
        
        $tpl->SetVariable('add_row', $addRow->Get());
        
        //==================================================================================
        // Delete Row Button
        //==================================================================================
        
        $deleteRow =& Piwi::createWidget('Button', 'deleteRow', _t('REGISTER_DELETE_ROW'), STOCK_DELETE);
        $deleteRow->AddEvent(
            ON_CLICK,
            "javascript:
                delete_purchase_order_row();
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                calculate_p_order_total();
            "
        );
        
        $tpl->SetVariable('delete_row', $deleteRow->Get());
        
        //==================================================================================
        // Products Table
        //==================================================================================
        
        $tpl->SetVariable('lbl_select', _t('REGISTER_SELECT'));
        $tpl->SetVariable('lbl_product_name', _t('REGISTER_LBL_PRODUCT_NAME'));
        $tpl->SetVariable('lbl_qty', _t('REGISTER_LBL_QTY'));
        $tpl->SetVariable('lbl_item_price', _t('REGISTER_ITEM_PRICE'));
        $tpl->SetVariable('lbl_discount', _t('REGISTER_DISCOUNT'));
        
        //==================================================================================
        // Add Product Button
        //==================================================================================
        
        $newProduct =& Piwi::createWidget('Button', 'newProduct', _t('REGISTER_PRODUCT'), STOCK_ADD);
        $newProduct->AddEvent(
            ON_CLICK,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                window.location = 'admin.php?gadget=Register&action=EditProduct';
            "
        );
        
        $tpl->SetVariable('new_product', $newProduct->Get());
        
        $products = $p_model->GetAllProducts();
        
        $selectedProducts = array();
        $inputQty = array();
        $inputPrice = array();
        $inputDiscounts = array();
        
        // if ID is set -> get order data from #order
        if (isset($_GET['id']))
        {
            $prodCount = count($order);
            
            foreach ($order as $item)
            {
                $selectedProducts[] = $item['product_id'];
                $inputQty[] = $item['quantity'];
                $inputPrice[] = $item['item_price'];
                $inputDiscounts[] = $item['discount'];
            }
        }
        
        if ($dictionary->exists($this->_productsKey))
        {
            $str = $dictionary->get($this->_productsKey);
            $str = urldecode($str['value']);
            $selectedProducts = $base_model->getSelectedArray($str, 'product');
            $inputQty = $base_model->getSelectedArray($str, 'qty');
            $inputPrice = $base_model->getSelectedArray($str, 'price');
            $inputDiscounts = $base_model->getSelectedArray($str, 'discount');
        }
        
        $countValues = count($selectedProducts);
        
        if (count($selectedProducts) == 0) {
            $countValues = 1;
        }
        
        // order total holder
        $orderTotal = 0;
        
        for ($i = 0; $i < $countValues; $i++)
        {
            $tpl->SetBlock('purchase_order/order_item');
            
            //==============================================================================
            // Check Boxes
            //==============================================================================
            
            $sel_chk =& Piwi::createWidget('CheckButtons', 'select_buttons[]', '');
            $sel_chk->SetId('select_button');
            $sel_chk->AddOption('', '1', 'selected');
            
            $tpl->SetVariable('chk', $sel_chk->Get());
            
            //==============================================================================
            // Products Combo
            //==============================================================================
            
            $productCombo =& Piwi::createWidget('Combo', 'products[]', '');
            $productCombo->SetStyle('width:200px;');
            $productCombo->AddOption('', '');
            $productCombo->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_purchase_order_to_dictionary();
                    save_purchase_order_products_to_dictionary();
                    calculate_p_order_total();
                "
            );
            
            if (($count = count($products)) > 0)
            {
                foreach ($products as $product)
                {
                    $productCombo->AddOption($product['product_name'], $product['product_id']);
                }
            }
            
            if (!isset($selectedProducts[$i]))
            {
                $selectedProducts[$i] = '';
            }
            
            $productCombo->SetDefault($selectedProducts[$i]);
            
            $tpl->SetVariable('products', $productCombo->Get());
            
            //==============================================================================
            // Quantity Field
            //==============================================================================
            
            if (!isset($inputQty[$i]))
            {
                $inputQty[$i] = 1;
            }
            
            $qty_field =& Piwi::createWidget('Entry', 'qty[]', $inputQty[$i]);
            $qty_field->SetStyle('width:50px; text-align:center;');
            $qty_field->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_purchase_order_to_dictionary();
                    save_purchase_order_products_to_dictionary();
                    calculate_p_order_total();
                "
            );
            
            $tpl->SetVariable('qty', $qty_field->Get());
            
            if (!isset($inputPrice[$i]))
            {
                $inputPrice[$i] = 0.00;
            }
            
            //==============================================================================
            // Prices Field
            //==============================================================================
            
            $price_field =& Piwi::createWidget('Entry', 'prices[]', $inputPrice[$i]);
            $price_field->SetStyle('width:50px; text-align:center;');
            $price_field->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_purchase_order_to_dictionary();
                    save_purchase_order_products_to_dictionary();
                    calculate_p_order_total();
                "
            );
            
            $tpl->SetVariable('item_price', $price_field->Get());
            
            //==============================================================================
            // Discounts Field
            //==============================================================================
            
            if (!isset($inputDiscounts[$i]))
            {
                $inputDiscounts[$i] = 0.00;
            }
            
            $discountField =& Piwi::createWidget('Entry', 'discounts[]', $inputDiscounts[$i]);
            $discountField->SetStyle('width:50px; text-align:center;');
            $discountField->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_purchase_order_to_dictionary();
                    save_purchase_order_products_to_dictionary();
                    calculate_p_order_total();
                "
            );
            
            $prodPrice = (($inputQty[$i] * $inputPrice[$i]) - $inputDiscounts[$i]);
            $orderTotal += $prodPrice;
            
            $tpl->SetVariable('item_discount', $discountField->Get());
            
            $tpl->ParseBlock('purchase_order/order_item');
        }
        
        $tpl->SetVariable('lbl_shipping', _t('REGISTER_SHIPPING'));
        $tpl->SetVariable('lbl_comments', _t('REGISTER_COMMENTS'));
        $tpl->SetVariable('lbl_discount', _t('REGISTER_DISCOUNT'));
        $tpl->SetVariable('lbl_tax', _t('REGISTER_TAX'));
        $tpl->SetVariable('lbl_total', _t('REGISTER_LBL_ORDER_TOTAL'));
        
        if (!isset($p_order['shipping']))
        {
            $p_order['shipping'] = 0.00;
        }
        
        //==================================================================================
        // Shipping Field
        //==================================================================================
        
        $shippingField =& Piwi::createWidget('Entry', 'shipping', $p_order['shipping']);
        $shippingField->SetStyle('width:50px;text-align:center;');
        $shippingField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                calculate_p_order_total();
            "
        );
        
        $tpl->SetVariable('shipping', $shippingField->Get());
        
        if (!isset($p_order['discount']))
        {
            $p_order['discount'] = 0.00;
        }
        
        //==================================================================================
        // Discount Field
        //==================================================================================
        
        $discountField =& Piwi::createWidget('Entry', 'discount', $p_order['discount']);
        $discountField->SetStyle('width:50px;text-align:center;');
        $discountField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                calculate_p_order_total();
            "
        );
        
        $tpl->SetVariable('discount', $discountField->Get());
        
        //==================================================================================
        // Tax Field
        //==================================================================================
        
        if (!isset($p_order['tax']))
        {
            $p_order['tax'] = 0;
        }
        
        $taxField =& Piwi::createWidget('Entry', 'tax', $p_order['tax']);
        $taxField->SetStyle('width:50px;text-align:center;');
        $taxField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
                calculate_p_order_total();
            "
        );
        
        $tpl->SetVariable('tax', $taxField->Get());
        
        $orderTotal = ($orderTotal + $p_order['shipping'] + $p_order['tax']) - $p_order['discount'];
        
        $tpl->SetVariable('total', '$ ' . $base_model->buildPrice($orderTotal));
        
        if (!isset($p_order['purchase_order_comments']))
        {
            $p_order['purchase_order_comments'] = '';
        }
        
        //==================================================================================
        // Comments TextArea
        //==================================================================================
        
        $commentsField =& Piwi::createWidget('TextArea', 'comments', urldecode($p_order['purchase_order_comments']));
        $commentsField->SetRows(4);
        $commentsField->SetColumns(20);
        $commentsField->SetStyle('width:330px;');
        $commentsField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_purchase_order_to_dictionary();
                save_purchase_order_products_to_dictionary();
            "
        );
        
        $tpl->SetVariable('comments_field', $commentsField->Get());
        
        //==================================================================================
        // Cancel Button
        //==================================================================================
        
        $cancelBtn =& Piwi::createWidget('Button', 'cancelBtn', _t('GLOBAL_CANCEL'), STOCK_CANCEL);
        $cancelBtn->AddEvent(
            ON_CLICK,
            "javascript:
                //clear_purchase_order();
                window.location = 'admin.php?gadget=Register&action=Orders';
            "
        );
        
        $tpl->SetVariable('cancel_button', $cancelBtn->Get());
        
        //==================================================================================
        // Save Button
        //==================================================================================
        
        $saveBtn =& Piwi::createWidget('Button', 'saveBtn', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $saveBtn->AddEvent(
            ON_CLICK,
            "javascript:
                if (validate_purchase_order_form() === true)
                {
                    //clear_purchase_order();
                    this.form.submit();
                }
            "
        );
        
        $tpl->SetVariable('save_button', $saveBtn->Get());
        
        $tpl->ParseBlock('purchase_order');
        
        return $tpl->Get();
    }
    
    /**
     * Processes Edit Purchase Order Form
     *
     * @access public
     */
    public function CompleteEditPurchaseOrder()
    {
        $this->CheckPermission('EditPurchaseOrder');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $request =& Jaws_Request::getInstance();
        $names = array(
            'id', 'suppliers', 'order_date', 'ref_no', 'status', 'products', 'qty',
            'prices', 'discounts', 'added', 'removed', 'comments', 'discount', 'tax', 'shipping');
        
        $post = $request->get($names, 'post');
        
        if ($post['id'] > 0)
        {
            $model->UpdatePurchaseOrder(
                $post['id'], $post['suppliers'], $post['order_date'], $post['ref_no'], $post['status'],
                $post['products'], $post['qty'], $post['prices'], $post['discounts'], $post['added'], $post['removed'],
                $post['comments'], $post['discount'], $post['tax'], $post['shipping']);
        }
        else{
            $model->SaveNewPurchaseOrder(
                $post['suppliers'], $post['order_date'], $post['ref_no'], $post['status'],
                $post['products'], $post['qty'], $post['prices'], $post['discounts'], $post['comments'],
                $post['discount'], $post['tax'], $post['shipping']);
        }
        
        if ($dictionary->exists($this->_orderKey))
        {
            $dictionary->deleteKey($this->_orderKey);
        }
        if ($dictionary->exists($this->_productsKey))
        {
            $dictionary->deleteKey($this->_productsKey);
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Orders');
    }
    
}