<?php
/**
 * Register Gadget Admin Settings HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Settings extends RegisterAdminHTML
{
    /**
     * Prepares the Settings management view
     *
     * @access public
     * @return string   parsed template
     */
    public function Settings()
    {
        /**
         * SETTINGS to Store
         * * Show Products
         * * Show Categories
         * * Show Suppliers
         * * Show Customers
         * * Show Orders
         */
        $this->CheckPermission('Settings');
        $this->AjaxMe('script.js');
        
        $settings = array();
        
        $show_products = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_products');
        $show_categories = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_categories');
        $show_suppliers = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_suppliers');
        $show_customers = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_customers');
        $show_orders = $GLOBALS['app']->Registry->Get('/gadgets/Register/show_orders');
        
        $category = null;
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Settings.html');
        $tpl->SetBlock('settings');
        $tpl->SetVariable('menubar', $this->MenuBar('Settings'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'UpdateSettings');
        
        //==================================================================================
        // Environment Field
        //==================================================================================
        
        $result = $dictionary->get('settings/environment');
        
        $tpl->SetVariable('lbl_environment', _t('REGISTER_ENVIRONMENT'));
        
        $env_combo =& Piwi::createWidget('Combo', 'environment', '');
        $env_combo->SetStyle('width:150px;');
        $env_combo->AddOption(_t('REGISTER_LBL_DEVELOPMENT'), 1);
        $env_combo->AddOption(_t('REGISTER_LBL_PRODUCTION'), 2);
        
        $env_combo->SetDefault($result['value']);
        
        $tpl->SetVariable('select_environment', $env_combo->Get());
        $tpl->SetVariable('environment_production', _t('REGISTER_ENVIRONMENT_PRODUCTION'));
        $tpl->SetVariable('environment_development', _t('REGISTER_ENVIRONMENT_DEVELOPMENT'));
        
        $tpl->SetVariable('lbl_company', _t('REGISTER_LBL_COMPANY'));
        
        //==================================================================================
        // Company Name Field
        //==================================================================================
        
        $result = $dictionary->get('settings/company');
        
        $tpl->SetVariable('lbl_company_name', _t('REGISTER_LBL_COMPANY_NAME'));
        $tpl->SetVariable('company_info', _t('REGISTER_COMPANY_INFO'));
        
        $field_company =& Piwi::createWidget('Entry', 'company', $result['value']);
        $field_company->SetStyle('width:80%;');
        
        $tpl->SetVariable('field_company', $field_company->Get());
        
        //==================================================================================
        // Company ABN Field
        //==================================================================================
        
        $result = $dictionary->get('settings/abn');
        
        $tpl->SetVariable('lbl_abn', _t('REGISTER_LBL_ABN'));
        
        $field_abn =& Piwi::createWidget('Entry', 'abn', $result['value']);
        $field_abn->SetStyle('width:40%;');
        
        $tpl->SetVariable('field_abn', $field_abn->Get());
        
        //==================================================================================
        // Company Email Field
        //==================================================================================
        
        $result = $dictionary->get('settings/email');
        
        $tpl->SetVariable('lbl_email', _t('REGISTER_LBL_EMAIL'));
        $field_email =& Piwi::createWidget('Entry', 'email', $result['value']);
        $field_email->SetStyle('width:80%;');
        
        $tpl->SetVariable('txt_email', $field_email->Get());
        
        //==================================================================================
        // Company Website Field
        //==================================================================================
        
        $result = $dictionary->get('settings/website');
        
        $tpl->SetVariable('lbl_website', _t('REGISTER_LBL_WEBSITE'));
        $field_site =& Piwi::createWidget('Entry', 'website', $result['value']);
        $field_site->SetStyle('width:80%;');
        
        $tpl->SetVariable('txt_website', $field_site->Get());
        
        //==================================================================================
        // Company Phone Field
        //==================================================================================
        
        $result = $dictionary->get('settings/phone');
        
        $tpl->SetVariable('lbl_phone', _t('REGISTER_LBL_PHONE'));
        $field_phone =& Piwi::createWidget('Entry', 'phone', $result['value']);
        $field_phone->SetStyle('width:40%');
        
        $tpl->SetVariable('txt_phone', $field_phone->Get());
        
        //==================================================================================
        // Company Fax Field
        //==================================================================================
        
        $result = $dictionary->get('settings/fax');
        
        $tpl->SetVariable('lbl_fax', _t('REGISTER_LBL_FAX'));
        $field_fax =& Piwi::createWidget('Entry', 'fax', $result['value']);
        $field_fax->SetStyle('width:40%');
        
        $tpl->SetVariable('txt_fax', $field_fax->Get());
        
        //==================================================================================
        // Company Street 1 Field
        //==================================================================================
        
        $result = $dictionary->get('settings/street1');
        
        $tpl->SetVariable('lbl_street1', _t('REGISTER_LBL_STREET'));
        $field_street1 =& Piwi::createWidget('Entry', 'street1', $result['value']);
        $field_street1->SetStyle('width:80%');
        
        $tpl->SetVariable('txt_street1', $field_street1->Get());
        
        //==================================================================================
        // Company Street 2 Field
        //==================================================================================
        
        $result = $dictionary->get('settings/street2');
        
        $field_street2 =& Piwi::createWidget('Entry', 'street2', $result['value']);
        $field_street2->SetStyle('width:80%');
        
        $tpl->SetVariable('txt_street2', $field_street2->Get());
        
        //==================================================================================
        // Company City Field
        //==================================================================================
        
        $result = $dictionary->get('settings/city');
        
        $tpl->SetVariable('lbl_city', _t('REGISTER_LBL_CITY'));
        $field_city =& Piwi::createWidget('Entry', 'city', $result['value']);
        $field_city->SetStyle('width:50%');
        
        $tpl->SetVariable('txt_city', $field_city->Get());
        
        //==================================================================================
        // Company State Field
        //==================================================================================
        
        $result = $dictionary->get('settings/state');
        
        $tpl->SetVariable('lbl_state', _t('REGISTER_LBL_STATE'));
        $field_state =& Piwi::createWidget('Entry', 'state', $result['value']);
        $field_state->SetStyle('width:40%');
        
        $tpl->SetVariable('txt_state', $field_state->Get());
        
        //==================================================================================
        // Company PostCode Field
        //==================================================================================
        
        $result = $dictionary->get('settings/pcode');
        
        $tpl->SetVariable('lbl_pcode', _t('REGISTER_LBL_PCODE'));
        $field_pcode =& Piwi::createWidget('Entry', 'pcode', $result['value']);
        $field_pcode->SetStyle('width:40%');
        
        $tpl->SetVariable('txt_pcode', $field_pcode->Get());
        
        //==================================================================================
        // Company Country Field
        //==================================================================================
        
        $result = $dictionary->get('settings/country');
        
        $tpl->SetVariable('lbl_country', _t('REGISTER_LBL_COUNTRY'));
        $field_country =& Piwi::createWidget('Entry', 'country', $result['value']);
        $field_country->SetStyle('width:50%');
        
        $tpl->SetVariable('txt_country', $field_country->Get());
        
        
        $btnSave =& Piwi::createWidget('Button', 'btn_save', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $btnSave->SetStyle('float:right;margin-top:20px;');
        $btnSave->AddEvent(
            ON_CLICK,
            "javascript:
                save_settings();
            "
        );
        
        $tpl->SetVariable('save_button', $btnSave->Get());
        
        $tpl->ParseBlock('settings');
        
        return $tpl->Get();
    }
    
    public function UpdateSettings()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        
        $this->CheckPermission('UpdateSettings');
        $this->AjaxMe('script.js');
        
    }
    
}