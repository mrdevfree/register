<?php
/**
 * Register Gadget Admin Products HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Products extends RegisterAdminHTML
{
    /**
     * Products Action
     *
     * @access public
     * @return string   parsed template
     */
    public function Products()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $this->CheckPermission('Products');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Products.html');
        $tpl->SetBlock('products');
        $tpl->SetVariable('menubar', $this->MenuBar('Products'));
        
        //==================================================================================
        // Add Product Button
        //==================================================================================
        
        $btnAddProduct =& Piwi::createWidget('Button', 'btn_add', _t('REGISTER_PRODUCT'), STOCK_ADD);
        $btnAddProduct->SetStyle('float:right;');
        $btnAddProduct->AddEvent(
            ON_CLICK,
            "javascript:
                window.location = 'admin.php?gadget=Register&action=EditProduct';
            "
        );
        
        $tpl->SetVariable('add_product', $btnAddProduct->Get());
        
        //==================================================================================
        // Display Products
        //==================================================================================
        
        // Process Products
        $tpl->SetBlock('products');
        $tpl->SetVariable('name', _t('REGISTER_LBL_NAME'));
        $tpl->SetVariable('lbl_stock', _t('REGISTER_LBL_STOCK'));
        $tpl->SetVariable('category', _t('REGISTER_LBL_CATEGORY'));
        $tpl->SetVariable('visible', _t('REGISTER_LBL_VISIBLE'));
        $tpl->SetVariable('actions', _t('REGISTER_LBL_ACTIONS'));
        
        $products = $model->GetAllProducts();
        
        if (($count = count($products, COUNT_NORMAL)) > 0)
        {
            $bg = '';
            $tpl->SetVariable('no-product', 'none');
            foreach ($products as $product)
            {
                $tpl->SetBlock('products/product-row');
                $bg = Jaws_Utils::RowColor($bg);
                $name = $product['product_name'];
                $cat = $product['category_name'];
                $vis = $product['product_visible'] == 0 ? 'No' : 'Yes';
                $qty = $product['qty'];
                $id = $product['product_id'];
                
                $imageButton =& Piwi::createWidget('Button', 'image', '', STOCK_IMAGE);
                $imageButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditProductImage&id=" . $id . "';
                    "
                );
                
                $editButton =& Piwi::createWidget('Button', 'edit', '', STOCK_EDIT);
                $editButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditProduct&id=" . $id . "';
                    "
                );
                
                $delButton =& Piwi::createWidget('Button', 'delete', '', STOCK_DELETE);
                $delButton->AddEvent(
                    ON_CLICK,
                    "if (confirm('Are you sure you want to delete this product?'))
                    {
                        delete_product(" . $id . ");
                    }"
                );
                
                $tpl->SetVariable('id', $id);
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('name', $name);
                $tpl->SetVariable('stock', $qty);
                $tpl->SetVariable('cat', $cat);
                $tpl->SetVariable('visible', $vis);
                $tpl->SetVariable('image_button', $imageButton->Get());
                $tpl->SetVariable('edit_button', $editButton->Get());
                $tpl->SetVariable('delete_button', $delButton->Get());
                $tpl->ParseBlock('products/product-row');
            }
        }
        else
        {
            $tpl->SetVariable('no-product', 'block');
        }
        
        $tpl->ParseBlock('products');
        
        return $tpl->Get();
    }
    /**
     * Prepares the add product management view
     *
     * @access public
     * @return string   parsed template
     */
    public function EditProduct()
    {
        $product = null;
        
        $this->CheckPermission('EditProduct');
        $this->AjaxMe('script.js');
        
        $base_model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $c_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Categories');
        $s_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Suppliers');
        
        // if editing product
        if (isset($_GET['id'])) {
            $product = $model->GetProductById($_GET['id']);
        }
        
        // get product if it exists in dictionary
        if ($dictionary->exists('edit_product'))
        {
            $product = array();
            $str = $dictionary->get('edit_product');
            $str = urldecode($str['value']);
            $product['product_id'] = 0;
            $product['product_name'] = $base_model->getUrlValue($str, 'name');
            $product['category_id'] = $base_model->getUrlValue($str, 'category');
            $product['supplier_id'] = $base_model->getUrlValue($str, 'supplier');
            $product['url'] = $base_model->getUrlValue($str, 'url');
            $product['product_visible'] = $base_model->getUrlValue($str, 'visible') == 'true' ? TRUE : FALSE;
            $product['price_wholesale'] = $base_model->getUrlValue($str, 'wPrice');
            $product['price_retail'] = $base_model->getUrlValue($str, 'rPrice');
            $product['product_type_id'] = $base_model->getUrlValue($str, 'type');
            $product['short_description'] = $base_model->getUrlValue($str, 'sDescription');
            $product['long_description'] = $base_model->getUrlValue($str, 'lDescription');
        }
        
        if (isset($_GET['category']))
        {
            $product['category_id'] = $_GET['category'];
        }
        
        if (isset($_GET['supplier']))
        {
            $product['supplier_id'] = $_GET['supplier'];
        }
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditProduct.html');
        $tpl->SetBlock('add_product');
        $tpl->SetVariable('menubar', $this->MenuBar('Products'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'EditProduct');
        $id = $product == null ? 0 : $product['product_id'];
        $tpl->SetVariable('id', $id);
        
        //==================================================================================
        // Product Name
        //==================================================================================
        
        $p_name =& Piwi::CreateWidget('Entry', 'name', $product['product_name']);
        $p_name->setStyle('width: 350px;');
        $p_name->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $tpl->SetVariable('lbl_name', _t('REGISTER_LBL_NAME'));
        $tpl->SetVariable('name', $p_name->Get());
        
        //==================================================================================
        // Product Category
        //==================================================================================
        
        $tpl->SetVariable('lbl_catid', _t('REGISTER_LBL_CATEGORY'));
        $categoryCombo =& Piwi::CreateWidget('Combo', 'catid', '');
        $categoryCombo->SetID('catid');
        $categoryCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $categoryCombo->setStyle('width:130px;');
        $categoryCombo->AddOption('', '');
        
        $categories = $c_model->GetAllCategories();
        
        // Adding Product Categories to combo
        foreach ($categories as $cat) {
            $categoryCombo->AddOption($cat['category_name'], $cat['category_id']);
        }
        
        $categoryCombo->SetDefault($product['category_id']);
        
        $tpl->SetVariable('catid', $categoryCombo->Get());
        
        //==================================================================================
        // Add Category Button
        //==================================================================================
        
        $btnAddCategory =& Piwi::createWidget('Button', 'add_category', _t('REGISTER_LBL_CATEGORY'), STOCK_ADD);
        $btnAddCategory->SetStyle('width:100px;');
        $btnAddCategory->AddEvent(
            ON_CLICK,
            "javascript:
                save_product_to_dictionary();
                window.location = 'admin.php?gadget=Register&action=EditCategory';
            "
        );
        
        $tpl->SetVariable('add_category_button', $btnAddCategory->Get());
        
        //==================================================================================
        // Product Supplier
        //==================================================================================
        
        $tpl->SetVariable('lbl_suppliers', _t('REGISTER_SUPPLIER'));
        $supplierCombo =& Piwi::createWidget('Combo', 'suppid', '');
        $supplierCombo->SetID('suppid');
        $supplierCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $supplierCombo->setStyle('width:130px;');
        $supplierCombo->AddOption('', '');
        
        $suppliers = $s_model->GetAllSuppliers();
        
        // Add Suppliers to Combo
        foreach ($suppliers as $supplier)
        {
            $supplierCombo->AddOption($supplier['company_name'], $supplier['supplier_id']);
        }
        
        $supplierCombo->SetDefault($product['supplier_id']);
        
        
        $tpl->SetVariable('suppid', $supplierCombo->Get());
        
        //==================================================================================
        // Add Supplier Button
        //==================================================================================
        
        $btnAddSupplier =& Piwi::createWidget('Button', 'add_supplier', _t('REGISTER_SUPPLIER'), STOCK_ADD);
        $btnAddSupplier->SetStyle('width:100px;');
        $btnAddSupplier->AddEvent(
            ON_CLICK,
            "javascript:
                save_product_to_dictionary();
                window.location = 'admin.php?gadget=Register&action=EditSupplier';
            "
        );
        
        $tpl->SetVariable('add_supplier_button', $btnAddSupplier->Get());
        
        //==================================================================================
        // Product Url
        //==================================================================================
        
        $p_url =& Piwi::createWidget('Entry', 'url', $product['url']);
        $p_url->setStyle('width: 350px;');
        $p_url->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $tpl->SetVariable('lbl_url', _t('REGISTER_LBL_URL'));
        $tpl->SetVariable('url', $p_url->Get());
        
        //==================================================================================
        // Product Visibility
        //==================================================================================
        
        $v = $product != null ? $product['product_visible'] : true;
        $chk_visible =& Piwi::createWidget('CheckButtons', 'visibility');
        $chk_visible->AddOption(_t('REGISTER_LBL_VISIBLE'), 'chk_visible', 'visibility', $v);
        $chk_visible->AddEvent(
            ON_CLICK,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $tpl->SetVariable('visibility', $chk_visible->Get());
        
        //==================================================================================
        // Product Wholesale Price
        //==================================================================================
        
        $tpl->SetVariable('lbl_whole_price', _t('REGISTER_LBL_WHOLESALE_PRICE'));
        $p_w_price =& Piwi::createWidget('Entry', 'whole_price', $product['price_wholesale']);
        $p_w_price->setStyle('width: 100px;');
        $p_w_price->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        
        $tpl->SetVariable('whole_price', $p_w_price->Get());
        
        //==================================================================================
        // Product Retail Price
        //==================================================================================
        
        $tpl->SetVariable('lbl_retail_price', _t('REGISTER_LBL_RETAIL_PRICE'));
        $p_r_price =& Piwi::createWidget('Entry', 'retail_price', $product['price_retail']);
        $p_r_price->setStyle('width: 100px;');
        $p_r_price->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        
        $tpl->SetVariable('retail_price', $p_r_price->Get());
        
        //==================================================================================
        // Product Type
        //==================================================================================
        
        $tpl->SetVariable('lbl_product_type', _t('REGISTER_PRODUCT_TYPE'));
        $typeCombo =& Piwi::createWidget('Combo', 'product_type', '');
        $typeCombo->SetID('type');
        $typeCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $typeCombo->setStyle('width:130px;');
        $typeCombo->AddOption('', '');
        
        $types = $model->GetAllProductTypes();
        
        // Add Suppliers to Combo
        foreach ($types as $type)
        {
            $typeCombo->AddOption($type['product_type_name'], $type['product_type_id']);
        }
        
        if ($product != NULL)
        {
            $typeCombo->SetDefault($product['product_type_id']);
        }
        else
        {
            $typeCombo->SetDefault(1);
        }
        
        $tpl->SetVariable('product_type', $typeCombo->Get());
        
        //==================================================================================
        // Product Short Description
        //==================================================================================
        
        $tpl->SetVariable('lbl_short_desc', _t('REGISTER_LBL_SHORT_DESC'));
        $short_desc =& $GLOBALS['app']->LoadEditor('Register', 'short_desc_block', $product['short_description'], false);
        $short_desc->setId('short_desc_block');
        $short_desc->TextArea->SetRows(4);
        $short_desc->TextArea->SetStyle('width: 100%;');
        $short_desc->SetWidth('96%');
        $short_desc->TextArea->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $tpl->SetVariable('short_desc_block', $short_desc->Get());
        
        //==================================================================================
        // Product Long Description
        //==================================================================================
        
        $tpl->SetVariable('lbl_long_desc', _t('REGISTER_LBL_LONG_DESC'));
        $long_desc =& $GLOBALS['app']->LoadEditor('Register', 'long_desc_block', $product['long_description'], false);
        $long_desc->setId('long_desc_block');
        $long_desc->TextArea->SetRows(8);
        $long_desc->TextArea->SetStyle('width: 100%;');
        $long_desc->SetWidth('96%');
        $long_desc->TextArea->AddEvent(
            ON_CHANGE,
            "javascript:
                save_product_to_dictionary();
            "
        );
        $tpl->SetVariable('long_desc_block', $long_desc->Get());
        
        //==================================================================================
        // Product Save Button
        //==================================================================================
        
        $btnSave =& Piwi::createWidget('Button', 'save', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $btnSave->AddEvent(
            ON_CLICK,
            "javascript:
            if (this.form.elements['name'].value == '') {
                alert('" . _t('REGISTER_PRODUCT_MISSING_NAME') . "');
                this.form.elements['name'].focus();
            } else if (this.form.elements['short_desc_block'].value == '') {
                alert('" . _t('REGISTER_PRODUCT_MISSING_SHORT_DESC') . "');
                this.form.elements['short_desc_block'].focus();
            } else {
                this.form.submit();
            }"
        );
        
        $tpl->SetVariable('save_button', $btnSave->Get());
        
        $tpl->ParseBlock('add_product');
        
        return $tpl->Get();
    }
    
    /**
     * Retrives all form data and passes
     * it to AdminModel for processing
     *
     * @access public
     */
    public function CompleteEditProduct()
    {
        $this->CheckPermission('AddProduct');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $request =& Jaws_Request::getInstance();
        $names = array('id', 'name', 'catid', 'suppid', 'url', 'visibility', 'whole_price', 'retail_price', 'product_type', 'short_desc_block', 'long_desc_block');
        
        $post = $request->get($names, 'post');
        
        if ($post['id'] > 0)
        {
            $model->UpdateProduct($post['id'], $post['name'], $post['catid'], $post['suppid'], isset($post['visibility'][0]), $post['short_desc_block'], $post['url'], $post['whole_price'], $post['retail_price'], $post['product_type'], $post['long_desc_block']);
        }
        else
        {
            $model->SaveNewProduct($post['name'], $post['catid'], $post['suppid'], isset($post['visibility'][0]), $post['short_desc_block'], $post['url'], $post['whole_price'], $post['retail_price'], $post['product_type'], $post['long_desc_block']);
        }
        
        $dictionary->deleteKey('edit_product');
        
        if ($dictionary->exists('edit_purchase_order'))
        {
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditPurchaseOrder');
        }
        
        if ($dictionary->exists('edit_customer_order'))
        {
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditCustomerOrder');
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Products');
    }
    
    /**
     * EditProductImage Action
     *
     * @access public
     * @return string   parsed template string
     */
    public function EditProductImage()
    {
        $id = NULL;
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $this->CheckPermission('EditProductImage');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditProductImage.html');
        $tpl->SetBlock('product_image');
        $tpl->SetVariable('menubar', $this->MenuBar('Products'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT . '?gadget=Register&action=UploadProductImage');
        $tpl->SetVariable('action', 'UploadProductImage');
        
        if (isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        
        $product = $model->GetProductById($id);
        
        $tpl->SetVariable('lbl_id', _t('REGISTER_LBL_ID'));
        $tpl->SetVariable('id', $product['product_id']);
        
        $tpl->SetVariable('lbl_name', _t('REGISTER_PRODUCT'));
        $tpl->SetVariable('name', $product['product_name']);
        
        $tpl->SetVariable('lbl_image', _t('REGISTER_IMAGE'));
        
        $imageField =& Piwi::createWidget('FileEntry', 'field_image');
        
        $tpl->SetVariable('field_image', $imageField->Get());
        
        $images = $model->GetAllProductImagesByID($id);
        
        foreach ($images as $image)
        {
            $tpl->SetBlock('product_image/image_row');
            $tpl->SetVariable('image', 'gadgets/Register/images/products/origin/' . $image['image_url']);
            $tpl->SetVariable('lbl_edit', _t('REGISTER_LBL_EDIT'));
            $tpl->SetVariable('url', BASE_SCRIPT . '?gadget=Register&action=TrimProductImage&id=' . $image['product_image_id']);
            $tpl->ParseBlock('product_image/image_row');
        }
        
        $uploadButton =& Piwi::createWidget('Button', 'uploadBtn', _t('REGISTER_UPLOAD'), STOCK_SAVE);
        $uploadButton->AddEvent(
            ON_CLICK,
            "javascript:
                this.form.submit();
            "
        );
        
        $tpl->SetVariable('upload_button', $uploadButton->Get());
        
        $tpl->ParseBlock('product_image');
        
        return $tpl->Get();
    }
    
    /**
     * Uploads Image Action
     *
     * Allows the 800x800 maximum image size
     *
     * @access public
     */
    public function UploadProductImage()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $a_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
        
        $target = JAWS_PATH . 'gadgets/Register/images/products/temp/';
        
        $request =& Jaws_Request::getInstance();
        $id = (int)$request->get('id', 'post');
        
        $counter = $model->GetProductImageCount($id);
        
        if ($counter == NULL || $counter === 0)
        {
            $counter    = 1;
        }
        
        // increment to next 
        $counter++;
        
        $image = $_FILES['field_image']['name'];
        $tmp = $_FILES['field_image']['tmp_name'];
        $width = $_FILES['field_image']['size'];
        
        $sizes = getimagesize($tmp);
        
        if ($sizes[0] > 800 || $sizes[1] > 800)
        {
            $GLOBALS['app']->Session->PushLastResponse(_t('REGISTER_ERROR_IMAGE_SIZE'), RESPONSE_ERROR);
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditProductImage&id=' . $id);
        }
        
        $ext = end(explode('.', $image));
        
        $filename = 'product.' . $id . '.' . $counter . '.' . $ext;
        $target = $target . $filename;
        
        $image_id = null;
        
        if (!file_exists($target))
        {
            if (move_uploaded_file($tmp, $target))
            {
                // Save to database
                $image_id = $a_model->AddNewProductImage($id, $filename);
            }
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=TrimProductImage&id=' . $image_id);
    }
    
    /**
     * Edits and Trims Image
     *
     * @access public
     * @return string
     */
    public function TrimProductImage()
    {
        $id = NULL;
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $this->CheckPermission('EditProductImage');
        $GLOBALS['app']->Layout->AddHeadLink('gadgets/Register/resources/cropper.css', 'stylesheet', 'text/css');
        $this->AjaxMe('script.js');
        $GLOBALS['app']->Layout->AddScriptLink('gadgets/Register/resources/cropper.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditProductImage.html');
        $tpl->SetBlock('trim_image');
        $tpl->SetVariable('menubar', $this->MenuBar('Products'));
        
        if (isset($_GET['id']))
        {
            $id = $_GET['id'];
        }
        
        // get product
        $image = $model->GetProductImageByID($id);
        
        $tpl->SetVariable('image', 'gadgets/Register/images/products/temp/' . $image['image_url']);
        
        $saveBtn =& Piwi::createWidget('Button', 'saveBtn', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $saveBtn->AddEvent(
            ON_CLICK,
            "javascript:
                complete_image_crop();
                window.location = 'admin.php?gadget=Register&action=EditProductImage&id=" . $image['product_id'] . "';
            "
        );
        
        $tpl->SetVariable('save_button', $saveBtn->Get());
        
        $tpl->ParseBlock('trim_image');
        
        return $tpl->Get();
    }
    
}