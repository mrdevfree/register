<?php
/**
 * Register Gadget Admin Customer Orders HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_CustomerOrders extends RegisterAdminHTML
{
    private $_orderKey = 'edit_customer_order';
    private $_productsKey = 'edit_customer_order_products';
        
    /**
     * Handles EditCustomerOrder Action
     *
     * @access public
     * @return string   parsed template
     */
    public function EditCustomerOrder()
    {
        $c_order = array();
        $order = null;
        
        $base_model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        $o_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $c_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Customers');
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $date = $GLOBALS['app']->LoadDate();
        
        $this->CheckPermission('EditCustomerOrder');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditCustomerOrder.html');
        $tpl->SetBlock('customer_order');
        $tpl->SetVariable('menubar', $this->MenuBar('Orders'));
        
        $tpl->SetVariable('script', BASE_SCRIPT . '?gadget=Register&action=CompleteEditCustomerOrder');
        $tpl->SetVariable('action', '');
        
        $tpl->SetVariable('title', _t('REGISTER_LBL_CUSTOMER_ORDER'));
        
        if (isset($_GET['id']))
        {
            $order = $o_model->GetCustomerOrderById($_GET['id']);
            $c_order['date'] = $order[0]['customer_order_date'];
            $c_order['customer_id'] = $order[0]['customer_id'];
            $c_order['status'] = $order[0]['customer_order_status_id'];
            $c_order['customer_order_comments'] = $order[0]['customer_order_comments'];
            $c_order['shipping'] = $order[0]['customer_order_shipping'];
            $c_order['discount'] = $order[0]['customer_order_discount'];
            $c_order['tax'] = $order[0]['customer_order_tax'];
            $c_order['removed'] = $order[0]['qty_removed'];
            $tpl->SetVariable('id', $order[0]['customer_order_id']);
        }
        
        //==================================================================================
        // Initialise Order from Dictionary if available
        //==================================================================================
        
        if ($dictionary->exists($this->_orderKey))
        {
            $c_order = array();
            $str = $dictionary->get($this->_orderKey);
            $str = urldecode($str['value']);
            $c_order['customer_order_id'] = 0;
            $c_order['customer_id'] = $base_model->getUrlValue($str, 'customer_id');
            $c_order['date'] = $model->getUrlValue($str, 'date');
            $c_order['status'] = $model->getUrlValue($str, 'status');
            $c_order['customer_order_comments'] = $base_model->getUrlValue($str, 'comments');
            $c_order['shipping'] = $base_model->getUrlValue($str, 'shipping');
            $c_order['discount'] = $base_model->getUrlValue($str, 'discount');
            $c_order['removed'] = $order[0]['removed'];
            $c_order['tax'] = $base_model->getUrlValue($str, 'tax');
        }
        
        if (!isset($c_order['removed']))
        {
            $c_order['removed'] = 0;
        }
        
        $tpl->SetVariable('removed', $c_order['removed']);
        
        //==================================================================================
        // SET Customer
        //==================================================================================
        $tpl->SetVariable('lbl_customer', _t('REGISTER_CUSTOMER'));
        
        $customers = $c_model->GetAllCustomers();
        
        $suppCombo =& Piwi::createWidget('Combo', 'customers');
        $suppCombo->setId('customers');
        $suppCombo->SetStyle('width:200px;');
        $suppCombo->AddOption('', 0);
        $suppCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
            "
        );
        
        if (($count = count($customers)) > 0)
        {
            foreach ($customers as $customer)
            {
                $suppCombo->AddOption($customer['name_first'] . ' ' . $customer['name_last'], $customer['customer_id']);
            }
        }
        
        if (isset($_GET['customer']))
        {
            $c_order['customer_id'] = $_GET['customer'];
        }
        
        // SET default CUSTOMER
        if (!isset($c_order['customer_id']))
        {
            $c_order['customer_id'] = '';
        }
        
        $suppCombo->setDefault($c_order['customer_id']);
        
        $tpl->SetVariable('customers', $suppCombo->Get());
        
        $btnAddCustomer =& Piwi::createWidget('Button', 'btnAddCustomer', _t('REGISTER_CUSTOMER'), STOCK_ADD);
        $btnAddCustomer->AddEvent(
            ON_CLICK,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                window.location = 'admin.php?gadget=Register&action=EditCustomer'"
        );
        
        $tpl->SetVariable('add_customer', $btnAddCustomer->Get());
        
        //==================================================================================
        // SET Date
        //==================================================================================
        
        $tpl->SetVariable('lbl_calander', _t('REGISTER_LBL_DATE'));
        
        if (!isset($c_order['date']))
        {
            $c_order['date'] = $date->Format(time(), 'Y-m-d');
        }
        
        $dateCal =& Piwi::createWidget('DatePicker', 'order_date', $c_order['date']);
        $dateCal->SetId('order_date');
        $dateCal->showTimePicker(true);
        $dateCal->setDateFormat('%Y-%m-%d');
        $dateCal->setLanguageCode($GLOBALS['app']->Registry->Get('/config/calendar_language'));
        $dateCal->setCalType($GLOBALS['app']->Registry->Get('/config/calendar_type'));
        $dateCal->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
            "
        );
        
        $tpl->SetVariable('calender', $dateCal->Get());
        
        //==================================================================================
        // SET Status
        //==================================================================================
        
        $tpl->SetVariable('lbl_status', _t('REGISTER_LBL_STATUS'));
        $statusCombo =& Piwi::createWidget('Combo', 'status');
        $statusCombo->SetID('status');
        $statusCombo->SetStyle('width:175px;');
        $statusCombo->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
            "
        );
        
        $statusCodes = $o_model->GetOrderStatusList();
        
        foreach ($statusCodes as $code)
        {
            $statusCombo->AddOption($code['status_name'], $code['order_status_id']);
        }
        
        // SET Status
        if (!isset($c_order['status']))
        {
            $c_order['status'] = 1;
        }
        
        $statusCombo->SetDefault($c_order['status']);
        
        $tpl->SetVariable('status', $statusCombo->Get());
        
        //==================================================================================
        // ORDER PRODUCTS
        //==================================================================================
        
        // Add Row Button
        $addRow =& Piwi::createWidget('Button', 'addRow', _t('REGISTER_ADD_ROW'), STOCK_ADD);
        $addRow->AddEvent(
            ON_CLICK,
            "javascript:
                add_customer_order_row();
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                calculate_c_order_total();
            "
        );
        
        $tpl->SetVariable('add_row', $addRow->Get());
        
        // Delete Rows Button
        $deleteRow =& Piwi::createWidget('Button', 'deleteRow', _t('REGISTER_DELETE_ROW'), STOCK_DELETE);
        $deleteRow->AddEvent(
            ON_CLICK,
            "javascript:
                delete_customer_order_row();
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                calculate_c_order_total();
            "
        );
        
        $tpl->SetVariable('delete_row', $deleteRow->Get());
        
        $tpl->SetVariable('lbl_select', _t('REGISTER_SELECT'));
        $tpl->SetVariable('lbl_product_name', _t('REGISTER_LBL_PRODUCT_NAME'));
        $tpl->SetVariable('lbl_qty', _t('REGISTER_LBL_QTY'));
        $tpl->SetVariable('lbl_price', _t('REGISTER_ITEM_PRICE'));
        $tpl->SetVariable('lbl_discount', _t('REGISTER_DISCOUNT'));
        
        //==================================================================================
        // Add New Product
        //==================================================================================
        
        $newProduct =& Piwi::createWidget('Button', 'newProduct', _t('REGISTER_PRODUCT'), STOCK_ADD);
        $newProduct->AddEvent(
            ON_CLICK,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                javascript: window.location = 'admin.php?gadget=Register&action=EditProduct';
            "
        );
        
        $tpl->SetVariable('new_product', $newProduct->Get());
        
        
        $products = $p_model->GetAllProducts();
        
        $selectedProducts = array();
        $inputQty = array();
        $inputDiscounts = array();
        
        // if ID is set -> get order data from $order
        if (isset($_GET['id']))
        {
            $prodCount = count($order);
            
            foreach ($order as $item)
            {
                $selectedProducts[] = $item['product_id'];
                $inputQty[] = $item['quantity'];
                $inputDiscounts[] = $item['discount'];
            }
        }
        
        if ($dictionary->exists($this->_productsKey))
        {
            $str = $dictionary->get($this->_productsKey);
            $str = urldecode($str['value']);
            $selectedProducts = $base_model->getSelectedArray($str, 'product');
            $inputQty = $base_model->getSelectedArray($str, 'qty');
            $inputDiscounts = $base_model->getSelectedArray($str, 'discount');
        }
        
        $countValues = count($selectedProducts);
        
        if (count($selectedProducts) == 0) {
            $countValues = 1;
        }
        
        // order total holder
        $orderTotal = 0;
        
        //==================================================================================
        // ADD All Products to Form
        //==================================================================================
        
        for ($i = 0; $i < $countValues; $i++)
        {
            $tpl->SetBlock('customer_order/order_item');
            
            //===============================================================================
            // ADD Select Checkboxes
            //===============================================================================
            
            $sel_chk =& Piwi::createWidget('CheckButtons', 'select_buttons[]', '');
            $sel_chk->SetId('select_button');
            $sel_chk->AddOption('', '1', 'selected');
            
            $tpl->SetVariable('chk', $sel_chk->Get());
            
            //===============================================================================
            // ADD Products Combo
            //===============================================================================
            
            $productCombo =& Piwi::createWidget('Combo', 'products[]', '');
            $productCombo->SetStyle('width:200px;');
            $productCombo->AddOption('', 0);
            $productCombo->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_customer_order_to_dictionary();
                    save_customer_order_products_to_dictionary();
                    calculate_c_order_total();
                "
            );
            
            if (($count = count($products)) > 0)
            {
                // Add each product to Combo List
                foreach ($products as $product)
                {
                    $productCombo->AddOption($product['product_name'], $product['product_id']);
                }
            }
            
            if (count($selectedProducts) > 0)
            {
                $productCombo->SetDefault($selectedProducts[$i]);
            }
            else{
                $productCombo->SetDefault('');
            }
            
            $tpl->SetVariable('products', $productCombo->Get());
            
            //===============================================================================
            // ADD Qty Input Field
            //===============================================================================
            
            if (!isset($inputQty[$i]))
            {
                $inputQty[$i] = 1;
            }
            
            $qty_field =& Piwi::createWidget('Entry', 'qty[]', $inputQty[$i]);
            $qty_field->SetStyle('width:50px; text-align:center;');
            $qty_field->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_customer_order_to_dictionary();
                    save_customer_order_products_to_dictionary();
                    calculate_c_order_total();
                "
            );
            
            $tpl->SetVariable('qty', $qty_field->Get());
            
            //===============================================================================
            // ADD Discount Input Field
            //===============================================================================
            
            $product = null;
            // get product
            if (count($selectedProducts) > $i)
            {
                $product = $p_model->GetProductById($selectedProducts[$i]);
            }
            
            if (!isset($inputDiscounts[$i]))
            {
                $inputDiscounts[$i] = 0;
            }
            
            $prodPrice = $model->buildPrice($inputQty[$i] * $product['price_retail'] - $inputDiscounts[$i]);
            $orderTotal += $prodPrice;
            
            $discountField =& Piwi::createWidget('Entry', 'discounts[]', $inputDiscounts[$i]);
            $discountField->SetStyle('width:50px;text-align:center');
            $discountField->AddEvent(
                ON_CHANGE,
                "javascript:
                    save_customer_order_to_dictionary();
                    save_customer_order_products_to_dictionary();
                    calculate_c_order_total();
                "
            );
            
            $tpl->SetVariable('discount', $discountField->Get());
            
            //===============================================================================
            // Product Item Price
            //===============================================================================
            
            $tpl->SetVariable('price', '$ ' . $model->buildPrice($product['price_retail']));
            
            
            $tpl->ParseBlock('customer_order/order_item');
            
        }
        
        //==================================================================================
        // Form Total Labels
        //==================================================================================
        
        $tpl->SetVariable('lbl_shipping', _t('REGISTER_SHIPPING'));
        $tpl->SetVariable('lbl_comments', _t('REGISTER_COMMENTS'));
        $tpl->SetVariable('lbl_discount', _t('REGISTER_DISCOUNT'));
        $tpl->SetVariable('lbl_tax', _t('REGISTER_TAX'));
        $tpl->SetVariable('lbl_total', _t('REGISTER_LBL_ORDER_TOTAL'));
        
        //==================================================================================
        // Order Shipping
        //==================================================================================
        
        if (!isset($c_order['shipping']))
        {
            $c_order['shipping'] = 0.00;
        }
        
        $shippingField =& Piwi::createWidget('Entry', 'shipping', $c_order['shipping']);
        $shippingField->SetStyle('width:50px;text-align:center;');
        $shippingField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                calculate_c_order_total();
            "
        );
        
        $tpl->SetVariable('shipping', $shippingField->Get());
        
        //==================================================================================
        // Order Discount
        //==================================================================================
        
        if (!isset($c_order['discount']))
        {
            $c_order['discount'] = 0.00;
        }
        
        $discountField =& Piwi::createWidget('Entry', 'discount', $c_order['discount']);
        $discountField->SetStyle('width:50px;text-align:center;');
        $discountField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                calculate_c_order_total();
            "
        );
        
        $tpl->SetVariable('discount', $discountField->Get());
        
        //==================================================================================
        // Order Tax
        //==================================================================================
        
        if (!isset($c_order['tax']))
        {
            $c_order['tax'] = 0.00;
        }
        
        $taxField =& Piwi::createWidget('Entry', 'tax', $c_order['tax']);
        $taxField->SetStyle('width:50px;text-align:center;');
        $taxField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
                calculate_c_order_total();
            "
        );
        
        $tpl->SetVariable('tax', $taxField->Get());
        
        //==================================================================================
        // Order Total
        //==================================================================================
        
        $orderTotal = ($orderTotal + $c_order['shipping'] + $c_order['tax']) - $c_order['discount'];
        
        $tpl->SetVariable('total', '$ ' . $base_model->buildPrice($orderTotal));
        
        //==================================================================================
        // Order Comments
        //==================================================================================
        
        if (!isset($c_order['customer_order_comments']))
        {
            $c_order['customer_order_comments'] = '';
        }
        
        $commentsField =& Piwi::createWidget('TextArea', 'comments', urldecode($c_order['customer_order_comments']));
        $commentsField->SetRows(4);
        $commentsField->SetColumns(20);
        $commentsField->SetStyle('width:330px;');
        $commentsField->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_order_to_dictionary();
                save_customer_order_products_to_dictionary();
            "
        );
        
        $tpl->SetVariable('comments_field', $commentsField->Get());
        
        //==================================================================================
        // Save and Cancel Buttons
        //==================================================================================
        
        $cancelBtn =& Piwi::createWidget('Button', 'cancelBtn', _t('GLOBAL_CANCEL'), STOCK_CANCEL);
        $cancelBtn->AddEvent(
            ON_CLICK,
            "javascript:
                //clear_customer_order();
                window.location = 'admin.php?gadget=Register&action=Orders'
            "
        );
        
        $tpl->SetVariable('cancel_button', $cancelBtn->Get());
        
        $saveBtn =& Piwi::createWidget('Button', 'saveBtn', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $saveBtn->AddEvent(
            ON_CLICK,
            "javascript:
                if (validate_customer_order_form() === true)
                {
                    //clear_customer_order();
                    this.form.submit();
                }
            "
        );
        
        $tpl->SetVariable('save_button', $saveBtn->Get());
        
        //$saveAndPrintBtn->AddEvent(
        //    ON_CLICK,
        //    "javascript:
        //        if (validate_customer_order_form() === true)
        //        {
        //            clear_customer_order();
        //            this.form.submit();
        //        }
        //    "
        //);
        
        //$saveAndEmailBtn->AddEvent(
        //    ON_CLICK,
        //    "javascript:
        //        if (validate_customer_order_form() === true)
        //        {
        //            clear_customer_order();
        //            this.form.submit();
        //        }
        //    "
        //);
        
        $tpl->SetVariable('save_button', $saveBtn->Get());
        //$tpl->SetVariable('save_and_print_btn', $saveAndPrintBtn->Get());
        //$tpl->SetVariable('save_and_email_btn', $saveAndEmailBtn->Get());
        
        $tpl->ParseBlock('customer_order');
        
        return $tpl->Get();
    }
    
    /**
     * Process Edit Customer Order Form
     *
     * @access public
     */
    public function CompleteEditCustomerOrder()
    {
        $this->CheckPermission('EditCustomerOrder');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $request =& Jaws_Request::getInstance();
        $names = array('id', 'customers', 'order_date', 'status', 'products', 'qty',
            'prices', 'discounts', 'comments', 'discount', 'tax', 'shipping');
        
        $post = $request->get($names, 'post');
        
        foreach ($post['products'] as $product)
        {
            $p = $p_model->GetProductById($product);
            $post['prices'][] = $p['price_retail'];
        }
        
        if ($post['id'] > 0)
        {
            $model->UpdateCustomerOrder(
                $post['id'], $post['customers'], $post['order_date'], $post['status'], $post['products'],
                $post['qty'], $post['prices'], $post['discounts'], $post['comments'], $post['discount'],
                $post['tax'], $post['shipping']);
        }
        else
        {
            $model->SaveNewCustomerOrder(
                $post['customers'], $post['order_date'], $post['status'], $post['products'],
                $post['qty'], $post['prices'], $post['discounts'], $post['comments'], $post['discount'],
                $post['tax'], $post['shipping']);
        }
        
        if ($dictionary->exists($this->_orderKey))
        {
            $dictionary->deleteKey($this->_orderKey);
        }
        if ($dictionary->exists($this->_productsKey))
        {
            $dictionary->deleteKey($this->_productsKey);
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Orders');
    }
}