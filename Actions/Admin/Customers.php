<?php
/**
 * Register Gadget Admin Customers HTML file
 *
 * @category    GadgetAdmin
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class Register_Actions_Admin_Customers extends RegisterAdminHTML
{
    /**
     * Customers Action
     *
     * @access public
     * @return string   parsed template
     */
    public function Customers()
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Customers');
        $o_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $this->CheckPermission('Customers');
        $this->AjaxMe('script.js');
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('Customers.html');
        $tpl->SetBlock('customers');
        $tpl->SetVariable('menubar', $this->MenuBar('Customers'));
        
        $btnAddCustomer =& Piwi::createWidget('Button', 'btn_add', _t('REGISTER_CUSTOMER'), STOCK_ADD);
        $btnAddCustomer->SetStyle('float:right;');
        $btnAddCustomer->AddEvent(
            ON_CLICK,
            "javascript:
                window.location = 'admin.php?gadget=Register&action=EditCustomer';
            "
        );
        $tpl->SetVariable('add_customer', $btnAddCustomer->Get());
        
        // Process Customers
        $tpl->SetBlock('customers');
        $tpl->SetVariable('name', _t('REGISTER_LBL_NAME'));
        $tpl->SetVariable('total', _t('REGISTER_LBL_ORDER_TOTAL'));
        $tpl->SetVariable('visible', _t('REGISTER_LBL_VISIBLE'));
        $tpl->SetVariable('actions', _t('REGISTER_LBL_ACTIONS'));
        
        $customers = $model->GetAllCustomers();
        
        if (($count = count($customers, COUNT_NORMAL)) > 0)
        {
            $bg = '';
            $tpl->SetVariable('no-customer', 'none');
            foreach ($customers as $customer)
            {
                $tpl->SetBlock('customers/customer-row');
                $bg = Jaws_Utils::RowColor($bg);
                $id = $customer['customer_id'];
                $name = $customer['name_first'] . ' ' . $customer['name_last'];
                $total = $o_model->GetCustomerOrdersTotal($id);
                $vis = $customer['customer_visible'] == 0 ? 'No' : 'Yes';
                
                $editButton =& Piwi::createWidget('Button', 'edit', '', STOCK_EDIT);
                $editButton->AddEvent(
                    ON_CLICK,
                    "javascript:
                        window.location = 'admin.php?gadget=Register&action=EditCustomer&id=" . $id . "';
                    "
                );
                
                $delButton =& Piwi::createWidget('Button', 'delete', '', STOCK_DELETE);
                $delButton->AddEvent(
                    ON_CLICK,
                    "if (confirm('Are you sure you want to delete this customer?'))
                    {
                        delete_customer(" . $id . ");
                    }"
                );
                
                $tpl->SetVariable('id', $id);
                $tpl->SetVariable('bgcolor', $bg);
                $tpl->SetVariable('name', $name);
                $tpl->SetVariable('total', $total);
                $tpl->SetVariable('visible', $vis);
                $tpl->SetVariable('edit_button', $editButton->Get());
                $tpl->SetVariable('delete_button', $delButton->Get());
                $tpl->ParseBlock('customers/customer-row');
            }
        }
        else
        {
            $tpl->SetVariable('no-customer', 'block');
        }
        
        $tpl->ParseBlock('customers');
        
        return $tpl->Get();
    }
    
    /**
     * Edit Customer Action
     *
     * @access public
     * @return string   parsed template
     */
    public function EditCustomer()
    {
        $customer = null;
        
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Customers');
        $base_model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $this->CheckPermission('EditCustomer');
        $this->AjaxMe('script.js');
        
        if (isset($_GET['id'])) {
            $customer = $model->GetCustomerById($_GET['id']);
        }
        
        // get customer if it exists in dictionary
        if ($dictionary->exists('edit_customer'))
        {
            $customer = array();
            $str = $dictionary->get('edit_customer');
            $str = urldecode($str['value']);
            $customer['customer_id'] = 0;
            $customer['company_name'] = $base_model->getUrlValue($str, 'company');
            $customer['abn'] = $base_model->getUrlValue($str, 'abn');
            $customer['customer_visible'] = $base_model->getUrlValue($str, 'visible') == 'true' ? TRUE : FALSE;
            $customer['email'] = $base_model->getUrlValue($str, 'email');
            $customer['website'] = $base_model->getUrlValue($str, 'website');
            $customer['name_first'] = $base_model->getUrlValue($str, 'f_name');
            $customer['name_last'] = $base_model->getUrlValue($str, 'l_name');
            $customer['phone'] = $base_model->getUrlValue($str, 'phone');
            $customer['fax'] = $base_model->getUrlValue($str, 'fax');
            $customer['mobile'] = $base_model->getUrlValue($str, 'mobile');
            $customer['street1'] = $base_model->getUrlValue($str, 'street1');
            $customer['street2'] = $base_model->getUrlValue($str, 'street2');
            $customer['city'] = $base_model->getUrlValue($str, 'city');
            $customer['state'] = $base_model->getUrlValue($str, 'state');
            $customer['pcode'] = $base_model->getUrlValue($str, 'pcode');
            $customer['country'] = $base_model->getUrlValue($str, 'country');
            $customer['ship_same'] = $base_model->getUrlValue($str, 'ship_same');
            $customer['ship_street1'] = $base_model->getUrlValue($str, 'ship_street1');
            $customer['ship_street2'] = $base_model->getUrlValue($str, 'ship_street2');
            $customer['ship_city'] = $base_model->getUrlValue($str, 'ship_city');
            $customer['ship_state'] = $base_model->getUrlValue($str, 'ship_state');
            $customer['ship_pcode'] = $base_model->getUrlValue($str, 'ship_pcode');
            $customer['ship_country'] = $base_model->getUrlValue($str, 'ship_country');
        }
        
        $tpl = new Jaws_Template('gadgets/Register/templates/');
        $tpl->Load('EditCustomer.html');
        $tpl->SetBlock('edit_customer');
        $tpl->SetVariable('menubar', $this->MenuBar('Customers'));
        
        $tpl->SetVariable('base_script', BASE_SCRIPT);
        $tpl->SetVariable('action', 'EditCustomer');
        $id = $customer == null ? 0 : $customer['customer_id'];
        $tpl->SetVariable('id', $id);
        
        //==================================================================================
        // Personal Info
        //==================================================================================
        
        $tpl->SetVariable('lbl_personal', _t('REGISTER_LBL_PERSONAL'));
        
        //==================================================================================
        // Company Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_comp_name', _t('REGISTER_LBL_COMPANY_NAME'));
        $company_name =& Piwi::createWidget('Entry', 'txt_company', $customer['company_name']);
        $company_name->SetStyle('width:300px;');
        $company_name->SetTitle(_t('REGISTER_LBL_COMPANY_NAME'));
        $company_name->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_company', $company_name->Get());
        
        //==================================================================================
        // ABN
        //==================================================================================
        
        $tpl->SetVariable('lbl_abn', _t('REGISTER_LBL_ABN'));
        $abn =& Piwi::createWidget('Entry', 'txt_abn', $customer['abn']);
        $abn->SetStyle('width:150px;');
        $abn->SetTitle(_t('REGISTER_LBL_ABN'));
        $abn->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_abn', $abn->Get());
        
        //==================================================================================
        // Visibility
        //==================================================================================
        
        $vis = $customer != null ? $customer['customer_visible'] : true;
        $chk_visible =& Piwi::createWidget('CheckButtons', 'visibility');
        $chk_visible->AddEvent(
            ON_CLICK,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $chk_visible->AddOption(_t('REGISTER_LBL_VISIBLE'), 'chk_visible', 'visibility', $vis);
        
        $tpl->SetVariable('visible', $chk_visible->Get());
        
        //==================================================================================
        // Email
        //==================================================================================
        
        $tpl->SetVariable('lbl_email', _t('REGISTER_LBL_EMAIL'));
        $email =& Piwi::createWidget('Entry', 'txt_email', $customer['email']);
        $email->SetStyle('width:300px');
        $email->SetTitle(_t('REGISTER_LBL_EMAIL'));
        $email->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_email', $email->Get());
        
        //==================================================================================
        // Website
        //==================================================================================
        
        $tpl->SetVariable('lbl_website', _t('REGISTER_LBL_WEBSITE'));
        $website =& Piwi::createWidget('Entry', 'txt_website', $customer['website']);
        $website->SetStyle('width:300px');
        $website->SetTitle(_t('REGISTER_LBL_WEBSITE'));
        $website->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_website', $website->Get());
        
        //==================================================================================
        // First Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_first_name', _t('REGISTER_LBL_FIRST_NAME'));
        $rep_first =& Piwi::createWidget('Entry', 'txt_first_name', $customer['name_first']);
        $rep_first->SetStyle('width:150px');
        $rep_first->SetTitle(_t('REGISTER_LBL_FIRST_NAME'));
        $rep_first->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_first_name', $rep_first->Get());
        
        //==================================================================================
        // Last Name
        //==================================================================================
        
        $tpl->SetVariable('lbl_last_name', _t('REGISTER_LBL_LAST_NAME'));
        $rep_last =& Piwi::createWidget('Entry', 'txt_last_name', $customer['name_last']);
        $rep_last->SetStyle('width:150px');
        $rep_last->SetTitle(_t('REGISTER_LBL_LAST_NAME'));
        $rep_last->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_last_name', $rep_last->Get());
        
        //==================================================================================
        // Phone
        //==================================================================================
        
        $tpl->SetVariable('lbl_phone', _t('REGISTER_LBL_PHONE'));
        $phone =& Piwi::createWidget('Entry', 'txt_phone', $customer['phone']);
        $phone->SetStyle('width:150px');
        $phone->SetTitle(_t('REGISTER_LBL_PHONE'));
        $phone->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_phone', $phone->Get());
        
        //==================================================================================
        // Fax
        //==================================================================================
        
        $tpl->SetVariable('lbl_fax', _t('REGISTER_LBL_FAX'));
        $fax =& Piwi::createWidget('Entry', 'txt_fax', $customer['fax']);
        $fax->SetStyle('width:150px');
        $fax->SetTitle(_t('REGISTER_LBL_FAX'));
        $fax->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_fax', $fax->Get());
        
        //==================================================================================
        // Mobile
        //==================================================================================
        
        $tpl->SetVariable('lbl_mobile', _t('REGISTER_LBL_MOBILE'));
        $mobile =& Piwi::createWidget('Entry', 'txt_mobile', $customer['mobile']);
        $mobile->SetStyle('width:150px');
        $mobile->SetTitle(_t('REGISTER_LBL_MOBILE'));
        $mobile->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_mobile', $mobile->Get());
        
        //==================================================================================
        // Address
        //==================================================================================
        
        $tpl->SetVariable('lbl_address', _t('REGISTER_BILLING_ADDRESS'));
        
        //==================================================================================
        // Street 1
        //==================================================================================
        
        $tpl->SetVariable('lbl_street1', _t('REGISTER_LBL_STREET'));
        $street1 =& Piwi::createWidget('Entry', 'txt_street1', $customer['street1']);
        $street1->SetStyle('width:300px');
        $street1->SetTitle(_t('REGISTER_LBL_STREET'));
        $street1->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_street1', $street1->Get());
        
        //==================================================================================
        // Street 2
        //==================================================================================
        
        $street2 =& Piwi::createWidget('Entry', 'txt_street2', $customer['street2']);
        $street2->SetStyle('width:300px');
        $street2->SetTitle('');
        $street2->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_street2', $street2->Get());
        
        //==================================================================================
        // City
        //==================================================================================
        
        $tpl->SetVariable('lbl_city', _t('REGISTER_LBL_CITY'));
        $city =& Piwi::createWidget('Entry', 'txt_city', $customer['city']);
        $city->SetStyle('width:200px');
        $city->SetTitle(_t('REGISTER_LBL_CITY'));
        $city->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_city', $city->Get());
        
        //==================================================================================
        // State
        //==================================================================================
        
        $tpl->SetVariable('lbl_state', _t('REGISTER_LBL_STATE'));
        $state =& Piwi::createWidget('Entry', 'txt_state', $customer['state']);
        $state->SetStyle('width:150px');
        $state->SetTitle(_t('REGISTER_LBL_STATE'));
        $state->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_state', $state->Get());
        
        //==================================================================================
        // Post Code / Zip
        //==================================================================================
        
        $tpl->SetVariable('lbl_pcode', _t('REGISTER_LBL_PCODE'));
        $pcode =& Piwi::createWidget('Entry', 'txt_pcode', $customer['pcode']);
        $pcode->SetStyle('width:150px');
        $pcode->SetTitle(_t('REGISTER_LBL_PCODE'));
        $pcode->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_pcode', $pcode->Get());
        
        //==================================================================================
        // Country
        //==================================================================================
        
        $tpl->SetVariable('lbl_country', _t('REGISTER_LBL_COUNTRY'));
        $country =& Piwi::createWidget('Entry', 'txt_country', $customer['country']);
        $country->SetStyle('width:300px');
        $country->SetTitle(_t('REGISTER_LBL_COUNTRY'));
        $country->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_country', $country->Get());
        
        $tpl->SetBlock('edit_customer/shipping');
        
        //==================================================================================
        // Different Shipping Address
        //==================================================================================
        
        if (!isset($customer['ship_same']))
        {
            $customer['ship_same'] = true;
        }
        
        $vis = isset($customer['ship_same']) ? $customer['ship_same'] : true;
        
        $check_ship =& Piwi::createWidget('CheckButtons', 'same_shipping');
        $check_ship->AddEvent(
            ON_CLICK,
            "javascript:
                save_customer_to_dictionary();
                if ($(this).checked)
                {
                    $('shipping').hide();
                }
                else
                {
                    $('shipping').show();
                }
            "
        );
        
        $check_ship->AddOption(_t('REGISTER_SAME_SHIPPING_ADDRESS'), 'check_ship', 'same_shipping', $vis == 'true' ? 1 : 0);
        
        $tpl->SetVariable('check_shipping', $check_ship->Get());
        
        //==================================================================================
        // SHIPPING ADDRESS
        //==================================================================================
        
        if ($vis == 'true')
        {
            $tpl->SetVariable('show_shipping', 'none');
        }
        else
        {
            $tpl->SetVariable('show_shipping', '');
        }
        
        $tpl->SetVariable('lbl_address', _t('REGISTER_SHIPPING_ADDRESS'));
        
        //==================================================================================
        // Street 1
        //==================================================================================
        
        if (!isset($customer['ship_street1']))
        {
            $customer['ship_street1'] = '';
        }
        
        $tpl->SetVariable('lbl_street1', _t('REGISTER_LBL_STREET'));
        $street1 =& Piwi::createWidget('Entry', 'txt_ship_street1', $customer['ship_street1']);
        $street1->SetStyle('width:300px');
        $street1->SetTitle(_t('REGISTER_LBL_STREET'));
        $street1->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_street1', $street1->Get());
        
        //==================================================================================
        // Street 2
        //==================================================================================
        
        if (!isset($customer['ship_street2']))
        {
            $customer['ship_street2'] = '';
        }
        
        $street2 =& Piwi::createWidget('Entry', 'txt_ship_street2', $customer['ship_street2']);
        $street2->SetStyle('width:300px');
        $street2->SetTitle('');
        $street2->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_street2', $street2->Get());
        
        //==================================================================================
        // City
        //==================================================================================
        
        if (!isset($customer['ship_city']))
        {
            $customer['ship_city'] = '';
        }
        
        $tpl->SetVariable('lbl_city', _t('REGISTER_LBL_CITY'));
        $city =& Piwi::createWidget('Entry', 'txt_ship_city', $customer['ship_city']);
        $city->SetStyle('width:200px');
        $city->SetTitle(_t('REGISTER_LBL_CITY'));
        $city->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_city', $city->Get());
        
        //==================================================================================
        // State
        //==================================================================================
        
        if (!isset($customer['ship_state']))
        {
            $customer['ship_state'] = '';
        }
        
        $tpl->SetVariable('lbl_state', _t('REGISTER_LBL_STATE'));
        $state =& Piwi::createWidget('Entry', 'txt_ship_state', $customer['ship_state']);
        $state->SetStyle('width:150px');
        $state->SetTitle(_t('REGISTER_LBL_STATE'));
        $state->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_state', $state->Get());
        
        //==================================================================================
        // Post Code / Zip
        //==================================================================================
        
        if (!isset($customer['ship_pcode']))
        {
            $customer['ship_pcode'] = '';
        }
        
        $tpl->SetVariable('lbl_pcode', _t('REGISTER_LBL_PCODE'));
        $pcode =& Piwi::createWidget('Entry', 'txt_ship_pcode', $customer['ship_pcode']);
        $pcode->SetStyle('width:150px');
        $pcode->SetTitle(_t('REGISTER_LBL_PCODE'));
        $pcode->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_pcode', $pcode->Get());
        
        //==================================================================================
        // Country
        //==================================================================================
        
        if (!isset($customer['ship_country']))
        {
            $customer['ship_country'] = '';
        }
        
        $tpl->SetVariable('lbl_country', _t('REGISTER_LBL_COUNTRY'));
        $country =& Piwi::createWidget('Entry', 'txt_ship_country', $customer['ship_country']);
        $country->SetStyle('width:300px');
        $country->SetTitle(_t('REGISTER_LBL_COUNTRY'));
        $country->AddEvent(
            ON_CHANGE,
            "javascript:
                save_customer_to_dictionary();
            "
        );
        
        $tpl->SetVariable('txt_country', $country->Get());
        
        $tpl->ParseBlock('edit_customer/shipping');
        
        // END SHIPPING
        
        //==================================================================================
        // Save Button
        //==================================================================================
        
        $btnSave =& Piwi::createWidget('Button', 'save', _t('GLOBAL_SAVE'), STOCK_SAVE);
        $btnSave->AddEvent(
            ON_CLICK,
            "javascript:
            if (this.form.elements['txt_first_name'].value == '') {
                alert('" . _t('REGISTER_CUSTOMER_MISSING_FIRST_NAME') . "');
                this.form.elements['txt_first_name'].focus();
            }
            else if (this.form.elements['txt_last_name'].value == '') {
                alert('" . _t('REGISTER_CUSTOMER_MISSING_LAST_NAME') . "');
                this.form.elements['txt_last_name'].focus();
            } else {
                this.form.submit();
            }"
        );
        
        $tpl->SetVariable('save_button', $btnSave->Get());
        
        $tpl->ParseBlock('edit_customer');
        
        return $tpl->Get();
    }
    
    /**
     * Processes EditCustomer form
     *
     * @access public
     */
    public function CompleteEditCustomer()
    {
        $this->CheckPermission('EditCustomer');
        $model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Customers');
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $request =& Jaws_Request::getInstance();
        $names = array('id', 'txt_company', 'txt_abn', 'visibility', 'txt_email',
                       'txt_website', 'txt_first_name', 'txt_last_name',
                       'txt_phone', 'txt_fax', 'txt_mobile', 'txt_street1', 'txt_street2',
                       'txt_city', 'txt_state', 'txt_pcode', 'txt_country', 'same_shipping',
                       'txt_ship_street1', 'txt_ship_street2', 'txt_ship_city',
                       'txt_ship_state', 'txt_ship_pcode', 'txt_ship_country');
        
        $post = $request->get($names, 'post');
        
        if ($post['id'] > 0)
        {
            $model->UpdateCustomer($post['id'], $post['txt_first_name'], $post['txt_last_name'], $post['txt_company'], $post['txt_abn'], 
                                   $post['txt_phone'], $post['txt_fax'], $post['txt_mobile'], $post['txt_email'], $post['txt_website'],
                                   $post['txt_street1'], $post['txt_street2'], $post['txt_city'], $post['txt_state'], $post['txt_pcode'],
                                   $post['txt_country'], isset($post['visibility']));
        }
        else{
            $model->SaveNewCustomer($post['txt_first_name'], $post['txt_last_name'], $post['txt_company'], $post['txt_abn'],
                                    $post['txt_phone'], $post['txt_fax'], $post['txt_mobile'], $post['txt_email'], $post['txt_website'],
                                    $post['txt_street1'], $post['txt_street2'], $post['txt_city'], $post['txt_state'], $post['txt_pcode'],
                                    $post['txt_country'], isset($post['visibility']));
        }
        
        $post['id'] = $GLOBALS['db']->lastInsertID('register_customers');
        
        $model->UpdateShippingAddress(
            $post['id'], isset($post['same_shipping']), $post['txt_ship_street1'], $post['txt_ship_street2'],
            $post['txt_ship_city'], $post['txt_ship_state'], $post['txt_ship_pcode'], $post['txt_ship_country']);
        
        $dictionary->deleteKey('edit_customer');
        
        if ($dictionary->exists('edit_customer_order'))
        {
            Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=EditCustomerOrder&customer=' . $post['id']);
        }
        
        Jaws_Header::Location(BASE_SCRIPT . '?gadget=Register&action=Customers');
    }
}