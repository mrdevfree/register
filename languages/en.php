<?php
/**
 * Meta Data
 *
 * "Project-Id-Version:         Register"
 * "Last-Translater:            Tom Kaczocha <tom@crazydev.org>"
 * "Language-Team:              EN"
 * "MIME-Version:               1.0"
 * "Content-Type:               text/plain; charset=UTF-8"
 * "Content-Transfer-Encoding:  8bit"
 */

define('_EN_REGISTER_NAME',                             'Register');
define('_EN_REGISTER_DESCRIPTION',                      'A stock and order management system');

/* Layout */
define('_EN_REGISTER_LAYOUT_DISPLAY_FULL',              'Full Display');
define('_EN_REGISTER_LAYOUT_DISPLAY_FULL_DESC',         'Displays entire list of products with image and short description');
define('_EN_REGISTER_LAYOUT_DISPLAY_LIST',              'Product List Display');
define('_EN_REGISTER_LAYOUT_DISPLAY_LIST_DESC',         'Displays list of product names');
define('_EN_REGISTER_LAYOUT_DISPLAY_LIST_IN_CAT',       'Product List Display in Categories');
define('_EN_REGISTER_LAYOUT_DISPLAY_LIST_IN_CAT_DESC',  'Displays a list of product names under each category');

/* Product Management */
define('_EN_REGISTER_LBL_PRODUCTS',                 'Products');
define('_EN_REGISTER_LBL_NAME',                     'Name');
define('_EN_REGISTER_LBL_URL',                      'Url');
define('_EN_REGISTER_LBL_VISIBLE',                  'Visible');
define('_EN_REGISTER_LBL_ACTIONS',                  'Actions');
define('_EN_REGISTER_LBL_WHOLESALE_PRICE',          'Wholesale Price');
define('_EN_REGISTER_LBL_RETAIL_PRICE',             'Retail Price');
define('_EN_REGISTER_LBL_PRICE',                    'Price');
define('_EN_REGISTER_LBL_CATEGORIES',               'Categories');
define('_EN_REGISTER_LBL_CATEGORY',                 'Category');
define('_EN_REGISTER_LBL_NEW_PRODUCT'   ,           'New Product');
define('_EN_REGISTER_LBL_NEW_SUPPLIER',             'New Supplier');
define('_EN_REGISTER_LBL_DESCRIPTION',              'Description');
define('_EN_REGISTER_LBL_SHORT_DESC',               'Short Description');
define('_EN_REGISTER_LBL_LONG_DESC',                'Long Description');
define('_EN_REGISTER_LBL_COUNT',                    'Count');
define('_EN_REGISTER_LBL_ORDER_TOTAL',              'Order Total');
define('_EN_REGISTER_LBL_CREATED',                  'Created');
define('_EN_REGISTER_LBL_COMPANY',                  'Company');
define('_EN_REGISTER_COMMENTS',                     'Comments');
define('_EN_REGISTER_LBL_ABN',                      'ABN');
define('_EN_REGISTER_LBL_PHONE',                    'Phone');
define('_EN_REGISTER_LBL_FAX',                      'Fax');
define('_EN_REGISTER_LBL_MOBILE',                   'Mobile');
define('_EN_REGISTER_LBL_STREET',                   'Street');
define('_EN_REGISTER_LBL_CITY',                     'City');
define('_EN_REGISTER_LBL_STATE',                    'State');
define('_EN_REGISTER_LBL_PCODE',                    'Post Code/Zip');
define('_EN_REGISTER_LBL_COUNTRY',                  'Country');
define('_EN_REGISTER_LBL_FIRST_NAME',               'First Name');
define('_EN_REGISTER_LBL_LAST_NAME',                'Last Name');
define('_EN_REGISTER_LBL_COMPANY_REP',              'Company Rep');
define('_EN_REGISTER_LBL_COMPANY_NAME',             'Company Name');
define('_EN_REGISTER_LBL_ADDRESS',                  'Address');
define('_EN_REGISTER_LBL_PERSONAL',                 'Personal');
define('_EN_REGISTER_LBL_EMAIL',                    'Email');
define('_EN_REGISTER_LBL_WEBSITE',                  'Website');
define('_EN_REGISTER_LBL_DATE',                     'Date');
define('_EN_REGISTER_LBL_STATUS',                   'Status');
define('_EN_REGISTER_LBL_STOCK',                    'Stock');
define('_EN_REGISTER_LBL_NEW_PURCHASE_ORDER',       'New Purchase Order');
define('_EN_REGISTER_LBL_NEW_CUSTOMER_ORDER',       'New Customer Order');
define('_EN_REGISTER_LBL_CUSTOMER_ORDER',           'Customer Order');
define('_EN_REGISTER_LBL_CUSTOMER_ORDERS',          'Customer Orders');
define('_EN_REGISTER_LBL_PURCHASE_ORDER',           'Purchase Order');
define('_EN_REGISTER_LBL_PURCHASE_ORDERS',          'Purchase Orders');
define('_EN_REGISTER_LBL_REFERENCE_NO',             'Reference No');
define('_EN_REGISTER_LBL_PRODUCT_ID',               'Product ID');
define('_EN_REGISTER_LBL_ID',                       'ID');
define('_EN_REGISTER_LBL_PRODUCT_NAME',             'Product Name');
define('_EN_REGISTER_LBL_QTY',                      'Qty');
define('_EN_REGISTER_SELECT',                       'Select');
define('_EN_REGISTER_ADD_ROW',                      'Add Row');
define('_EN_REGISTER_DELETE_ROW',                   'Delete Row');
define('_EN_REGISTER_SAVE_EMAIL',                   'Save and Email');
define('_EN_REGISTER_SAVE_PRINT',                   'Save and Print');
define('_EN_REGISTER_LBL_COST',                     'Cost');
define('_EN_REGISTER_ITEM_PRICE',                   'Item Price');
define('_EN_REGISTER_DISCOUNT',                     'Discount');
define('_EN_REGISTER_SHIPPING',                     'Shipping');
define('_EN_REGISTER_TAX',                          'Tax');
define('_EN_REGISTER_TEST_NAME',                    'Test Name');
define('_EN_REGISTER_TEST_CASE',                    'Test Case');
define('_EN_REGISTER_RESULT',                       'Result');
define('_EN_REGISTER_TIME',                         'Time');
define('_EN_REGISTER_RUN_TESTS',                    'Run Tests');
define('_EN_REGISTER_TOTAL_EXECUTION_TIME',         'Total Execution Time');
define('_EN_REGISTER_ENVIRONMENT',                  'Environment');
define('_EN_REGISTER_LBL_DEVELOPMENT',              'Development');
define('_EN_REGISTER_LBL_PRODUCTION',               'Production');
define('_EN_REGISTER_PRODUCT_TYPE',                 'Product Type');
define('_EN_REGISTER_SHIPPING_ADDRESS',             'Shipping Address');
define('_EN_REGISTER_BILLING_ADDRESS',              'Billing Address');
define('_EN_REGISTER_SAME_SHIPPING_ADDRESS',        'Same Shipping Address');

define('_EN_REGISTER_PRODUCT_DELETE_MESSAGE',       'Are you sure you want to delete this product?');
define('_EN_REGISTER_CATEGORY_DELETE_MESSAGE',      'Are you sure you want to delete this category?');
define('_EN_REGISTER_SUPPLIER_DELETE_MESSAGE',      'Are you sure you want to delete this supplier?');
define('_EN_REGISTER_CUSTOMER_DELETE_MESSAGE',      'Are you sure you want to delete this customer?');

define('_EN_REGISTER_ENVIRONMENT_PRODUCTION',       '<b>Production</b> offers greater protection for your data and disabled development options. Use this option when the application is live.');
define('_EN_REGISTER_ENVIRONMENT_DEVELOPMENT',      '<b>Development</b> will allow the recreation of the database and therefore risk losing data.');
define('_EN_REGISTER_COMPANY_INFO',                 'This informaton is used throughout the Register gadget. It is used on Customer Invoices and emails.');

define('_EN_REGISTER_IMAGE',                        'Image');
define('_EN_REGISTER_LBL_EDIT',                     'edit');
define('_EN_REGISTER_UPLOAD',                       'Upload');

/* Menu Resources */
define('_EN_REGISTER_SUMMARY',                      'Summary');
define('_EN_REGISTER_PRODUCT',                      'Product');
define('_EN_REGISTER_CATEGORY',                     'Category');
define('_EN_REGISTER_SUPPLIER',                     'Supplier');
define('_EN_REGISTER_ORDERS',                       'Orders');
define('_EN_REGISTER_CUSTOMER',                     'Customer');
define('_EN_REGISTER_REPORTS',                      'Reports');
define('_EN_REGISTER_SETTINGS',                     'Settings');
define('_EN_REGISTER_CUSTOMERS',                    'Customers');
define('_EN_REGISTER_SUPPLIERS',                    'Suppliers');

 /* Success Responses */
define('_EN_REGISTER_PRODUCT_ADDED',                'The product has been added');
define('_EN_REGISTER_CATEGORY_ADDED',               'The category has been added');
define('_EN_REGISTER_SUPPLIER_ADDED',               'The supplier has been added');
define('_EN_REGISTER_CUSTOMER_ADDED',               'The customer has been added');
define('_EN_REGISTER_ORDER_ADDED',                  'The order has been added');

define('_EN_REGISTER_PRODUCT_UPDATED',              'The product has been updated');
define('_EN_REGISTER_CATEGORY_UPDATED',             'The category has been updated');
define('_EN_REGISTER_SUPPLIER_UPDATED',             'The supplier has been updated');
define('_EN_REGISTER_CUSTOMER_UPDATED',             'The customer has been updated');
define('_EN_REGISTER_ORDER_UPDATED',                'The order has been updated');
define('_EN_REGISTER_ORDER_ITEM_UPDATED',           'The order item has been updated');
define('_EN_REGISTER_SETTINGS_UPDATED',             'The settings have been updated');

define('_EN_REGISTER_PRODUCT_DELETED',              'Product has been deleted');
define('_EN_REGISTER_CATEGORY_DELETED',             'Category has been deleted');
define('_EN_REGISTER_SUPPLIER_DELETED',             'Supplier has been deleted');
define('_EN_REGISTER_CUSTOMER_DELETED',             'Customer has been deleted');
define('_EN_REGISTER_ORDER_DELETED',                'Order has been deleted');
define('_EN_REGISTER_ORDER_ITEM_DELETED',           'Order item has been deleted');

/* Errors */
define('_EN_REGISTER_ERROR_CALCULATING_PRICE',      'Error Calculating Price');
define('_EN_REGISTER_ERROR_IMAGE_SIZE',             'Maximum image size is 800x800');

/* Missing Form Fields Errors */
define('_EN_REGISTER_PRODUCT_MISSING_NAME',         'Product name is missing');
define('_EN_REGISTER_PRODUCT_MISSING_SHORT_DESC',   'Short description is missing');
define('_EN_REGISTER_CATEGORY_MISSING_NAME',        'Category name is missing');
define('_EN_REGISTER_SUPPLIER_MISSING_NAME',        'Company name is missing');
define('_EN_REGISTER_CUSTOMER_MISSING_FIRST_NAME',  'First name is missing');
define('_EN_REGISTER_CUSTOMER_MISSING_LAST_NAME',   'Last name is missing');

/* ID Errors */
define('_EN_REGISTER_CATEGORY_ID_ERROR',            'No category id supplied');
define('_EN_REGISTER_PRODUCT_ID_ERROR',             'No product id supplied');
define('_EN_REGISTER_SUPPLIER_ID_ERROR',            'No supplier id supplied');
define('_EN_REGISTER_CUSTOMER_ID_ERROR',            'No customer id supplied');
define('_EN_REGISTER_ORDER_ID_ERROR',               'No order id supplied');

define('_EN_REGISTER_INVALID_KEY_ERROR',            'Missing or invalid key supplied');
define('_EN_REGISTER_KEY_EXISTS_ERROR',             'Key already exists');
define('_EN_REGISTER_KEY_NOT_EXISTS_ERROR',         'Key does not exist');

/* Not Added Errors */
define('_EN_REGISTER_ERROR_PRODUCT_NOT_ADDED',      'There was a problem adding the product');
define('_EN_REGISTER_ERROR_CATEGORY_NOT_ADDED',     'There was a problem adding the category');
define('_EN_REGISTER_ERROR_SUPPLIER_NOT_ADDED',     'There was a problem adding the supplier');
define('_EN_REGISTER_ERROR_CUSTOMER_NOT_ADDED',     'There was a problem adding the customer');
define('_EN_REGISTER_ERROR_ORDER_NOT_ADDED',        'There was a problem adding the order');
define('_EN_REGISTER_ERROR_ORDER_ITEM_NOT_ADDED',   'There was a problem adding an order item');

/* Not Updated Errors */
define('_EN_REGISTER_ERROR_PRODUCT_NOT_UPDATED',    'Product not updated');
define('_EN_REGISTER_ERROR_CATEGORY_NOT_UPDATED',   'Category not updated');
define('_EN_REGISTER_ERROR_SUPPLIER_NOT_UPDATED',   'Supplier not updated');
define('_EN_REGISTER_ERROR_CUSTOMER_NOT_UPDATED',   'Customer not updated');
define('_EN_REGISTER_ERROR_ORDER_NOT_UPDATED',      'Order not updated');
define('_EN_REGISTER_ERROR_ORDER_ITEM_NOT_UPDATED', 'Order item not updated');

/* Not Deleted Errors */
define('_EN_REGISTER_ERROR_PRODUCT_NOT_DELETED',    'Product not deleted');
define('_EN_REGISTER_ERROR_CATEGORY_NOT_DELETED',   'Category not deleted');
define('_EN_REGISTER_ERROR_SUPPLIER_NOT_DELETED',   'Supplier not deleted');
define('_EN_REGISTER_ERROR_CUSTOMER_NOT_DELETED',   'Customer not deleted');
define('_EN_REGISTER_ERROR_ORDER_NOT_DELETED',      'Order not deleted');
define('_EN_REGISTER_ERROR_ORDER_ITEM_NOT_DELETED', 'Order item not deleted');
define('_EN_REGISTER_ERROR_KEY_VAL_NOT_DELETED',    'Key-Value pair not deleted');

define('_EN_REGISTER_ERROR_CATEGORY_NOT_EXIST',     'Category does not exist');
define('_EN_REGISTER_ERROR_PRODUCT_NOT_EXIST',      'Product does not exist');

/* Faied Retrieval Errors */
define('_EN_REGISTER_FAILED_GETTING_PRODUCT',       'Failed retrieving product');
define('_EN_REGISTER_FAILED_GETTING_CATEGORY',      'Failed retrieving category');
define('_EN_REGISTER_FAILED_GETTING_SUPPLIER',      'Failed retrieving supplier');
define('_EN_REGISTER_FAILED_GETTING_CUSTOMER',      'Failed retrieving customer');
define('_EN_REGISTER_FAILED_GETTING_ORDER',         'Failed retrieving order');

/* Delation Errors */
define('_EN_REGISTER_ERROR_PRODUCT_HAS_ORDERS',     'Cannot delete products with orders');
define('_EN_REGISTER_ERROR_CATEGORY_HAS_PRODUCTS',  'Cannot delete categories with products');
define('_EN_REGISTER_ERROR_SUPPLIER_HAS_PRODUCTS',  'Cannot delete suppliers with products');
define('_EN_REGISTER_ERROR_CUSTOMER_HAS_ORDERS',    'Cannot delete customers with orders');
define('_EN_REGISTER_ERROR_ORDER_IN_TRANSIT',       'Cannot delete an order that has been \'Filled\' or is \'In-Transit\' or \'Closed\'');