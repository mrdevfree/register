<?php
/**
 * Register Gadget Front HTML
 *
 * @category    Gadget
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterHTML extends Jaws_GadgetHTML
{
    /**
     * Constructor
     *
     * @access public
     */
    public function RegisterHTML()
    {
        $this->Init('Register');
    }
    
    /**
     * Default HTML Action
     *
     * @access public
     */
    public function DefaultAction()
    {
        //$layoutGadget = $GLOBALS['app']->LoadGadget('Register', 'LayoutHTML');
        //return $layoutGadget->Display();
    }
    
}