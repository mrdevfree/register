<?php
/**
 * Register - URL List gadget hook
 *
 * @category    GadgetHook
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterURLListHook
{
    /**
     * Returns an array with all available items the Menu gadget
     * can use
     *
     * @access public
     * @return array    urls
     */
    function Hook()
    {
        $urls[] = array();
        //$urls[] = array('url' => $GLOBALS['app']->Map->GetURLFor('Register', 'DefaultAction'),
        //                'title' => _t('REGISTER_NAME'));
        
        $model = $GLOBALS['app']->loadGadget('Register', 'Model');
        
        return $urls;
    
    }
    
}