<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Model Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterModelTests extends TestCase
{
    private $model;
    
    /**
     * Constructor
     *
     * Initialises Models
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model');
    }
    
    /**
     * Test Method returns Correct Value
     *
     * @access public
     */
    public function testGetUrlValue()
    {
        $key1 = 'product';
        $key2 = 'customer';
        $value1 = '1';
        $value2 = '2';
        
        $var = '';
        $var .=         $key1 . '=' . $value1;
        $var .= '&' .   $key2 . '=' . $value2;
        
        $this->assertEquals($value1, $this->model->getUrlValue($var, $key1));
        $this->assertEquals($value2, $this->model->getUrlValue($var, $key2));
        $this->assertNotEquals($value2, $this->model->getUrlValue($var, $key1));
    }
    
    /**
     * Test Method returns the Correct array
     *
     * @access public
     */
    public function testGetSelectedArray()
    {
        $products = array(1, 2, 10);
        $qty = array(2, 10, 100);
        $discounts = array(5.50, 10.99, 2);
        
        $var = '';
        
        for ($i = 0; $i < count($products); $i++)
        {
            $var .= 'product=' . $products[$i];
            $var .= '&qty=' . $qty[$i];
            $var .= '&discount=' . $discounts[$i];
            $var .= '$';
        }
        
        $p = $this->model->getSelectedArray($var, 'product');
        $q = $this->model->getSelectedArray($var, 'qty');
        $d = $this->model->getSelectedArray($var, 'discount');
        
        for ($i = 0; $i < count($products); $i++)
        {
            $this->assertEquals($products[$i], $p[$i]);
            $this->assertEquals($qty[$i], $q[$i]);
            $this->assertEquals($discounts[$i], $d[$i]);
        }
        
    }
    
    /**
     * Tests Calculating Order Total
     *
     * @access public
     */
    public function testCalculateCustomerOrderTotal()
    {
        $order = '';
        $var = '';
        
        $customer_id = 1;
        $date = '';
        $status = 1;
        $comments = 'this is a comment';
        $shipping = 2.00;
        $discount = 1.00;
        $tax = 3.00;
        
        $order  = 'customer_id=' . $customer_id;
        $order .= '&date=' . $date;
        $order .= '&status=' . $status;
        $order .= '&comments=' . $comments;
        $order .= '&shipping=' . $shipping;
        $order .= '&discount=' . $discount;
        $order .= '&tax=' . $tax;
        
        $products = array(1, 2, 1);
        $qty = array(2, 10, 100);
        $discounts = array(5.50, 10.99, 2);
        
        for ($i = 0; $i < count($products); $i++)
        {
            $var .= 'product=' . $products[$i];
            $var .= '&qty=' . $qty[$i];
            $var .= '&discount=' . $discounts[$i];
            $var .= '$';
        }
        
        $total = $this->model->CalculateCustomerOrderTotal($order, $var);
        
        $this->assertEquals(260.41, $total);
        
    }
    
}