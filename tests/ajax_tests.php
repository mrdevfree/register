<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';
require_once JAWS_PATH . 'gadgets/Register/AdminAjax.php';

/**
 * Register Gadget Ajax Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterAjaxTests extends TestCase
{
    private $model;
    private $db_model;
    private $o_model;
    private $p_admin_model;
    
    /**
     * Test Case Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct(JAWS_PATH . 'gadgets/Register/tests/ajax_tests.php');
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'AdminAjax');
        $this->o_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
        $this->p_admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
    }
    
    /**
     * Tests the correct loading of AdminAjax object
     *
     * @access public
     */
    public function testAjaxModelNotNull()
    {
        $this->assertNotNull($this->model);
        $this->assertTrue($this->model instanceof Jaws_Ajax);
    }
    
    /**
     * Tests Deleting Product that does not
     * exist.
     *
     * @access public
     */
    public function testDeleteFakeProduct()
    {
        $product_id = 100;
        
        $result = $this->model->DeleteProduct($product_id);
        
        $this->assertEquals(_t('REGISTER_ERROR_PRODUCT_NOT_EXIST'), $result[0]['message']);
    }
    
    /**
     * Tests Deleting Category that does not
     * exist.
     *
     * @access public
     */
    public function testDeleteFakeCategory()
    {
        $category_id = 100;
        
        $result = $this->model->DeleteCategory($category_id);
        
        $this->assertEquals(_t('REGISTER_ERROR_CATEGORY_NOT_EXIST'), $result[0]['message']);
    }
    
    /**
     * Tests Deleting Category
     *
     * @access public
     */
    public function testDeleteCategory()
    {
        $category_id = 1;
        
        $result = $this->model->DeleteCategory($category_id);
        
        $this->assertEquals($category_id, $result[0]['data']['id']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Deleting Supplier
     *
     * @access public
     */
    public function testDeleteSupplier()
    {
        $supplier_id = 1;
        
        $result = $this->model->DeleteSupplier($supplier_id);
        
        $this->assertEquals($supplier_id, $result[0]['data']['id']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Deleting Customer
     *
     * @access public
     */
    public function testDeleteCustomer()
    {
        $customer_id = 3;
        
        $result = $this->model->DeleteCustomer($customer_id);
        
        $this->assertEquals($customer_id, $result[0]['data']['id']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests that function returns the correct prices
     * in the correct format.
     *
     * @access public
     */
    public function testGetProductPrices()
    {
        $IDs = array ('1', '2');
        $prices = array('2.50', '1.99');
        
        $json_string = $this->model->GetProductPrices(implode(',', $IDs));
        
        $this->assertTrue(strlen($json_string) > 0);
        
        $result = json_decode($json_string);
        
        for ($i = 0; $i < count($IDs); $i++)
        {
            $this->assertEquals($IDs[$i], $result[$i]->product_id);
            $this->assertEquals($prices[$i], $result[$i]->price);
        }
    }
    
    /**
     * Tests the Deletion of a Purchase Order
     *
     * @access public
     */
    public function testDeletePurchaseOrder()
    {
        $purchase_order_id = 1;
        
        $orders = $this->o_model->GetAllPurchaseOrders();
        $this->assertEquals(2, count($orders), 'There should be 2 purchase orders');
        
        $result = $this->model->DeletePurchaseOrder($purchase_order_id);
        
        $this->assertEquals($purchase_order_id, $result[0]['data']['id'], 'IDs should equal');
        
        $orders = $this->o_model->GetAllPurchaseOrders();
        
        $this->assertEquals(1, count($orders), 'There should be only 1 purchase order after deletion');
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests the Deletion of a Customer Order
     *
     * @access public
     */
    public function testDeleteCustomerOrder()
    {
        $customer_order_id = 1;
        
        $orders = $this->o_model->GetAllCustomerOrders();
        $this->assertEquals(2, count($orders), 'There should be 2 customer orders');
        
        $result = $this->model->DeleteCustomerOrder($customer_order_id);
        
        $this->assertEquals($customer_order_id, $result[0]['data']['id'], 'IDs should equal');
        
        $orders = $this->o_model->GetAllCustomerOrders();
        
        $this->assertEquals(1, count($orders), 'There should be only 1 customer order after deletion');
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests saving of settings to Dictionary
     *
     * @access public
     */
    public function testSaveSettings()
    {
        $env = 1;
        $var = 'environment=' . $env;
        
        $result = $this->model->SaveSettings($var);
        
        $this->assertEquals(TRUE, $result);
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $environment = $dictionary->get('settings/environment');
        
        $this->assertEquals($env, $environment['value']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving product being edited to Dictionary
     *
     * @access public
     */
    public function testSaveProductToDictionary()
    {
        $key = 'edit_product';
        
        $name = 'new product';
        $url = '';
        $visible = TRUE;
        $category = 1;
        $supplier = 1;
        $wholesale = 0.30;
        $retail = 1.99;
        $short = 'this is a short';
        $long = 'this is a longer description';
        
        $product = '';
        $product .= 'name=' . $name;
        $product .= '&url=' . $url;
        $product .= '&visible=' . $visible;
        $product .= '&category=' . $category;
        $product .= '&supplier=' . $supplier;
        $product .= '&wPrice=' . $wholesale;
        $product .= '&rPrice=' . $retail;
        $product .= '&sDescription=' . $short;
        $product .= '&lDescription=' . $long;
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($product));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        $this->assertEquals($name, $model->getUrlValue($result, 'name'));
        $this->assertEquals($url, $model->getUrlValue($result, 'url'));
        $this->assertEquals($visible, $model->getUrlValue($result, 'visible'));
        $this->assertEquals($category, $model->getUrlValue($result, 'category'));
        $this->assertEquals($supplier, $model->getUrlValue($result, 'supplier'));
        $this->assertEquals($wholesale, $model->getUrlValue($result, 'wPrice'));
        $this->assertEquals($retail, $model->getUrlValue($result, 'rPrice'));
        $this->assertEquals($short, $model->getUrlValue($result, 'sDescription'));
        $this->assertEquals($long, $model->getUrlValue($result, 'lDescription'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving category being edited to Dictionary
     *
     * @access public
     */
    public function testSaveCategoryToDictionary()
    {
        $key = 'edit_category';
        
        $name = 'new category';
        $visible = TRUE;
        $description = 'this is a short description';
        
        $category = '';
        $category .= 'name=' . $name;
        $category .= '&visible=' . $visible;
        $category .= '&description=' . $description;
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($category));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        $this->assertEquals($name, $model->getUrlValue($result, 'name'));
        $this->assertEquals($visible, $model->getUrlValue($result, 'visible'));
        $this->assertEquals($description, $model->getUrlValue($result, 'description'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving supplier being edited to Dictionary
     *
     * @access public
     */
    public function testSaveSupplierToDictionary()
    {
        $key = 'edit_supplier';
        
        $name = 'new supplier';
        $abn = '2541-235-5658';
        $site = 'www.supplier.com';
        $visible = TRUE;
        $phone = '03 9305 1536';
        $fax = '03 9305 1536';
        $f_name = 'Tony';
        $l_name = 'Something';
        $email = 'tony@supplier.com';
        $mobile = '0412 568 125';
        $street1 = '33 Johnnahn St';
        $street2 = 'Soso Buildings';
        $city = 'Some City';
        $state = 'Some State';
        $pCode = '24156';
        $country = 'Australia';
        
        $supplier = '';
        $supplier .= 'name=' . $name;
        $supplier .= '&abn=' . $abn;
        $supplier .= '&website=' . $site;
        $supplier .= '&visible=' . $visible;
        $supplier .= '&phone=' . $phone;
        $supplier .= '&fax=' . $fax;
        $supplier .= '&f_name=' . $f_name;
        $supplier .= '&l_name=' . $l_name;
        $supplier .= '&email=' . $email;
        $supplier .= '&mobile=' . $mobile;
        $supplier .= '&street1=' . $street1;
        $supplier .= '&street2=' . $street2;
        $supplier .= '&city=' . $city;
        $supplier .= '&state=' . $state;
        $supplier .= '&pcode=' . $pCode;
        $supplier .= '&country=' . $country;
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($supplier));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        $this->assertEquals($name, $model->getUrlValue($result, 'name'));
        $this->assertEquals($abn, $model->getUrlValue($result, 'abn'));
        $this->assertEquals($site, $model->getUrlValue($result, 'website'));
        $this->assertEquals($visible, $model->getUrlValue($result, 'visible'));
        $this->assertEquals($phone, $model->getUrlValue($result, 'phone'));
        $this->assertEquals($fax, $model->getUrlValue($result, 'fax'));
        $this->assertEquals($f_name, $model->getUrlValue($result, 'f_name'));
        $this->assertEquals($l_name, $model->getUrlValue($result, 'l_name'));
        $this->assertEquals($email, $model->getUrlValue($result, 'email'));
        $this->assertEquals($mobile, $model->getUrlValue($result, 'mobile'));
        $this->assertEquals($street1, $model->getUrlValue($result, 'street1'));
        $this->assertEquals($street2, $model->getUrlValue($result, 'street2'));
        $this->assertEquals($city, $model->getUrlValue($result, 'city'));
        $this->assertEquals($state, $model->getUrlValue($result, 'state'));
        $this->assertEquals($pCode, $model->getUrlValue($result, 'pcode'));
        $this->assertEquals($country, $model->getUrlValue($result, 'country'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving customer being edited to Dictionary
     *
     * @access public
     */
    public function testSaveCustomerToDictionary()
    {
        $key = 'edit_customer';
        
        $name = 'customer company';
        $abn = '2541-235-5658';
        $site = 'www.supplier.com';
        $visible = TRUE;
        $phone = '03 9305 1536';
        $fax = '03 9305 1536';
        $f_name = 'Tony';
        $l_name = 'Something';
        $email = 'tony@supplier.com';
        $mobile = '0412 568 125';
        $street1 = '33 Johnnahn St';
        $street2 = 'Soso Buildings';
        $city = 'Some City';
        $state = 'Some State';
        $pCode = '24156';
        $country = 'Australia';
        $ship_street1 = '25 Main St';
        $ship_street2 = '';
        $ship_city = 'Greenborough';
        $ship_state = 'NSW';
        $ship_pCode = '2512';
        $ship_country = 'Australia';
        
        $customer = '';
        $customer .= 'company=' . $name;
        $customer .= '&abn=' . $abn;
        $customer .= '&website=' . $site;
        $customer .= '&visible=' . $visible;
        $customer .= '&phone=' . $phone;
        $customer .= '&fax=' . $fax;
        $customer .= '&f_name=' . $f_name;
        $customer .= '&l_name=' . $l_name;
        $customer .= '&email=' . $email;
        $customer .= '&mobile=' . $mobile;
        $customer .= '&street1=' . $street1;
        $customer .= '&street2=' . $street2;
        $customer .= '&city=' . $city;
        $customer .= '&state=' . $state;
        $customer .= '&pcode=' . $pCode;
        $customer .= '&country=' . $country;
        $customer .= '&ship_street1=' . $ship_street1;
        $customer .= '&ship_street2=' . $ship_street2;
        $customer .= '&ship_city=' . $ship_city;
        $customer .= '&ship_state=' . $ship_state;
        $customer .= '&ship_pcode=' . $ship_pCode;
        $customer .= '&ship_country=' . $ship_country;
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($customer));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        $this->assertEquals($name, $model->getUrlValue($result, 'company'));
        $this->assertEquals($abn, $model->getUrlValue($result, 'abn'));
        $this->assertEquals($site, $model->getUrlValue($result, 'website'));
        $this->assertEquals($visible, $model->getUrlValue($result, 'visible'));
        $this->assertEquals($phone, $model->getUrlValue($result, 'phone'));
        $this->assertEquals($fax, $model->getUrlValue($result, 'fax'));
        $this->assertEquals($f_name, $model->getUrlValue($result, 'f_name'));
        $this->assertEquals($l_name, $model->getUrlValue($result, 'l_name'));
        $this->assertEquals($email, $model->getUrlValue($result, 'email'));
        $this->assertEquals($mobile, $model->getUrlValue($result, 'mobile'));
        $this->assertEquals($street1, $model->getUrlValue($result, 'street1'));
        $this->assertEquals($street2, $model->getUrlValue($result, 'street2'));
        $this->assertEquals($city, $model->getUrlValue($result, 'city'));
        $this->assertEquals($state, $model->getUrlValue($result, 'state'));
        $this->assertEquals($pCode, $model->getUrlValue($result, 'pcode'));
        $this->assertEquals($country, $model->getUrlValue($result, 'country'));
        
        $this->assertEquals($ship_street1, $model->getUrlValue($result, 'ship_street1'));
        $this->assertEquals($ship_street2, $model->getUrlValue($result, 'ship_street2'));
        $this->assertEquals($ship_city, $model->getUrlValue($result, 'ship_city'));
        $this->assertEquals($ship_state, $model->getUrlValue($result, 'ship_state'));
        $this->assertEquals($ship_pCode, $model->getUrlValue($result, 'ship_pcode'));
        $this->assertEquals($ship_country, $model->getUrlValue($result, 'ship_country'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving Purchase Order being edited to Dictionary
     *
     * @access public
     */
    public function testSavePurchaseOrderToDictionary()
    {
        $key = 'edit_purchase_order';
        
        $supplier_id = 1;
        $date = '2012-09-27';
        $ref_no = '2562-2564';
        $status = 1;
        $comments = 'this is a test comment';
        $shipping = 5.00;
        $discount = 1.00;
        $tax = 2.50;
        
        $purchase_order = '';
        $purchase_order .= 'supplier_id=' . $supplier_id;
        $purchase_order .= '&date=' . $date;
        $purchase_order .= '&ref=' . $ref_no;
        $purchase_order .= '&status=' . $status;
        $purchase_order .= '&comments=' . $comments;
        $purchase_order .= '&shipping=' . $shipping;
        $purchase_order .= '&discount=' . $discount;
        $purchase_order .= '&tax=' . $tax;
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($purchase_order));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        
        $this->assertEquals($supplier_id, $model->getUrlValue($result, 'supplier_id'));
        $this->assertEquals($date, $model->getUrlValue($result, 'date'));
        $this->assertEquals($ref_no, $model->getUrlValue($result, 'ref'));
        $this->assertEquals($status, $model->getUrlValue($result, 'status'));
        $this->assertEquals($comments, $model->getUrlValue($result, 'comments'));
        $this->assertEquals($shipping, $model->getUrlValue($result, 'shipping'));
        $this->assertEquals($discount, $model->getUrlValue($result, 'discount'));
        $this->assertEquals($tax, $model->getUrlValue($result, 'tax'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving Purchase Order Products being edited to Dictionary
     *
     * @access public
     */
    public function testSavePurchaseOrderProductsToDictionary()
    {
        $key = 'edit_purchase_order_products';
        
        $products = array(1, 2);
        $qty = array(10, 20);
        $prices = array(2.50, 1.99);
        $discounts = array(0, 1.00);
        
        $purchase_order_products = '';
        
        for ($i = 0; $i < count($products); $i++)
        {
            $purchase_order_products .= 'product=' . $products[$i];
            $purchase_order_products .= '&qty=' . $qty[$i];
            $purchase_order_products .= '&price=' . $prices[$i];
            $purchase_order_products .= '&discount=' . $discounts[$i];
            $purchase_order_products .= '$';
        }
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($purchase_order_products));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        
        $product_results = $model->getSelectedArray($result, 'product');
        $qty_results = $model->getSelectedArray($result, 'qty');
        $price_results = $model->getSelectedArray($result, 'price');
        $discount_results = $model->getSelectedArray($result, 'discount');
        
        $this->assertEquals(count($products), count($product_results));
        
        for ($i = 0; $i < count($products); $i++)
        {
            $this->assertEquals($products[$i], $product_results[$i]);
            $this->assertEquals($qty[$i], $qty_results[$i]);
            $this->assertEquals($prices[$i], $price_results[$i]);
            $this->assertEquals($discounts[$i], $discount_results[$i]);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving Customer Order being edited to Dictionary
     *
     * @access public
     */
    public function testSaveCustomerOrderToDictionary()
    {
        $key = 'edit_customer_order';
        
        $customer_id = 1;
        $date = '2012-09-27';
        $status = 1;
        $comments = 'this is a test comment';
        $shipping = 5.00;
        $discount = 1.00;
        $tax = 2.50;
        
        $customer_order = '';
        $customer_order .= 'customer_id=' . $customer_id;
        $customer_order .= '&date=' . $date;
        $customer_order .= '&status=' . $status;
        $customer_order .= '&comments=' . $comments;
        $customer_order .= '&shipping=' . $shipping;
        $customer_order .= '&discount=' . $discount;
        $customer_order .= '&tax=' . $tax;
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($customer_order));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        
        $this->assertEquals($customer_id, $model->getUrlValue($result, 'customer_id'));
        $this->assertEquals($date, $model->getUrlValue($result, 'date'));
        $this->assertEquals($status, $model->getUrlValue($result, 'status'));
        $this->assertEquals($comments, $model->getUrlValue($result, 'comments'));
        $this->assertEquals($shipping, $model->getUrlValue($result, 'shipping'));
        $this->assertEquals($discount, $model->getUrlValue($result, 'discount'));
        $this->assertEquals($tax, $model->getUrlValue($result, 'tax'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Saving Customer Order Products being edited to Dictionary
     *
     * @access public
     */
    public function testSaveCustomerOrderProductsToDictionary()
    {
        $key = 'edit_customer_order_products';
        
        $products = array(1, 2);
        $qty = array(10, 20);
        $discounts = array(0, 1.00);
        
        $customer_order_products = '';
        
        for ($i = 0; $i < count($products); $i++)
        {
            $customer_order_products .= 'product=' . $products[$i];
            $customer_order_products .= '&qty=' . $qty[$i];
            $customer_order_products .= '&discount=' . $discounts[$i];
            $customer_order_products .= '$';
        }
        
        $this->model->SaveKeyValueToDictionary($key, urlencode($customer_order_products));
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model');
        
        $result = $dictionary->get($key);
        $this->assertNotNull($result);
        
        $result = urldecode($result['value']);
        
        $product_results = $model->getSelectedArray($result, 'product');
        $qty_results = $model->getSelectedArray($result, 'qty');
        $discount_results = $model->getSelectedArray($result, 'discount');
        
        $this->assertEquals(count($products), count($product_results));
        
        for ($i = 0; $i < count($products); $i++)
        {
            $this->assertEquals($products[$i], $product_results[$i]);
            $this->assertEquals($qty[$i], $qty_results[$i]);
            $this->assertEquals($discounts[$i], $discount_results[$i]);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests adding Tests to Dictionary
     *
     * @access public
     */
    public function testAddTestsToDictionary()
    {
        $test_collection = array('RegisterProductsTests', 'RegisterCategoryTests');
        $test_string = implode('$', $test_collection);
        
        $this->model->addTestsToDictionary($test_string);
        
        $result = $this->model->getTestsFromDictionary();
        
        $tests = explode('$', $result);
        
        $this->assertEquals($test_collection[0], $tests[0]);
        $this->assertEquals($test_collection[1], $tests[1]);
        
        $this->resetDatabase();
    }
    
    /**
     * Tests saving image crop to dictionary
     *
     * @access public
     */
    public function testSaveCropToDictionary()
    {
        $image = 'gadgets/Register/images/products/temp/product.1.1.png';
        $width = 100;
        $height = 100;
        $x = 10;
        $y = 10;
        
        $this->model->SaveCropToDictionary($image, $x, $y, $width, $height);
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        
        $result = $dictionary->get($image);
        
        $this->assertNotNull($result);
        
        $this->assertEquals($width, $this->db_model->getUrlValue($result['value'], 'width'));
        $this->assertEquals($height, $this->db_model->getUrlValue($result['value'], 'height'));
        $this->assertEquals($x, $this->db_model->getUrlValue($result['value'], 'x'));
        $this->assertEquals($y, $this->db_model->getUrlValue($result['value'], 'y'));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Trying to crop image without dictionary
     * key value settings
     *
     * @access public
     */
    public function testCompleteImageCropFail()
    {
        $image = 'gadgets/Register/images/products/temp/product.jpg';
        $result = $this->model->CompleteImageCrop($image);
        
        $this->assertStrictFalse($result);
    }
    
    /**
     * Tests croping an image successfully
     *
     * @access public
     */
    public function testCompleteImageCropSuccess()
    {
        $path = 'gadgets/Register/images/products/';
        $image = 'temp/product.1.1.png';
        $width = 200;
        $height = 200;
        $x = 100;
        $y = 100;
        $testImage = 'product.test.png';
        
        $this->assertStrictTrue(file_exists($path . $image));
        
        $this->model->SaveCropToDictionary($path . $image, $x, $y, $width, $height);
        
        $dictionary = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $result = $dictionary->get($path . $image);
        
        $this->assertNotNull($result);
        
        $result = $this->model->CompleteImageCrop($path . $image, $testImage);
        
        $this->assertStrictTrue($result);
        
        $this->assertStrictTrue(file_exists($path . 'origin/' . $testImage));
        $this->assertStrictTrue(is_file($path . 'origin/' . $testImage));
        
        unlink($path . 'origin/' . $testImage);
        $this->assertStrictFalse(file_exists($path . 'origin/' . $testImage));
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}