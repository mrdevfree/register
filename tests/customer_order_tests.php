<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Customer Order Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterCustomerOrderTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     *
     * Initialises Admin Model
     */
    function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $this->admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Test getting all Customer Orders
     *
     * @access public
     */
    function testGetAllCustomerOrders()
    {
        $orders = $this->model->GetAllCustomerOrders();
        
        $this->assertTrue(2 == count($orders), "Should return 2 customer orders");
    }
    
    /**
     * Test getting Purchase Order by ID
     *
     * @access public
     */
    function testGetCustomerOrderById()
    {
        $order_id = 1;
        $customer_id = 1;
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertEquals($customer_id, $order[0]['customer_id']);
        $this->assertStrictTrue(isset($order[0]['product_name']));
    }
    
    /**
     * Test Getting Customer Order Products
     *
     * @access public
     */
    public function testGetCustomerOrderProductsByCustomerOrderId()
    {
        $customer_order_id = 1;
        
        $products = $this->model->GetCustomerOrderProductsByCustomerOrderId($customer_order_id);
        
        $this->assertNotEmpty($products, 'Array should have 2 products');
        $this->assertTrue(count($products) == 2, 'Count should be 2');
    }
    
    /**
     * Tests Getting Customer Order Count for Products
     *
     * @access public
     */
    public function testGetCustomerOrderCountForProduct()
    {
        $product_id = 1;
        $product_1_count = 2;
        
        $count = $this->model->GetCustomerOrderCountForProduct($product_id);
        $this->assertEquals($product_1_count, $count);
        
        $product_id = 10;
        $product_10_count = 0;
        
        $count = $this->model->GetCustomerOrderCountForProduct($product_id);
        $this->assertEquals($product_10_count, $count);
    }
    
    /**
     * Test Adding New Customer Order
     *
     * @access public
     */
    public function testAddNewCustomerOrder()
    {
        $customer_id = 1;
        $date = '2012-09-09 07:27:12';
        $status = 1;
        $products = array(1, 2);
        $qty = array(10, 20);
        $prices = array(2.50, 1.99);
        $discounts = array(0, 0);
        $tax = 1.50;
        $discount = 0.00;
        $comments = 'No discount for this order';
        $shipping = 10.00;
        
        $id = $this->admin_model->SaveNewCustomerOrder(
            $customer_id, $date, $status, $products, $qty, $prices, $discounts,
            $comments, $discount, $tax, $shipping);
        
        $this->assertNotNull($id, 'ID should not be null');
        $this->assertTrue($id > 2, '2 Orders already exist. This order ID must be greater');
        
        $orders = $this->model->GetAllCustomerOrders();
        
        $this->assertTrue(count($orders) == 3, 'There should be three orders');
        
        $order = $this->model->GetCustomerOrderById($id);
        
        $this->assertNotNull($order, 'Order should not be null');
        
        $this->assertEquals($customer_id, $order[0]['customer_id']);
        $this->assertEquals($date, $order[0]['customer_order_date']);
        $this->assertEquals($status, $order[0]['customer_order_status_id']);
        $this->assertEquals($tax, $order[0]['customer_order_tax']);
        $this->assertEquals($discount, $order[0]['customer_order_discount']);
        $this->assertEquals($comments, $order[0]['customer_order_comments']);
        $this->assertEquals($shipping, $order[0]['customer_order_shipping']);
        
        for ($i = 0; $i < count($order); $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Saving Customer Order Item
     *
     * @access public
     */
    public function testSaveCustomerOrderItem()
    {
        $order_id = 2;
        $product_id = 1;
        $qty = 10;
        $price = 2.50;
        $discount = 0.00;
        $status = 1;
        
        $id = $this->admin_model->SaveCustomerOrderItem($order_id, $product_id, $qty, $price, $discount, FALSE, $status);
        
        $this->assertNotNull($id);
        $this->assertTrue($id == 5, 'Fifth order item');
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Customer Order and Adding a
     * Product to the Order.
     *
     * @access public
     */
    public function testUpdateCustomerOrderAddProduct()
    {
        $order_id = 1;
        $customer_id = 1;
        $date = '2012-09-09 07:27:12';
        $status = 1;
        $products = array(1, 2, 1);
        $qty = array(10, 20, 10);
        $prices = array(2.50, 1.99, 2.50);
        $discounts = array(0, 0, 5.00);
        $tax = 1.50;
        $discount = 0.00;
        $removed = FALSE;
        $comments = 'No discount for this order';
        $shipping = 10.00;
        
        $this->admin_model->UpdateCustomerOrder($order_id, $customer_id, $date, $status, $products, $qty,
                                                $prices, $discounts, $removed, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order, 'Order should not be null');
        
        $this->assertEquals($customer_id, $order[0]['customer_id']);
        $this->assertEquals($date, $order[0]['customer_order_date']);
        $this->assertEquals($status, $order[0]['customer_order_status_id']);
        $this->assertEquals($tax, $order[0]['customer_order_tax']);
        $this->assertEquals($discount, $order[0]['customer_order_discount']);
        $this->assertEquals($comments, $order[0]['customer_order_comments']);
        $this->assertEquals($shipping, $order[0]['customer_order_shipping']);
        
        for ($i = 0; $i < count($order); $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Customer Order and Removing a
     * Product from the Order.
     *
     * @access public
     */
    public function testUpdateCustomerOrderRemoveProduct()
    {
        $order_id = 1;
        $customer_id = 1;
        $date = '2012-09-09 07:27:12';
        $status = 1;
        $products = array(1);
        $qty = array(10);
        $prices = array(2.50);
        $discounts = array(0);
        $tax = 1.50;
        $discount = 0.00;
        $removed = FALSE;
        $comments = 'No discount for this order';
        $shipping = 10.00;
        
        $this->admin_model->UpdateCustomerOrder($order_id, $customer_id, $date, $status, $products, $qty,
                                                $prices, $discounts, $removed, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order, 'Order should not be null');
        
        $this->assertEquals($customer_id, $order[0]['customer_id']);
        $this->assertEquals($date, $order[0]['customer_order_date']);
        $this->assertEquals($status, $order[0]['customer_order_status_id']);
        $this->assertEquals($tax, $order[0]['customer_order_tax']);
        $this->assertEquals($discount, $order[0]['customer_order_discount']);
        $this->assertEquals($comments, $order[0]['customer_order_comments']);
        $this->assertEquals($shipping, $order[0]['customer_order_shipping']);
        
        for ($i = 0; $i < count($order); $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Update Customer Order to products being In-Transit
     * and therefore Product Qty shuld decrease.
     *
     * @access public
     */
    public function testUpdateCustomerOrderToInTransit()
    {
        $order_id = 1;
        $customer_id = 1;
        $date = '2012-09-09 07:27:12';
        $status = 3;
        $products = array(1, 2);
        $qty = array(10, 20);
        $prices = array(2.50, 1.99);
        $discounts = array(0, 0);
        $tax = 1.50;
        $discount = 0.00;
        $removed = FALSE;
        $comments = 'No discount for this order';
        $shipping = 10.00;
        
        $this->admin_model->UpdateCustomerOrder($order_id, $customer_id, $date, $status, $products, $qty,
                                                $prices, $discounts, $removed, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order, 'Order should not be null');
        
        $p_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $products = $p_model->GetAllProducts();
        
        $this->assertTrue(count($products) == 2);
        
        for ($i = 0; $i < count($products); $i++)
        {
            $this->assertEquals($qty[$i] * -1, $products[$i]['qty']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Customer Order by Adding and Removing a
     * Product from the Order.
     *
     * @access public
     */
    public function testUpdateCustomerOrderAddAndRemoveProduct()
    {
        $order_id = 1;
        $customer_id = 1;
        $date = '2012-09-09 07:27:12';
        $status = 1;
        $products = array(1, 1);
        $qty = array(10, 10);
        $prices = array(2.50, 2.50);
        $discounts = array(0, 5.00);
        $tax = 1.50;
        $discount = 0.00;
        $removed = FALSE;
        $comments = 'No discount for this order';
        $shipping = 10.00;
        
        $this->admin_model->UpdateCustomerOrder($order_id, $customer_id, $date, $status, $products, $qty,
                                                $prices, $discounts, $removed, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order, 'Order should not be null');
        
        $this->assertEquals($customer_id, $order[0]['customer_id']);
        $this->assertEquals($date, $order[0]['customer_order_date']);
        $this->assertEquals($status, $order[0]['customer_order_status_id']);
        $this->assertEquals($tax, $order[0]['customer_order_tax']);
        $this->assertEquals($discount, $order[0]['customer_order_discount']);
        $this->assertEquals($comments, $order[0]['customer_order_comments']);
        $this->assertEquals($shipping, $order[0]['customer_order_shipping']);
        
        for ($i = 0; $i < count($order); $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting Customer Order Item
     *
     * @access public
     */
    public function testDeleteCustomerOrderItem()
    {
        $order_id = 1;
        $order_item_id = 1;
        
        $items = $this->model->GetCustomerOrderProductsByCustomerOrderId($order_id);
        
        $this->assertTrue(count($items) == 2, '2 Items before Deletion');
        
        $this->admin_model->DeleteCustomerOrderItem($order_item_id);
        
        $items = $this->model->GetCustomerOrderProductsByCustomerOrderId($order_id);
        
        $this->assertNotNull($items);
        $this->assertNotEmpty($items);
        
        $this->assertTrue(count($items) == 1);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting Customer Order that is NOT yet
     * dispatched.
     *
     * Deletion should SUCCEED
     *
     * @access public
     */
    public function testDeletingCustomerOrderNotInTransit()
    {
        $order_id = 1;
        $orders = $this->model->GetAllCustomerOrders();
        
        $this->assertTrue(count($orders) == 2);
        
        $this->admin_model->DeleteCustomerOrder($order_id);
        $orders = $this->model->GetAllCustomerOrders();
        $this->assertTrue(count($orders) == 1);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting Customet Order that HAS been
     * dispatched.
     *
     * Deletion should FAIL
     *
     * @access public
     */
    public function testDeletingCustomerOrderInTransit()
    {
        $order_id = 1;
        $customer_id = 1;
        $date = '2012-09-09 07:27:12';
        $status = 3;
        $products = array(1, 2);
        $qty = array(10, 20);
        $prices = array(2.50, 1.99);
        $discounts = array(0, 0);
        $tax = 1.50;
        $discount = 0.00;
        $removed = FALSE;
        $comments = 'No discount for this order';
        $shipping = 10.00;
        
        // update Order to be In-Transit
        $this->admin_model->UpdateCustomerOrder($order_id, $customer_id, $date, $status, $products, $qty,
                                                $prices, $discounts, $removed, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order, 'Order should not be null');
        
        $this->assertEquals($status, $order[0]['customer_order_status_id']);
        
        // This deletion should fail
        $this->admin_model->DeleteCustomerOrder($order_id);
        
        // Order should not be null
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order, 'Order should not be null');
        $this->assertTrue(count($order) == 2);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Closing Customer Order
     *
     * @access public
     */
    public function testCloseAndOpenCustomerOrder()
    {
        $product_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $product_1_id = 1;
        $product_2_id = 2;
        
        $order_id = 1;
        $original_status = 1;
        $closed_status = 5;
        
        $original_product_1_qty = 0;
        $original_product_2_qty = 0;
        
        $final_product_1_qty = -5;
        $final_product_2_qty = -4;
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $this->assertNotNull($order);
        
        $products = array();
        $qty = null;
        $prices = null;
        $discounts = null;
        
        for ($i = 0; $i < count($order); $i++)
        {
            $products[]     = $order[$i]['product_id'];
            $qty[]          = $order[$i]['quantity'];
            $prices[]       = $order[$i]['item_price'];
            $discounts[]    = $order[$i]['discount'];
        }
        
        $this->assertStrictTrue(count($products) == 2);
        $this->assertStrictTrue(count($qty) == 2);
        $this->assertStrictTrue(count($prices) == 2);
        $this->assertStrictTrue(count($discounts) == 2);
        
        $product_1 = $product_model->GetProductById($product_1_id);
        $product_2 = $product_model->GetProductById($product_2_id);
        
        $this->assertNotNull($product_1);
        $this->assertNotNull($product_2);
        
        $this->assertEquals($original_status, $order[0]['customer_order_status_id']);
        $this->assertEquals($original_product_1_qty, $product_1['qty']);
        $this->assertEquals($original_product_2_qty, $product_2['qty']);
        $this->assertFalse($order[0]['qty_removed']);
        
        // UPDATE - Closed
        $result = $this->admin_model->UpdateCustomerOrder(
            $order_id, $order[0]['customer_id'], $order[0]['customer_order_date'],
            $closed_status, $products, $qty, $prices, $discounts, $order[0]['qty_removed'], $order[0]['customer_order_comments'],
            $order[0]['customer_order_discount'], $order[0]['customer_order_tax'], $order[0]['customer_order_shipping']);
        
        // TEST UPDATE
        $this->assertStrictTrue($result);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $product_1 = $product_model->GetProductById($product_1_id);
        $product_2 = $product_model->GetProductById($product_2_id);
        
        $this->assertEquals($closed_status, $order[0]['customer_order_status_id']);
        $this->assertEquals($final_product_1_qty, $product_1['qty']);
        $this->assertEquals($final_product_2_qty, $product_2['qty']);
        $this->assertTrue($order[0]['qty_removed']);
        
        // UPDATE - NOT Closed anymore - back to original
        $result = $this->admin_model->UpdateCustomerOrder(
            $order_id, $order[0]['customer_id'], $order[0]['customer_order_date'],
            $original_status, $products, $qty, $prices, $discounts, $order[0]['qty_removed'], $order[0]['customer_order_comments'],
            $order[0]['customer_order_discount'], $order[0]['customer_order_tax'], $order[0]['customer_order_shipping']);
        
        $this->assertStrictTrue($result);
        
        $order = $this->model->GetCustomerOrderById($order_id);
        
        $product_1 = $product_model->GetProductById($product_1_id);
        $product_2 = $product_model->GetProductById($product_2_id);
        
        $this->assertEquals($original_status, $order[0]['customer_order_status_id']);
        $this->assertEquals($original_product_1_qty, $product_1['qty']);
        $this->assertEquals($original_product_2_qty, $product_2['qty']);
        $this->assertFalse($order[0]['qty_removed']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}