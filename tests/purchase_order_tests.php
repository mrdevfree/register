<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Purchase Order Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterPurchaseOrderTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     *
     * Initialises Admin Model
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $this->admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Orders');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Test getting all Purchase Orders
     *
     * @access public
     */
    public function testGetAllPurchaseOrders()
    {
        $orders = $this->model->GetAllPurchaseOrders();
        
        $this->assertTrue(2 == count($orders), "Should return 2 purchase order");
    }
    
    /**
     * Test getting Purchase Order by ID
     *
     * @access public
     */
    public function testGetPurchaseOrderById()
    {
        $order_id = 1;
        $supplier_id = 1;
        
        $order = $this->model->GetPurchaseOrderById($order_id);
        
        $this->assertEquals($supplier_id, $order[0]['supplier_id']);
    }
    
    /**
     * Tests Getting Purchase Order Count for Products
     *
     * @access public
     */
    public function testGetPurchaseOrderCountForProduct()
    {
        $product_id = 1;
        $product_1_count = 1;
        
        $count = $this->model->GetPurchaseOrderCountForProduct($product_id);
        
        $this->assertEquals($product_1_count, $count);
        
        $product_id = 10;
        $product_10_count = 0;
        
        $count = $this->model->GetPurchaseOrderCountForProduct($product_id);
        $this->assertEquals($product_10_count, $count);
    }
    
    /**
     * Test Getting Purchase Order Products
     *
     * @access public
     */
    public function testGetPurchaseOrderProductsByPurchaseOrderId()
    {
        $purchase_order_id = 1;
        
        $products = $this->model->GetPurchaseOrderProductsByPurchaseOrderId($purchase_order_id);
        
        $this->assertNotEmpty($products, 'Array should have 2 products');
        $this->assertTrue(count($products) == 2, 'Count should be 2');
    }
    
    /**
     * Test Adding new Purchase Order
     *
     * @access public
     */
    public function testAddPurchaseOrder()
    {
        $supplier = 2;
        $date = $GLOBALS['app']->UserTime2UTC(time(), 'Y-m-d H:i:s');
        $ref = '546-256-235';
        $status = 1; // Open
        $products = array(1, 2);
        $qty = array(2, 5);
        $prices = array(0.80, 0.30);
        $discounts = array(0, 0);
        $comments = 'This is my test comment';
        $discount = 0;
        $tax = 2.20;
        $shipping = 0;
        
        $id = $this->admin_model->SaveNewPurchaseOrder($supplier, $date, $ref, $status, $products, $qty,
                                  $prices, $discounts, $comments, $discount, $tax, $shipping);
        
        $this->assertTrue($id != null && $id != '');
        
        $order = $this->model->GetPurchaseOrderById($id);
        
        if ($order == NULL)
        {
            $this->fail('Order is NULL');
        }
        
        $this->assertEquals($supplier, $order[0]['supplier_id']);
        $this->assertEquals($date, $order[0]['purchase_order_date']);
        $this->assertEquals($ref, $order[0]['reference_no']);
        $this->assertEquals($status, $order[0]['purchase_order_status_id']);
        $this->assertEquals('Open', $order[0]['status_name']);
        
        $this->resetDatabase();
    }
    
    /**
     * Tests Updating Purchase Order and Adding a Product
     *
     * @access public
     */
    public function testUpdatePurchaseOrderAndAddProduct()
    {
        $purchase_order_id = 1;
        $supplier = 1;
        $date = '2012-09-09 07:27:12';
        $ref = '1256-2568-2567';
        $status = 3; // Filled
        $products = array(1, 2, 1);
        $qty = array(150, 1, 5);
        $prices = array(0.80, 0.80, 0.80);
        $discounts = array(30, 0, 0);
        $added = FALSE;
        $comments = 'This is my test comment';
        $discount = 0;
        $tax = 2.20;
        $shipping = 0;
        
        $this->admin_model->UpdatePurchaseOrder(
            $purchase_order_id, $supplier, $date, $ref, $status, $products, $qty, $prices,
            $discounts, $added, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetPurchaseOrderById($purchase_order_id);
        
        $this->assertTrue($order != NULL);
        
        $this->assertEquals($date, $order[0]['purchase_order_date']);
        $this->assertEquals($ref, $order[0]['reference_no']);
        $this->assertEquals($status, $order[0]['purchase_order_status_id']);
        $this->assertEquals($tax, $order[0]['purchase_order_tax']);
        $this->assertEquals($discount, $order[0]['purchase_order_discount']);
        $this->assertEquals($shipping, $order[0]['purchase_order_shipping']);
        $this->assertEquals($comments, $order[0]['purchase_order_comments']);
        $this->assertEquals('Filled', $order[0]['status_name']);
        
        $count = count($order);
        
        $this->assertEquals(3, $count);
        
        for ($i = 0; $i < $count; $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Purchase Order and Removing a Product
     * from the Purchase Order
     *
     * @access public
     */
    public function testUpdatePurchaseOrderAndRemoveProduct()
    {
        $purchase_order_id = 1;
        $supplier = 1;
        $date = '2012-09-09 07:27:12';
        $ref = '1256-2568-2567';
        $status = 3; // Filled
        $products = array(1);
        $qty = array(10);
        $prices = array(0.80);
        $discounts = array(30);
        $added = FALSE;
        $comments = 'This is my test comment';
        $discount = 0;
        $tax = 2.20;
        $shipping = 0;
        
        $this->admin_model->UpdatePurchaseOrder(
            $purchase_order_id, $supplier, $date, $ref, $status, $products, $qty, $prices,
            $discounts, $added, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetPurchaseOrderById($purchase_order_id);
        
        $this->assertTrue($order != NULL);
        
        $this->assertEquals($date, $order[0]['purchase_order_date']);
        $this->assertEquals($ref, $order[0]['reference_no']);
        $this->assertEquals($status, $order[0]['purchase_order_status_id']);
        $this->assertEquals($tax, $order[0]['purchase_order_tax']);
        $this->assertEquals($discount, $order[0]['purchase_order_discount']);
        $this->assertEquals($shipping, $order[0]['purchase_order_shipping']);
        $this->assertEquals($comments, $order[0]['purchase_order_comments']);
        $this->assertEquals('Filled', $order[0]['status_name']);
        
        $count = count($order);
        
        $this->assertEquals(1, $count);
        
        for ($i = 0; $i < $count; $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Purchase Order and Adding 1 Product
     * and Removing 1 Product
     *
     * @access public
     */
    public function testUpdatePurchaseOrderAddAndRemoveProduct()
    {
        $purchase_order_id = 1;
        $supplier = 1;
        $date = '2012-09-09 07:27:12';
        $ref = '1256-2568-2567';
        $status = 3; // Filled
        $products = array(1, 1);
        $qty = array(150, 5);
        $prices = array(0.80, 0.80);
        $discounts = array(30, 0);
        $added = FALSE;
        $comments = 'This is my test comment';
        $discount = 0;
        $tax = 2.20;
        $shipping = 0;
        
        $this->admin_model->UpdatePurchaseOrder(
            $purchase_order_id, $supplier, $date, $ref, $status, $products, $qty, $prices,
            $discounts, $added, $comments, $discount, $tax, $shipping);
        
        $order = $this->model->GetPurchaseOrderById($purchase_order_id);
        
        $this->assertTrue($order != NULL);
        
        $this->assertEquals($date, $order[0]['purchase_order_date']);
        $this->assertEquals($ref, $order[0]['reference_no']);
        $this->assertEquals($status, $order[0]['purchase_order_status_id']);
        $this->assertEquals($tax, $order[0]['purchase_order_tax']);
        $this->assertEquals($discount, $order[0]['purchase_order_discount']);
        $this->assertEquals($shipping, $order[0]['purchase_order_shipping']);
        $this->assertEquals($comments, $order[0]['purchase_order_comments']);
        $this->assertEquals('Filled', $order[0]['status_name']);
        
        $count = count($order);
        
        $this->assertEquals(2, $count);
        
        for ($i = 0; $i < $count; $i++)
        {
            $this->assertEquals($products[$i], $order[$i]['product_id']);
            $this->assertEquals($qty[$i], $order[$i]['quantity']);
            $this->assertEquals($prices[$i], $order[$i]['item_price']);
            $this->assertEquals($discounts[$i], $order[$i]['discount']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Adding a Purchase Order Item
     *
     * @access public
     */
    public function testAddPurchaseOrderItem()
    {
        $purchase_id = 3;
        $product_id = 2;
        $qty = 10;
        $price = 2.50;
        $discount = 0.50;
        $status = 2;
        
        $id = $this->admin_model->SavePurchaseOrderItem(
            $purchase_id, $product_id, $qty, $price, $discount, FALSE, $status);
        
        $products = $this->model->GetPurchaseOrderProductsByPurchaseOrderId($purchase_id);
        
        $this->assertEquals($purchase_id, $products[0]['purchase_order_id']);
        $this->assertEquals($product_id, $products[0]['product_id']);
        $this->assertEquals($qty, $products[0]['quantity']);
        $this->assertEquals($price, $products[0]['item_price']);
        $this->assertEquals($discount, $products[0]['discount']);
        
        // cleanuup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting a Purchase Order Item
     *
     * @access public
     */
    public function testDeletePurchaseOrderItem()
    {
        $purchase_id = 1;
        $product_id = 1;
        $items_id = 2;
        
        $id = $this->admin_model->DeletePurchaseOrderItem($items_id);
        
        $products = $this->model->GetPurchaseOrderProductsByPurchaseOrderId($purchase_id);
        
        $this->assertEquals(1, count($products));
        $this->assertEquals(1, $products[0]['product_id']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting Purchase Order
     *
     * @access public
     */
    public function testDeletePurchaseOrder()
    {
        $purchase_order_id = 1;
        
        $this->admin_model->DeletePurchaseOrder($purchase_order_id);
        
        $orders = $this->model->GetAllPurchaseOrders();
        
        $this->assertEquals(1, count($orders));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Closing Purchase Order
     *
     * @access public
     */
    public function testCloseAndOpenPurchaseOrder()
    {
        $product_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $product_1_id = 1;
        $product_2_id = 2;
        
        $order_id = 1;
        $original_status = 1;
        $closed_status = 5;
        
        $original_product_1_qty = 0;
        $original_product_2_qty = 0;
        
        $final_product_1_qty = 150;
        $final_product_2_qty = 250;
        
        $order = $this->model->GetPurchaseOrderById($order_id);
        
        $this->assertNotNull($order);
        
        $products = array();
        $qty = null;
        $prices = null;
        $discounts = null;
        
        for ($i = 0; $i < count($order); $i++)
        {
            $products[]     = $order[$i]['product_id'];
            $qty[]          = $order[$i]['quantity'];
            $prices[]       = $order[$i]['item_price'];
            $discounts[]    = $order[$i]['discount'];
        }
        
        $this->assertStrictTrue(count($products) == 2);
        $this->assertStrictTrue(count($qty) == 2);
        $this->assertStrictTrue(count($prices) == 2);
        $this->assertStrictTrue(count($discounts) == 2);
        
        $product_1 = $product_model->GetProductById($product_1_id);
        $product_2 = $product_model->GetProductById($product_2_id);
        
        $this->assertNotNull($product_1);
        $this->assertNotNull($product_2);
        
        $this->assertEquals($original_status, $order[0]['purchase_order_status_id']);
        $this->assertEquals($original_product_1_qty, $product_1['qty']);
        $this->assertEquals($original_product_2_qty, $product_2['qty']);
        $this->assertFalse($order[0]['qty_added']);
        
        // UPDATE - Closed
        $result = $this->admin_model->UpdatePurchaseOrder(
            $order_id, $order[0]['supplier_id'], $order[0]['purchase_order_date'], $order[0]['reference_no'],
            $closed_status, $products, $qty, $prices, $discounts, $order[0]['qty_added'], $order[0]['purchase_order_comments'],
            $order[0]['purchase_order_discount'], $order[0]['purchase_order_tax'], $order[0]['purchase_order_shipping']);
        
        // TEST UPDATE
        $this->assertStrictTrue($result);
        
        $order = $this->model->GetPurchaseOrderById($order_id);
        
        $product_1 = $product_model->GetProductById($product_1_id);
        $product_2 = $product_model->GetProductById($product_2_id);
        
        $this->assertEquals($closed_status, $order[0]['purchase_order_status_id']);
        $this->assertEquals($final_product_1_qty, $product_1['qty']);
        $this->assertEquals($final_product_2_qty, $product_2['qty']);
        $this->assertTrue($order[0]['qty_added']);
        
        // UPDATE - NOT Closed anymore - back to original
        $result = $this->admin_model->UpdatePurchaseOrder(
            $order_id, $order[0]['supplier_id'], $order[0]['purchase_order_date'], $order[0]['reference_no'],
            $original_status, $products, $qty, $prices, $discounts, $order[0]['qty_added'], $order[0]['purchase_order_comments'],
            $order[0]['purchase_order_discount'], $order[0]['purchase_order_tax'], $order[0]['purchase_order_shipping']);
        
        $this->assertStrictTrue($result);
        
        $order = $this->model->GetPurchaseOrderById($order_id);
        
        $product_1 = $product_model->GetProductById($product_1_id);
        $product_2 = $product_model->GetProductById($product_2_id);
        
        $this->assertEquals($original_status, $order[0]['purchase_order_status_id']);
        $this->assertEquals($original_product_1_qty, $product_1['qty']);
        $this->assertEquals($original_product_2_qty, $product_2['qty']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}