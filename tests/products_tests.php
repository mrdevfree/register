<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Products Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterProductsTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     *
     * Initialises all models
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        $this->admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Products');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Test getting all products
     *
     * @access public
     */
    public function testGetAllProducts()
    {
        $products = $this->model->GetAllProducts();
        
        $this->assertTrue(2 == count($products), "Should return 2 products");
    }
    
    public function testGetAllProductsWithTypes()
    {
        $type = 'Physical';
        
        $products = $this->model->GetAllProducts();
        
        foreach ($products as $product)
        {
            $this->assertEquals($type, $product['product_type_name']);
        }
    }
    
    /**
     * Test getting Product Count by Supplier
     *
     * @access public
     */
    public function testGetProductCountBySupplier()
    {
        $supplier_id = 1;
        $count = $this->model->GetProductCountBySupplier($supplier_id);
        
        $this->assertEquals(2, $count);
    }
    
    /**
     * Test getting product count in category
     *
     * @access public
     */
    public function testGetProductCountByCategory()
    {
        $category_id = 2;
        $count = $this->model->GetProductCountByCategory($category_id);
        
        $this->assertEquals(2, $count);
    }
    
    /**
     * Test getting all Product Types
     *
     * @access public
     */
    public function testGetAllProductTypes()
    {
        $def = array('Physical', 'Electronic');
        
        $types = $this->model->GetAllProductTypes();
        
        for ($i = 0; $i < count($def); $i++)
        {
            $this->assertEquals($def[$i], $types[$i]['product_type_name']);
        }
    }
    
    /**
     * Test getting Product by ID
     *
     * @access public
     */
    public function testGetProductById()
    {
        $product_id = 1;
        $product_name = 'mTraveller - WP7';
        
        $product = $this->model->GetProductById($product_id);
        
        $this->assertEquals($product_name, $product['product_name']);
    }
    
    /**
     * Test adding new Product
     *
     * @access public
     */
    public function testAddNewProduct()
    {
        $name = 'test product';
        $catID = 1;
        $suppID = 1;
        $visible = TRUE;
        $short = 'this is a short description';
        $url = 'http://crazydev.org/products/test product';
        $wPrice = '25.95';
        $rPrice = '30.95';
        $type = 1;
        $long = 'this is a long description of the product';
        
        $this->admin_model->SaveNewProduct($name, $catID, $suppID, $visible, $short,
                                     $url = '', $wPrice = '', $rPrice = '', $type, $long = '');
        
        $id = $GLOBALS['db']->LastInsertId('register_products');
        
        $this->assertTrue($id > 0);
        
        $product = $this->model->GetProductById($id);
        
        $this->assertEquals($name, $product['product_name']);
        $this->assertEquals($catID, $product['category_id']);
        $this->assertEquals($suppID, $product['supplier_id']);
        $this->assertEquals($visible, $product['product_visible']);
        $this->assertEquals($short, $product['short_description']);
        $this->assertEquals($url, $product['url']);
        $this->assertEquals($wPrice, $product['price_wholesale']);
        $this->assertEquals($rPrice, $product['price_retail']);
        $this->assertEquals($type, $product['product_type']);
        $this->assertEquals($long, $product['long_description']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Updating Product
     *
     * @access public
     */
    public function testUpdateProduct()
    {
        $product_id = 1;
        $visible = 0;
        $retail_price = '1.99';
        $wholesale_price = '0.60';
        $short = '';
        $long = '';
        $url = '';
        $type = 2;
        $typeName = 'Electronic';
        
        $product = $this->model->GetProductById($product_id);
        
        $this->assertTrue($product != NULL);
        
        $this->admin_model->UpdateProduct($product_id, $product['product_name'], $product['category_id'],
                                    $product['supplier_id'], $visible, $short, $url, $wholesale_price,
                                    $retail_price, $type, $long);
        
        $product = $this->model->GetProductById($product_id);
        
        $this->assertEquals($visible, $product['product_visible']);
        $this->assertEquals($retail_price, $product['price_retail']);
        $this->assertEquals($wholesale_price, $product['price_wholesale']);
        $this->assertEquals($short, $product['short_description']);
        $this->assertEquals($long, $product['long_description']);
        $this->assertEquals($type, $product['product_type']);
        $this->assertEquals($typeName, $product['product_type_name']);
        $this->assertEquals($url, $product['url']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Product Qty
     *
     * @access public
     */
    public function testUpdateProductQty()
    {
        $product_id = 1;
        $currentQuantity = 0;
        $addQuantity = 5;
        
        $product = $this->model->GetProductById($product_id);
        
        $this->assertTrue($currentQuantity == $product['qty']);
        
        $this->admin_model->UpdateProductQty($product_id, $addQuantity, TRUE);
        
        $product = $this->model->GetProductById($product_id);
        
        $this->assertTrue($addQuantity == $product['qty'], 'Quantity should be 5');
        
        $this->admin_model->UpdateProductQty($product_id, $addQuantity, FALSE);
        
        $product = $this->model->GetProductById($product_id);
        
        $this->assertTrue(0 == $product['qty'], 'Quantity should be 5');
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests if its possible to delete a purchase order with
     * existing purchase order
     *
     * @access public
     */
    public function testDeleteProductWithExistingPurchaseOrder()
    {
        $product_id = 1;
        
        $product = $this->model->GetProductById($product_id);
        $this->assertEquals($product_id, $product['product_id']);
        
        $this->admin_model->DeleteProduct($product_id);
        
        $response = $GLOBALS['app']->Session->PopLastResponse();
        $this->assertEquals(_t('REGISTER_ERROR_PRODUCT_HAS_ORDERS'), $response[0]['message']);
        
        $product = $this->model->GetProductById($product_id);
        $this->assertNotNull($product);
        $this->assertEquals($product_id, $product['product_id']);
    }
    
    /**
     * Tests getting all Product Images
     *
     * @access public
     */
    public function testGetAllProductImagesByID()
    {
        $product_id = 1;
        
        $images = $this->model->GetAllProductImagesByID($product_id);
        
        $this->assertNotNull($images);
        $this->assertEquals(1, count($images));
    }
    
    /**
     * Tests getting Image Count for Product
     *
     * @access public
     */
    public function testGetProductImageCount()
    {
        $product_id = 1;
        
        $count = $this->model->GetProductImageCount($product_id);
        
        $this->assertNotNull($count);
        $this->assertEquals(1, $count);
    }
    
    /**
     * Test adding new product image to
     * database
     *
     * @access public
     */
    public function testAddNewProductImage()
    {
        $product_id = 1;
        $filename = 'example.1.2.jpg';
        
        $id = $this->admin_model->AddnewProductImage($product_id, $filename);
        
        $this->assertNotNull($id);
        $this->assertEquals(2, $id);
    }
    
    public function testGetProductImageByID()
    {
        $image_id = 1;
        $name = 'product.1.1.jpg';
        
        $image = $this->model->GetProductImageByID($image_id);
        
        $this->assertNotNull($image);
        $this->assertEquals($name, $image['image_url']);
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}