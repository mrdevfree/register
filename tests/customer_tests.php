<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Customer Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterCustomerTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     *
     * Initialises Admin Model
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Customers');
        $this->admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Customers');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Test getting all Customers
     */
    public function testGetAllCustomers()
    {
        $customers = $this->model->GetAllCustomers();
        
        $this->assertTrue(3 == count($customers), "Should return 2 customers");
    }
    
    /**
     * Test getting Customer by ID
     */
    public function testGetCustomerById()
    {
        $customer_id = 2;
        $first_name = 'Suzie';
        $last_name = 'Jones';
        
        $customer = $this->model->GetCustomerById($customer_id);
        
        $this->assertEquals($first_name, $customer['name_first']);
        $this->assertEquals($last_name, $customer['name_last']);
    }
    
    /**
     * Tests Adding New Customer
     *
     * @access public
     */
    public function testAddNewCustomer()
    {
        $f_name = 'Tim';
        $l_name = 'Somethiing';
        $company = 'Test New Name Company';
        $abn = '125-126-526';
        $phone = '03 9563 0230';
        $fax = '03 9563 5987';
        $mobile = '0411 512 512';
        $email = 't.something@site.com';
        $website = 'site.com.au';
        $street1 = '513 Main St';
        $street2 = 'Great Views Offices';
        $city = 'South Auckland';
        $state = 'None';
        $pcode = '2158';
        $country = 'New Zealands';
        $visible = TRUE;
        
        $id = $this->admin_model->SaveNewCustomer(
            $f_name, $l_name, $company, $abn, $phone, $fax, $mobile, $email, $website,
            $street1, $street2, $city, $state, $pcode, $country, $visible);
        
        $this->assertNotNull($id);
        $this->assertTrue($id > 2);
        
        $customer = $this->model->GetCustomerById($id);
        
        $this->assertEquals($f_name, $customer['name_first']);
        $this->assertEquals($l_name, $customer['name_last']);
        $this->assertEquals($company, $customer['company_name']);
        $this->assertEquals($abn, $customer['abn']);
        $this->assertEquals($phone, $customer['phone']);
        $this->assertEquals($fax, $customer['fax']);
        $this->assertEquals($mobile, $customer['mobile']);
        $this->assertEquals($email, $customer['email']);
        $this->assertEquals($website, $customer['website']);
        $this->assertEquals($street1, $customer['street1']);
        $this->assertEquals($street2, $customer['street2']);
        $this->assertEquals($city, $customer['city']);
        $this->assertEquals($state, $customer['state']);
        $this->assertEquals($pcode, $customer['pcode']);
        $this->assertEquals($country, $customer['country']);
        $this->assertEquals($visible, $customer['customer_visible']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Customer
     *
     * @access public
     */
    public function testUpdateCustomer()
    {
        $customer_id = 1;
        $f_name = 'Tim';
        $l_name = 'Somethiing';
        $company = 'Test New Name Company';
        $abn = '125-126-526';
        $phone = '03 9563 0230';
        $fax = '03 9563 5987';
        $mobile = '0411 512 512';
        $email = 't.something@site.com';
        $website = 'site.com.au';
        $street1 = '513 Main St';
        $street2 = 'Great Views Offices';
        $city = 'South Auckland';
        $state = 'None';
        $pcode = '2158';
        $country = 'New Zealands';
        $visible = TRUE;
        
        $this->admin_model->UpdateCustomer(
                $customer_id, $f_name, $l_name, $company, $abn, $phone, $fax,
                $mobile, $email, $website, $street1, $street2, $city, $state, $pcode,
                $country, $visible);
        
        $customer = $this->model->GetCustomerById($customer_id);
        
        $this->assertEquals($f_name, $customer['name_first']);
        $this->assertEquals($l_name, $customer['name_last']);
        $this->assertEquals($company, $customer['company_name']);
        $this->assertEquals($abn, $customer['abn']);
        $this->assertEquals($phone, $customer['phone']);
        $this->assertEquals($fax, $customer['fax']);
        $this->assertEquals($mobile, $customer['mobile']);
        $this->assertEquals($email, $customer['email']);
        $this->assertEquals($website, $customer['website']);
        $this->assertEquals($street1, $customer['street1']);
        $this->assertEquals($street2, $customer['street2']);
        $this->assertEquals($city, $customer['city']);
        $this->assertEquals($state, $customer['state']);
        $this->assertEquals($pcode, $customer['pcode']);
        $this->assertEquals($country, $customer['country']);
        $this->assertEquals($visible, $customer['customer_visible']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting a Customer without any existing
     * orders.
     *
     * Deletion will be successful
     * 
     * @access public
     */
    public function testDeleteCustomerWithoutOrders()
    {
        $customer_id = 3;
        $f_names = array('John', 'Suzie');
        $l_names = array('Smith', 'Jones');
        
        $customers = $this->model->GetAllCustomers();
        $this->assertTrue(count($customers) == 3);
        
        $this->admin_model->DeleteCustomer($customer_id);
        
        $customers = $this->model->GetAllCustomers();
        
        $this->assertTrue(count($customers) == 2);
        
        for ($i = 0; $i < count($customers); $i++)
        {
            $this->assertEquals($f_names[$i], $customers[$i]['name_first']);
            $this->assertEquals($l_names[$i], $customers[$i]['name_last']);
        }
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting a Customer WITH existing
     * orders.
     *
     * Deletion will FAIL.
     *
     * @access public
     */
    public function testDeleteCustomerWithOrders()
    {
        $customer_id = 1;
        $f_name = 'John';
        $l_name = 'Smith';
        
        $customers = $this->model->GetAllCustomers();
        $this->assertTrue(count($customers) == 3);
        
        $this->admin_model->DeleteCustomer($customer_id);
        
        $customers = $this->model->GetAllCustomers();
        $this->assertTrue(count($customers) == 3);
        
        $this->assertEquals($f_name, $customers[0]['name_first']);
        $this->assertEquals($l_name, $customers[0]['name_last']);
    }
    
    /**
     * Test Updating Customer Shipping Address
     *
     * @access public
     */
    public function testUpdateShippingAddress()
    {
        $customer_id = 1;
        $street1 = '513 Main St';
        $street2 = '';
        $city = 'South Auckland';
        $state = 'None';
        $pcode = '2158';
        $country = 'New Zealands';
        
        $customer = $this->model->GetCustomerById($customer_id);
        
        $this->assertEquals('23 Elizableth St', $customer['street1']);
        
        $this->admin_model->UpdateShippingAddress($customer_id, FALSE, $street1, $street2, $city, $state, $pcode, $country);
        
        $customer = $this->model->GetCustomerById($customer_id);
        
        $this->assertEquals($street1, $customer['ship_street1']);
        $this->assertEquals($street2, $customer['ship_street2']);
        $this->assertEquals($city, $customer['ship_city']);
        $this->assertEquals($state, $customer['ship_state']);
        $this->assertEquals($pcode, $customer['ship_pcode']);
        $this->assertEquals($country, $customer['ship_country']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Customer with Ship_Same set has no shipping address
     * defined
     *
     * @access public
     */
    public function testCustomerUserIdOfZeroSameShippingAddress()
    {
        $customer_id = 1;
        $street = '102 Jobs Rd';
        
        $customer = $this->model->GetCustomerById($customer_id);
        
        $this->assertFalse($customer['ship_same']);
        $this->assertEquals($street, $customer['ship_street1']);
        
        $customer_id = 2;
        $street = NULL;
        
        $customer = $this->model->GetCustomerById($customer_id);
        
        $this->assertTrue($customer['ship_same']);
        $this->assertNull($street, $customer['ship_street1']);
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}