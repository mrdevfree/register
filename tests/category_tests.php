<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Category Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterCategoryTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Categories');
        $this->admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Categories');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Tests Getting all Categories
     *
     * @access public
     */
    public function testGetAllCategories()
    {
        $categories = $this->model->GetAllCategories();
        
        $this->assertTrue(2 == count($categories), "Should return 2 categories");
    }
    
    /**
     * Tests Getting Category by ID
     *
     * @access public
     */
    public function testGetCategoryById()
    {
        $category_id = 2;
        $category_name = 'Mobile';
        
        $category = $this->model->GetCategoryById($category_id);
        
        $this->assertEquals($category_name, $category['category_name']);
    }
    
    /**
     * Test Adding New Category
     *
     * @access public
     */
    public function testAddNewCategory()
    {
        $name = 'Some New Category';
        $description = 'A short description for category';
        $visible = TRUE;
        
        $id = $this->admin_model->SaveNewCategory($name, $visible, $description);
        
        $this->assertNotNull($id);
        $this->assertTrue($id > 2);
        
        $category = $this->model->GetCategoryById($id);
        
        $this->assertNotNull($category);
        $this->assertEquals($name, $category['category_name']);
        $this->assertEquals($description, $category['description']);
        $this->assertEquals($visible, $category['category_visible']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Updating Category
     *
     * @access public
     */
    public function testUpdateCategory()
    {
        $category_id = 1;
        $name = 'Some New Category';
        $description = 'A short description for category';
        $visible = TRUE;
        
        $this->admin_model->UpdateCategory($category_id, $name, $visible, $description);
        
        $category = $this->model->GetCategoryById($category_id);
        
        $this->assertNotNull($category);
        $this->assertEquals($name, $category['category_name']);
        $this->assertEquals($description, $category['description']);
        $this->assertEquals($visible, $category['category_visible']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting Category without existing
     * products.
     *
     * Deletion should SUCCEED
     *
     * @access public
     */
    public function testDeleteCategoryWithoutProducts()
    {
        $category_id = 1;
        $name = 'Some New Category';
        $description = 'A short description for category';
        $visible = TRUE;
        
        $this->admin_model->DeleteCategory($category_id);
        
        $categories = $this->model->GetAllCategories();
        
        $this->assertTrue(count($categories) == 1);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Test Deleting Category with existing
     * products.
     *
     * Deletion should FAIL
     *
     * @access public
     */
    public function testDeleteCategoryWithProducts()
    {
        $category_id = 2;
        
        $this->admin_model->DeleteCategory($category_id);
        
        $categories = $this->model->GetAllCategories();
        
        $this->assertTrue(count($categories) == 2);
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}