<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';
require_once JAWS_PATH . 'gadgets/Register/Model/Publisher.php';

/**
 * Register Gadget Products Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterPublisherTests extends TestCase
{
    private $_model;
    
    /**
     * Constructor
     *
     * Initialises all models
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = new Register_Publisher();
    }
    
    /**
     * Tests Register_Publisher object is not null
     *
     * @access public
     */
    public function testModelNotNull()
    {
        $this->assertNotNull($this->_model);
    }
    
    /**
     * Tests creating a pdf invoice
     *
     * @access public
     */
    public function testCreateInvoice()
    {
        $order_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Orders');
        $customer_model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Customers');
        
        $name = 'Invoice-1.pdf';
        
        $order = $order_model->getCustomerOrderById(1);
        $customer = $customer_model->getCustomerById($order[0]['customer_id']);
        
        $this->_model->createInvoice($name, $order, $customer);
        
        $exists = file_exists('gadgets/Register/assets/' . $name);
        $this->assertStrictTrue($exists);
        
        $name = 'Invoice-2.pdf';
        
        $order = $order_model->getCustomerOrderById(2);
        $customer = $customer_model->getCustomerById($order[0]['customer_id']);
        
        $this->_model->createInvoice($name, $order, $customer);
        
        // cleanup
        if ($exists)
        {
            //unlink('gadgets/Register/assets/' . $name);
        }
    }
    
}