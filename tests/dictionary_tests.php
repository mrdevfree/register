<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Dictionary Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterDictionaryTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     *
     * Initialises Models
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Dictionary');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Tests that the Dictionary class exists
     *
     * @access public
     */
    public function testClassExists()
    {
        $class = new ReflectionClass('Register_Model_Dictionary');
        $this->assertEquals('Register_Model_Dictionary', $class->getName());
    }
    
    /**
     * Tests Getting a single key from the
     * dictionary
     *
     * @access public
     */
    public function testGetKey()
    {
        $key = 'test_key';
        $value = 'test_value';
        
        $this->assertTrue($this->model->get() instanceof Jaws_Error);
        
        $result = $this->model->get($key);
        
        $this->assertNotEmpty($result);
        
        $this->assertEquals($key, $result['key']);
        $this->assertEquals($value, $result['value']);
    }
    
    /**
     * Tests Getting all Key-Values pairs from
     * the database
     *
     * @access public
     */
    public function testGetAllKeys()
    {
        $result = $this->model->getAll();
        
        $this->assertNotEmpty($result);
        $this->assertEquals(14, count($result));
    }
    
    /**
     * Test Getting Non-Existent Key
     *
     * @access public
     */
    public function testGetNonExistentKey()
    {
        $key = 'fake_key';
        
        $result = $this->model->get($key);
        
        // Should be NULL
        $this->assertNull($result);
    }
    
    /**
     * Tests Creating a new key
     *
     * @access public
     */
    public function testCreateKey()
    {
        $key = 'new_key';
        $value = 'new_value';
        
        $this->assertTrue($this->model->createKey() instanceof Jaws_Error);
        
        $id = $this->model->createKey($key, $value);
        
        $this->assertTrue($id != NULL);
        $this->assertEquals(15, $id);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Creating a duplicate new key
     *
     * @access public
     */
    public function testCreateDuplicateKey()
    {
        $key = 'test_key';
        $value = 'test_value';
        
        $error = $this->model->createKey($key, $value);
        
        $this->assertTrue($error instanceof Jaws_Error);
    }
    
    /**
     * Test Updating Key Value
     *
     * @access public
     */
    public function testSetKey()
    {
        $key = 'test_key';
        $value = 'new_test_value';
        
        $result = $this->model->set($key, $value);
        
        $this->assertTrue($result);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Deleting Key-Value record
     *
     * @access public
     */
    public function testDeleteKey()
    {
        $key = 'test_key';
        
        $del_key = $this->model->deleteKey($key);
        
        $this->assertEquals($key, $del_key);
        
        $result = $this->model->getAll();
        
        $this->assertEquals(13, count($result));
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Key Exists Function
     *
     * @access public
     */
    public function testKeyExists()
    {
        $key1 = 'test_key';
        $key2 = 'fake_dijkjd_key';
        
        $result = $this->model->exists($key1);
        $this->assertTrue($result);
        
        $result = $this->model->exists($key2);
        $this->assertFalse($result);
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}