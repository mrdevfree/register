<?php

require_once JAWS_PATH . 'libraries/jawstest/test_case.php';

/**
 * Register Gadget Supplier Tests
 * 
 * @category    GadgetTests
 * @package     Register Tests
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterSupplierTests extends TestCase
{
    private $model;
    private $admin_model;
    private $db_model;
    
    /**
     * Constructor
     *
     * Initialises Admin Model
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Suppliers');
        $this->admin_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel', 'Suppliers');
        $this->db_model = $GLOBALS['app']->LoadGadget('Register', 'AdminModel');
    }
    
    /**
     * Test getting all Suppliers
     *
     * @access public
     */
    public function testGetAllSuppliers()
    {
        $suppliers = $this->model->GetAllSuppliers();
        
        $this->assertTrue(2 == count($suppliers), "Should return 2 suppliers");
    }
    
    /**
     * Test getting Supplier by ID
     *
     * @access public
     */
    public function testGetSupplierById()
    {
        $supplier_id = 2;
        $company_name = 'ResultsFocus';
        
        $supplier = $this->model->GetSupplierById($supplier_id);
        
        $this->assertEquals($company_name, $supplier['company_name']);
    }
    
    /**
     * Tests Adding New Supplier
     *
     * @access public
     */
    public function testAddSupplier()
    {
        $company = 'Test Company';
        $abn = '125-256-526';
        $f_name = 'George';
        $l_name = 'Somethiing';
        $phone = '03 9563 8815';
        $fax = '03 9563 8815';
        $mobile = '0411 512 512';
        $email = 'g.something@site.com';
        $website = 'site.com';
        $street1 = '512 Main St';
        $street2 = '';
        $city = 'Auckland';
        $state = '';
        $pcode = '2354';
        $country = 'New Zealand';
        $visible = TRUE;
        
        $id = $this->admin_model->SaveNewSupplier(
                $company, $abn, $f_name, $l_name, $phone, $fax, $mobile,
                $email, $website, $street1, $street2, $city, $state, $pcode,
                $country, $visible);
        
        $this->assertNotNull($id);
        $this->assertTrue($id > 1);
        
        $supplier = $this->model->GetSupplierById($id);
        
        $this->assertEquals($company, $supplier['company_name']);
        $this->assertEquals($abn, $supplier['abn']);
        $this->assertEquals($f_name, $supplier['rep_first_name']);
        $this->assertEquals($l_name, $supplier['rep_last_name']);
        $this->assertEquals($phone, $supplier['phone']);
        $this->assertEquals($fax, $supplier['fax']);
        $this->assertEquals($mobile, $supplier['mobile']);
        $this->assertEquals($email, $supplier['email']);
        $this->assertEquals($website, $supplier['website']);
        $this->assertEquals($street1, $supplier['street1']);
        $this->assertEquals($street2, $supplier['street2']);
        $this->assertEquals($city, $supplier['city']);
        $this->assertEquals($state, $supplier['state']);
        $this->assertEquals($pcode, $supplier['pcode']);
        $this->assertEquals($country, $supplier['country']);
        $this->assertEquals($visible, $supplier['supplier_visible']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Updating Supplier
     *
     * @access public
     */
    public function testUpdateSupplier()
    {
        $id = 2;
        $company = 'Test New Name Company';
        $abn = '125-126-526';
        $f_name = 'Tim';
        $l_name = 'Somethiing';
        $phone = '03 9563 0230';
        $fax = '03 9563 5987';
        $mobile = '0411 512 512';
        $email = 't.something@site.com';
        $website = 'site.com.au';
        $street1 = '513 Main St';
        $street2 = 'Great Views Offices';
        $city = 'South Auckland';
        $state = 'None';
        $pcode = '2158';
        $country = 'New Zealands';
        $visible = FALSE;
        
        $this->admin_model->UpdateSupplier(
            $id, $company, $abn, $f_name, $l_name, $phone, $fax, $mobile,
            $email, $website, $street1, $street2, $city, $state, $pcode, $country,
            $visible);
        
        $supplier = $this->model->GetSupplierById($id);
        
        $this->assertEquals($company, $supplier['company_name']);
        $this->assertEquals($abn, $supplier['abn']);
        $this->assertEquals($f_name, $supplier['rep_first_name']);
        $this->assertEquals($l_name, $supplier['rep_last_name']);
        $this->assertEquals($phone, $supplier['phone']);
        $this->assertEquals($fax, $supplier['fax']);
        $this->assertEquals($mobile, $supplier['mobile']);
        $this->assertEquals($email, $supplier['email']);
        $this->assertEquals($website, $supplier['website']);
        $this->assertEquals($street1, $supplier['street1']);
        $this->assertEquals($street2, $supplier['street2']);
        $this->assertEquals($city, $supplier['city']);
        $this->assertEquals($state, $supplier['state']);
        $this->assertEquals($pcode, $supplier['pcode']);
        $this->assertEquals($country, $supplier['country']);
        $this->assertEquals($visible, $supplier['supplier_visible']);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Deleting a Supplier
     *
     * @access public
     */
    public function testDeleteSupplierWithNoProducts()
    {
        $supplier_id = 2;
        
        $suppliers = $this->model->GetAllSuppliers();
        $this->assertTrue(count($suppliers) == 2);
        
        $this->admin_model->DeleteSupplier($supplier_id);
        
        $suppliers = $this->model->GetAllSuppliers();
        $this->assertTrue(count($suppliers) == 1);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Tests Deleting a Supplier
     *
     * @access public
     */
    public function testDeleteSupplierWithProducts()
    {
        $supplier_id = 1;
        
        $suppliers = $this->model->GetAllSuppliers();
        $this->assertTrue(count($suppliers) == 2);
        
        $this->admin_model->DeleteSupplier($supplier_id);
        
        $suppliers = $this->model->GetAllSuppliers();
        $this->assertTrue(count($suppliers) == 2);
        
        // cleanup
        $this->resetDatabase();
    }
    
    /**
     * Helper method to reset database
     *
     * @access private
     */
    private function resetDatabase()
    {
        $this->db_model->removeTestDatabase();
        $this->db_model->createTestDatabase();
    }
}