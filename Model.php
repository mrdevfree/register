<?php
/**
 * Register Gadget
 *
 * @category    GadgetModel
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */
class RegisterModel extends Jaws_Model
{
    /**
     * Initialises Test Suite
     *
     * @access public
     * @return Suite    Suite object
     */
    public function initTestSuite()
    {
        require_once JAWS_PATH . 'libraries/jawstest/test_suite.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/products_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/category_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/customer_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/supplier_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/purchase_order_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/customer_order_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/ajax_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/dictionary_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/model_tests.php';
        require_once JAWS_PATH . 'gadgets/Register/tests/publisher_tests.php';
        
        $suite = new TestSuite();
        $suite->addTestCase(new RegisterProductsTests());
        $suite->addTestCase(new RegisterCategoryTests());
        $suite->addTestCase(new RegisterCustomerTests());
        $suite->addTestCase(new RegisterSupplierTests());
        $suite->addTestCase(new RegisterPurchaseOrderTests());
        $suite->addTestCase(new RegisterCustomerOrderTests());
        $suite->addTestCase(new RegisterAjaxTests());
        $suite->addTestCase(new RegisterDictionaryTests());
        $suite->addTestCase(new RegisterModelTests());
        $suite->addTestCase(new RegisterPublisherTests());
        
        return $suite;
    }
    
    /**
     * Retrieves value for key
     * out of purchase_order value string
     *
     * @access public
     * @param string    $string     url param string
     * @param string    $key        key looking for
     * @return string   value for key
     */
    public function getUrlValue($string, $key)
    {
        $elements = explode('&', $string);
        
        foreach ($elements as $element)
        {
            $val = explode('=', $element);
            if ($val[0] === $key)
            {
                return $val[1];
            }
        }
    }
    
    /**
     * Retrieves an array of values for key
     * out of order_products value string
     *
     * @access public
     * @param string    $string haystack
     * @param string    $key    needle
     * @return string   value for matching needle
     */
    public function getSelectedArray($string, $key)
    {
        $result = array();
        $str = urldecode($string);
        $str = substr($str, 0, strlen($str) - 1);
        
        // break it down into products
        $vars = explode("$", $str);
        foreach ($vars as $var)
        {
            // break it down into product variables
            $s = explode("&", $var);
            foreach ($s as $v) {
                
                $pos = strpos($v, "=");
                if (substr($v, 0, $pos) === $key)
                {
                    $result[] = substr($v, $pos + 1);
                }
            }
        }
        
        return $result;
    }
    
    /**
     * Calculates Customer Order Total
     *
     * @access public
     * @param string    $orderString    customer order string
     * @param string    $orderProducts  customer order products string
     * @return float    order price
     */
    public function calculateCustomerOrderTotal($orderString, $orderProducts)
    {
        $model = $GLOBALS['app']->LoadGadget('Register', 'Model', 'Products');
        
        $total = 0;
        
        $shipping   = $this->getUrlValue($orderString, 'shipping');
        $discount   = $this->getUrlValue($orderString, 'discount');
        $tax        = $this->getUrlValue($orderString, 'tax');
        
        $products   = $this->getSelectedArray($orderProducts, 'product');
        $qty        = $this->getSelectedArray($orderProducts, 'qty');
        $discounts  = $this->getSelectedArray($orderProducts, 'discount');
        
        for($i = 0; $i < count($products); $i++)
        {
            $result = $model->getProductById($products[$i]);
            $total += ($result['price_retail'] * $qty[$i]) - $discounts[$i];
        }
        
        $total += $shipping;
        $total -= $discount;
        $total += $tax;
        
        return $total;
    }
    
    /**
     * Constructs Price string out of
     * inputed price
     *
     * @access public
     * @param string    $price price string
     * @return string   formatted price string
     */
    public function buildPrice($price = '')
    {
        if ($price == 0)
        {
            return '0.00';
        }
        
        $pos = strpos($price, '.');
        
        if ($pos == null)
        {
            return $price . '.00';
        }
        else if ((strlen($price) - $pos) <= 2)
        {
            return $price . '0';
        }
        
        return $price;
    }
    
    public function deleteImage($image = '')
    {
        
    }
    
}