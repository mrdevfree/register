<?php
/**
 * (PEAR) Package configuration file to define dependencies, authors and license
 *
 * @category    PackageConfig
 * @package     Register
 * @author      Tom Kaczocha <tom@crazydev.org>
 * @copyright   2012 Tom Kaczocha
 * @license     http://www.gnu.org/copyleft/gpl.html
 */

$info = array(
              'lincense'    => 'GPL',
              'authors'     => array(
                                     array('lead', 'mrdevfree', 'Tom Kaczocha', 'tom@crazydev.org'),
                                     ),
              'deps'        => array('ControlPanel'),
              );